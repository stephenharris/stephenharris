# Restrict Publish

Restrict Publish allows administrators to the restrict the dates on which other users can publish.

For restricted users the plug-in replaces the WordPress dropdowns and input boxes for date selection with the jQuery UI datepicker. 
This datepicker will have the 'forbidden' dates disabled. If the user does try to publish (or schedule) on a 'forbidden' date, the post will be 
returned to draft status and an appropriate error message displayed. 




## Features

Plug-in features include

### Restrict Publish Dates
Ensure your authors can only publish content on dates that *you* make available. This plugin is perfect or managaging multi-author sites, or ensuring content is only published on selected days.

### User Friendly
Offers an intuitive and easy to use user interface for both admins and authors. Restricted users will have their publish date options replaced by jQuery datepicker with forbidden dates disabled. 

### Flexible Date Restrictions
Restrict the available dates by day of the week, month of the year and arbitrary dates.

### Supports Custom Post Types
Restrict publish dates of any Custom Post Type on your site. Run an eCommerce site? Make sure that new products are only published on weekdays, weekends, or any days you wish. Run a church site? Ensure sermons are only published on Sundays.

### Supports Custom User Roles
Restrict publish dates for any user role on your site. Authors, Editors, Shop Managers, and more!

### Secure
Server-side checks when the post is published or scheduled ensure that the user has selected an available date. Admins with suitable permissions can opt to schedule or publish the post for any date.




## FAQ

### Who is Restricted?
It can be anyone who can publish.  You can select which roles to which the restrictions apply. This includes custom roles that you, or a plug-in has added.

### What Post Types Can Be Restircted?
Any. You can select which in the plug-in settings.

### What Restrictions Can Be Imposed?
You can allow users to only publish on selected days of the week, selected months or arbitrary dates. To allow or exclude specific dates use the jQuery UI datepicker by clicking 'Exclude 
/ Include Specific Dates'.

### Where Are the Settings?
You can find the settings on your WordPress Settings > Writing page.


## Pricing

Restrict Publish is available for purchase from [FooPlugins](http://fooplugins.com/plugins/restrict-publish/) in three price brackets:

  * Personal - $9 - Support and updates for one year, for one site.
  * Business - $14 - Support and updates for one year, for five sites.
  * Developer - $19 - Support and updates for one year, for unlimited sites.

## License Information

Restrict Publish is licensed under the [GNU general public license](http://www.gnu.org/licenses/gpl.html) version 3.

## Support & Upgrades

Automatic Upgrades are available to customers in accordance with the license they have purchased:

 * A Personal License provides automatic upgrades for a single site.
 * A Business License provides automatic upgrades for up to five sites.
 * A Developer License provides automatic upgrades for unlimited sites for you or your clients. You may not resell access to your support license key.

Upgrades are provided for a term of 1 year from the time of purchase. After 1 year a new license will need to be purchased in order to continue receiving support and product upgrades.

Installing the WordPress plugins on a sub-domain or in a sub-folder counts as a unique site. Installing the plugin on a WordPress multi-site website, where members of the network receive their own installation of our plugins, is not prohibited; however, our support staff will only offer support to the primary license holder, or where the domain owner (network manager or network member) has purchased their own support license.



## Screenshots

![Alt text](restrict-publish-settings.png)
<p class="screenshot-caption"> The settings page where you can enable restrictions </p>


![Alt text](restrict-publish-datepicker.png)
<p class="screenshot-caption"> A user who is restricted selects the publication date via a jQuery datepicker. This has all disallowed dates diabled. </p>


![Alt text](restrict-publish-error-message.png)
<p class="screenshot-caption"> If a user is somehow successful in 'publishing' a post with a disallowed date, it is returned to draft status with a warning message displayed. This message is customisable by a filter. </p>

