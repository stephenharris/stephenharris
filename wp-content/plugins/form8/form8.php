<?php
/*
Plugin Name: Form8
Version: 0.3
Description: Developer focussed form manager
Author: Stephen Harris
Author URI: http://www.stephenharris.info
*/
//TODO Add classes to 'error elements'
//TODO Add actions / hooks
//TODO datepicker script

define( 'FORM8_DIR', plugin_dir_path( __FILE__ ) );

/**
 * Sets the FORM8_URL constant via plugin_dir_url.
 * This is done on 'after_setup_theme' hook so themes/plugins can filter it
 */
function _form8_set_constants(){
	/*
	 * Defines the plug-in directory url
	* <code>url:http://mysite.com/wp-content/plugins/form8</code>
	*/
	define( 'FORM8_URL', plugin_dir_url( __FILE__ ) );
}
add_action( 'after_setup_theme', '_form8_set_constants' );

require_once(FORM8_DIR.'utility-functions.php');
require_once(FORM8_DIR.'shortcodes.php');
require_once(FORM8_DIR.'form-customiser.php');
require_once(FORM8_DIR.'admin.php');