<?php

/**
 * Handles the [form8] shortcode
 * Accepts 'id' or 'name' as arguments - at least one is required to idenitify the form
 * @param array $atts Attributes used in the shortcode.
 * @return string The HTML markup of the form.
 */
function form8_form_shortcode( $atts ){
	
	$id = !empty( $atts['id'] ) ? (int) $atts['id'] : 0;
	$name = !empty( $atts['name'] ) ? $atts['name'] : false;
	
	if( empty( $id ) && empty( $name ) )
		return;
	
	if( empty($id) ){
		$id = form8_get_id_from_name( $name );
	}
	
	$form = new Form8_Form( $id );
	$html = $form->display_form();
	
	return $html;
	
}
add_shortcode( 'form8', 'form8_form_shortcode' );