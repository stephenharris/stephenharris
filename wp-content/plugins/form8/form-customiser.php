<?php
/**
 * Handles the form customiser
 */

/**
 * Registers form post type
 * Hooked onto init.
 * @access private
 * @ignore
*/
function _form8_register_form_post_type(){

	/* Register 'form8_form' post type*/
	$form_labels = array(
		'name' => 'Forms',
		'singular_name' => 'Form',
		'add_new' => __( 'Add New', 'form8' ),
		'add_new_item' => __( 'Add New Form', 'form8' ),
		'edit_item' => __( 'Edit Forms', 'form8' ),
		'new_item' => __( 'New Forms', 'form8' ),
		'all_items' => __( 'Forms', 'form8' ),
		'view_item' => __( 'View Form', 'form8' ),
		'search_items' => __( 'Search Forms', 'form8' ),
		'not_found' =>  __( 'No Forms found', 'form8' ),
		'not_found_in_trash' => __( 'No Forms found in Trash', 'form8' ),
		'menu_name' => __( 'Forms', 'form8' )
	);

	$form_args = array(
		'labels' => $form_labels,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => false,
		'show_in_menu' => false,
		'query_var' => true,
		'rewrite' =>false,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'supports' => array(),
	);
	register_post_type( 'form8_form', $form_args );
}
add_action ( 'init' ,'_form8_register_form_post_type' );

/**
 * The Form class
 */
class form8_Form{

	/**
	 * Form ID
	*/
	var $id = 0;

	/**
	 * 2D array: array of metaboxes ( keys: 'standard','advanced','profile')
         * with value an array of form elements
	*/
	static $elements = array();

	/**
	 *Array of metaboxes ( keys: 'standard','advanced','profile')
         * with value the name of the metabox
	*/
	static $metaboxes = array();
	
	function __construct( $id = 0 ){

		self::element_types_init();

		if( empty( $id ) ){
			$forms = get_posts( array( 'fields' => 'ids', 'post_type' => 'form8_form', 'numberposts' => 1 ) );
		
			if( $forms ){
				$id = $forms[0];
			}else{
				$id = wp_insert_post( array( 'post_status' => 'publish', 'post_type' => 'form8_form', 'post_title' => 'New-Form' ) );
			}
		}

		$this->id = (int) $id;
	}

	static function element_types_init(){
		self::$elements = array(
			'standard' => array(
					'select'=>__('Select','form8'), 'input'=>__('Single Line Text','form8'), 'textarea'=>__('Textarea','form8'),
					'radiobox'=>__('Radio Buttons','form8'), 'checkbox'=>__('Checkbox','form8'), 	
					'multiselect'=>__('Multiselect','form8'),
			),
			'advanced' => array(
					'number'=>__('Number','form8'), 'section'=>__('Section Break','form8'), 'html'=>__('HTML','form8'),
					'address'=>__('Address','form8'), 'phone'=>__('Phone','form8'), 'email'=>__('Email','form8'),
					'date'=>__('Date','form8'), /*'time'=>__('Time','form8'),*/ 'url'=>__('Website','form8'), 
					'antispam'=>__('Antispam maths question','form8'),
					'file_upload' => __('File upload')
			),
			'profile' => array( 
					'profile_name' => __( 'Name', 'form8' ), 'profile_aim' => __( 'AIM', 'form8' ), 
					'profile_website' =>__('Website','form8'), 'profile_yahooim' => __( 'Yahoo IM', 'form8' ), 
					'profile_googletalk' => __( 'Jabber/Google Talk', 'form8' ), 'profile_bio' => __( 'Bio', 'form8' ), 
			),

			'_required' => array(
			),
		);
		self::$metaboxes = array(
			'standard' => __( 'Standard Fields','form8' ), 'advanced' => __( 'Advanced Fields','form8' ), 
			'profile' => __( 'Profile Fields','form8' ),
		);	
		self::$elements = apply_filters( 'form8_form_element_types', self::$elements );
		self::$metaboxes = apply_filters( 'form8_form_element_metaboxes', self::$metaboxes );
	}

	static function get_element_types( $metabox = false ){
		if( empty( self::$elements ) )
			self::element_types_init();

		if( $metabox ){
			$elements = self::$elements[$metabox];
		}else{
			$elements = array();
			foreach( self::$elements as  $metabox => $m_elements ){
				$elements += $m_elements;
			}
		}
		
		return $elements;
	}

	function get_elements(){

		$form = get_post( $this->id );

		if ( false === $form || get_post_type( $form ) != 'form8_form' )
			return false;

		$fields = get_post_meta( $this->id, '_form8_fields', true );
		$fields = ( $fields ? $fields : array() );

		$defaults = array(
		);
		
		return apply_filters( 'form8_form_elements', $fields + $defaults, $this->id );
	}


	/**
	 * Produces the HTML mark-up for the form
	 * 
	 * @return string HTML mark-up for form
	 */
	function display_form(){

		wp_enqueue_script( 'form8-front-end');
				
		$html = sprintf( '<form method="post" action="%s" style="padding-top: 30px" id="form8-%d" enctype="multipart/form-data">', get_permalink().'#form8-'.$this->id, $this->id );

		$html .= $this->notices_markup();
		$html .= $this->errors_markup();
		
		$html .= sprintf( '<input type="hidden" name="form8-form-id" value="%d">', $this->id );
		$html .= sprintf( '<input type="hidden" name="form8-page-id" value="%d">', get_the_ID() );
		$html .= '<input type="hidden" name="action" value="form8-submit-form">';
		
		foreach( $this->get_elements() as $element_id => $element ){
			$element['element_id'] = $element_id;
			$element = new form8_Form_Element( $element['element_type'], $element );
			$html .= '<p>'. $element->display() .'</p>';
		}
		
		$html .= sprintf(
					'<p><input type="submit" value="%s" class="%s"></p>',
					$this->get_form_button_text(),
					$this->get_form_button_classes()
				);
		$html .= '</form>';
		
		return apply_filters( 'form8_display_form_html', $html, $this->id );
	}

	function display_form_settings(){
		$html = '<table><tbody>';
		$html .= sprintf(
					'<tr><th>%s<th><td>%s</td></tr>',
					'"Form submitted" message',
					form8_textarea_field(array(
						'value' => $this->get_form_submitted_text(),
						'id' => 'form8-form-button-text',
						'echo' => 0,
						'name' =>  'form8_settings[form_submitted]',
					))
				);

		$html .= sprintf(
				'<tr><th>%s<th><td>%s</td></tr>',
				'"Form submitted" class',
				form8_text_field(array(
						'value' => $this->get_form_notice_classes(),
						'id' => 'form8-form-button-text',
						'echo' => 0,
						'name' =>  'form8_settings[form_submitted_classes]',
				))
		);
		$html .= sprintf(
				'<tr><th>%s<th><td>%s</td></tr>',
				'Form error class',
				form8_text_field(array(
						'value' => $this->get_form_error_classes(),
						'id' => 'form8-form-button-text',
						'echo' => 0,
						'name' =>  'form8_settings[form_error_classes]',
				))
		);
	
		$html .= sprintf(
				'<tr><th>%s<th><td>%s</td></tr>',
				'Form button text',
				form8_text_field(array(
						'value' => $this->get_form_button_text(),
						'id' => 'form8-form-button-text',
						'echo' => 0,
						'name' =>  'form8_settings[form_button]',
				))
		);
		$html .= sprintf(
				'<tr><th>%s<th><td>%s</td></tr>',
				'Form button classes',
				form8_text_field(array(
						'value' => $this->get_form_button_classes(),
						'id' => 'form8-form-button-text',
						'echo' => 0,
						'name' =>  'form8_settings[form_button_classes]',
				))
		);
				
		$html .= '</tbody></table>';
		return $html;
	}
	
	function errors_markup(){
		
		global $form8_form_errors;
		
		$html = '';
		
		if( !is_null( $form8_form_errors ) && isset( $form8_form_errors[$this->id] ) && is_wp_error( $form8_form_errors[$this->id] )  ){
			$html = sprintf( '<div class="%s">',  $this->get_form_error_classes() );
			 $errors = $form8_form_errors[$this->id];
			 foreach( $errors->get_error_codes() as $code ){
			 	foreach( $errors->get_error_messages( $code ) as $message ){
			 		$html .= sprintf( '<p> %s </p>', $message );
			 	}
			 }
			 $html .= '</div>';
		}
		
		return apply_filters( 'form8_form_error_messages', $html, $this->id );
	}
	
	function notices_markup(){
		$html = '';
		
		if( !empty( $_GET['submission'] ) && 'successful' == $_GET['submission'] ){
			$html .= sprintf(
					'<div class="%s"><p>%s</p></div>',
					$this->get_form_notice_classes(),
					$this->get_form_submitted_text()
					);
		}
		
		return apply_filters( 'form8_form_notices', $html, $this->id );
	}
	
	/**
	 * Validates the form, give in the submission $input/
	 * 
	 * 
	 * @param array $input The input as array( element_id => user input )
	 * @param WP_Error An error object for submission errors
	 * @return WP_Error An error object containing any submission errors
	 */
	function validate_form( $input, $errors = false ){

		if( false === $errors )
			$errors = new WP_Error();

		foreach( $this->get_elements() as $element_id => $element ){

			$element['element_id'] = $element_id;
			$element = new form8_Form_Element( $element['element_type'], $element );
			$element->validate( $input, $errors );

		}//Foreach element
		
		do_action_ref_array( 'form8_validate_form', array( $input, $this->id, &$errors ) );

		return $errors;
	}
	
	
	function process_form( $input ){
		do_action( 'form8_process_form', $input, $this->id );
	}


	function save_form( $user_id, $input ){

		do_action( 'form8_pre_save_form', $this->id, $user_id, $input );
		
		$profile_fields = array_keys( form8_Form::get_element_types( 'profile' ) );
		
		foreach( $this->get_elements() as $element_id => $element ){
			$element['element_id'] = $element_id;
			$element = new form8_Form_Element( $element['element_type'], $element );
			$element->save_data( $user_id, $input[$element_id] );
		}

		do_action( 'form8_saved_form', $this->id, $user_id, $input );
	}


	function display_customiser(){
		
		
		echo '<div id="poststuff">';
		printf( '<input type="hidden" name="form8-form-id" value="%d">', $this->id );
		do_action( 'add_meta_boxes_tools_page_form8-admin', null ); 
		echo '<div id="form8-form-customiser-body" style="margin-right: 300px;min-width: 600px;">';

			//Form element 'bins'.
			echo '<div style="float: right;margin-right: -300px;width: 280px;">';
				echo '<div id="form8-form-fixed-mb"style="position: fixed; top: 140px;margin-right:30px; ">';
					do_meta_boxes( '', 'side', null );
					submit_button( __('Save Changes'), 'primary', 'form8-save-form' );
				echo '</div>';
			echo '</div>';

			//Form elements
			echo '<div id="form8-form-fields" style="min-width:600px; min-height:400px;border-radius:3px;border: 1px solid #DFDFDF;" class="form8-form-body">';
			
				echo $this->display_customiser_toolbar();
				
				echo '<div id="form8-settings" style="display:none;">';
					echo $this->display_form_settings();
				echo '</div>';
				
				echo '<ul id="form8-form">';
	
					foreach( $this->get_elements() as $element_id => $element ){
						$element['element_id'] = $element_id;
						$element = new form8_Form_Element( $element['element_type'], $element );
						$element->display_customiser();

					}
				echo '</ul>';
			echo '</div>';

		echo '</div>';//#form8-form-customiser-body 

		echo '<br class="clear">';
		echo '</div>';//#poststuff

	}
	
	function display_customiser_toolbar(){
		
		$html = '<div id="form8-customiser-header">';
		$html .=  '<div class="major-form-actions">';
		
		$html .= '<div class="edit-action">';
		$html .= form8_forms_dropdown( false );
		$html .= get_submit_button( __('Edit Form'), 'secondary', 'form8-go-to', false );
		$html .= '</div><!-- END .edit-action -->';
		
		$html .= '<div class="form-toolbar-option">';
		$html .= '<label>'. __( 'Form name', 'form8' ).' ';
		$html .= form8_text_field( array(
					'value' => form8_get_form_name( $this->id ),
					'name' => 'form8-form-name',
					'style' => 'width:auto;',
					'echo' => false,
				)).'</label>';
		
		$html .= '</div><!-- END .form-slug -->';
		
		$html .= '<div class="form-toolbar-option"> 
					<a href="#" id="form8-toggle-form">Form</a>
					&nbsp;|&nbsp;
					<a href="#" id="form8-toggle-settings">Settings</a>
				</div>';
		
		$html .= '<div class="delete-action">';
		$html .= sprintf(
		'<span class="submitbox"><a class="submitdelete" title="%s" href="%s" > %s </a></span>',
		'Delete this form',
		esc_url( wp_nonce_url( admin_url( 'tools.php?page=form8-admin&form8-delete-form='.$this->id), 'form8-create-form' ) ),
		'Delete Form'
				);
		
		$html .= '</div><!-- END .delete-action -->';
					
		$html .= '<br class="clear">';
					
		$html .= '</div><!-- END .major-form-actions -->';
		$html .= '</div>';
		
		return $html;
	}

	function get_form_submitted_text(){
		
		$text = get_post_meta( $this->id, '_form8_form_submitted', true );
		if( empty( $text ) ){
			$text = 'Form successfully submitted. Thank you!';
		}
		return apply_filters( 'form8_form_submitted_text', $text, $this );
	}
	
	function get_form_notice_classes(){
	
		$text = get_post_meta( $this->id, '_form8_form_submitted_classes', true );
		if( empty( $text ) ){
			$text = 'form8-success-notice';
		}
		return apply_filters( 'form8_form_submitted_classes', $text, $this );
	}
	

	function get_form_error_classes(){
	
		$text = get_post_meta( $this->id, '_form8_form_error_classes', true );
		if( empty( $text ) ){
			$text = 'form8-error-notice';
		}
		return apply_filters( 'form8_form_error_classes', $text, $this );
	}

	function get_form_button_text(){
		$text = get_post_meta( $this->id, '_form8_form_button', true );
		if( empty( $text ) ){
			$text = 'Submit';
		}
		return apply_filters( 'form8_form_button_text', $text, $this );
	}
	

	function get_form_button_classes(){
		$text = get_post_meta( $this->id, '_form8_form_button_classes', true );
		if( empty( $text ) ){
			$text = 'form8-button';
		}
		return apply_filters( 'form8_form_button_classes', $text, $this );
	}
	
	
	function get_missing_fields_text(){
		$text = 'Form successfully submitted. Thank you!';
		return apply_filters( 'form8_missing_fields_text', $text, $this );
	}
	
	function get_invalid_fields_text(){
		$text = 'Form successfully submitted. Thank you!';
		return apply_filters( 'form8_invalid_fields_text', $text, $this );
	}
	
	static function metabox_standard(){
		$element_types = self::get_element_types( 'standard' );
		self::list_element_types( $element_types );
	}

	static function metabox_advanced(){
		$element_types = self::get_element_types( 'advanced' );
		self::list_element_types( $element_types );
	}

	static function metabox_profile(){
		$element_types = self::get_element_types( 'profile' );
		self::list_element_types( $element_types );
	}

	static function list_element_types( $element_types ){
		echo '<ul id="form8_form_field_general_bin" class="form8-field-bin" >';
		foreach ( $element_types as $id => $label )
			printf( '<li id="form8_form_field_%s" class="button"><span class="item-title">%s</span></li>', $id, $label );
		echo '</ul>';
	}

}



/**
 * The Form element class
 */
class form8_Form_Element{


	function __construct( $element_type, $parameters = array() ){
	
		$parameters['field_type'] = $element_type;
		$parameters['element_type'] = $element_type;

		$defaults = array(
						'required' => '','placeholder' => '', 'hour24'=>false, 'description' => '', 'selected' => array(),
						'checked'=>array(), 'size' => false, 'style' => '', 'min' => false, 'max' => false, 'type' => 'text',
						'options' => array( 'First Option' , 'Second Option', 'Third Option' ), 'help'=> false, 
						'class' => 'input form8-'.$element_type, 'use_as_email_subject' => false, 
						'file_types' => array(), 'file_limit' => 0,
		);

		switch( $element_type ){
			case 'email':
		 		$defaults['label'] = __('Email','form8');
				$defaults['placeholder'] =  'john@example.com';
				$defaults['use_as_reply_to'] =  false;
			break;
			case 'date':
				$defaults['label'] = __('Date','form8');
				$defaults['format'] =  'Y-m-d';
				$defaults['placeholder'] =  date_i18n($field['format']);
				$defaults['class'] = 'input form8-date form8-date-input';
			break;
			case 'number':
				$defaults['label'] = __( 'Number', 'form8' );
				$defaults['type'] =  'number';
				$defaults['style'] =  'width:auto;';
			break;
			case 'time':
		 		$defaults['label'] = __('Time','form8');
				$defaults['hour24'] =  true;
				$defaults['placeholder'] =  date_i18n('H:i');
				$defaults['size'] =  5;
				$defaults['style'] =  'width:auto;';
			break;
			case 'profile_website':
			case 'url':
		 		$defaults['label'] = __( 'Website', 'form8' );
				$defaults['placeholder'] =  'http://';
			break;
			case 'phone':
				$defaults['placeholder'] = '(1234) 1234567';
		 		$defaults['label'] = __( 'Phone', 'form8' );
			break;
			case 'address':
		 		$defaults['label'] = __( 'Address', 'form8' );
				$defaults['components'] = array('street-address','city','postcode','state');
			break;
			case 'section':
		 		$defaults['label'] = __( 'Section', 'form8' );
			break;
			case 'profile_aim':
		 		$defaults['label'] = __( 'AIM', 'form8' );
			break;			
			case 'profile_yahooim':
		 		$defaults['label'] = __( 'Yahoo IM', 'form8' );
			break;
			case 'profile_googletalk':
		 		$defaults['label'] = __( 'Jabber / Google Talk', 'form8' );
			break;
			case 'profile_bio':
		 		$defaults['label'] = __( 'Bio', 'form8' );
			break;
			case 'profile_name':
				$defaults['label'] = __( 'Name', 'form8' );
				$defaults['components'] = array( 'first','last' );
			break;
			default:
		 		$defaults['label'] = __( 'Label', 'form8' );

		}

		$this->parameters = wp_parse_args( $parameters, $defaults );
		$this->type = $parameters['field_type'];
		$this->id = $parameters['element_id'];

		if( 'date' == $this->type ){
			$this->parameters['data'] = array(
				'dateformat' => form8_php2jquerydate( $this->get( 'format' ) ),
			);
		}
	}

	function get( $param ){
		return isset( $this->parameters[$param] ) ? $this->parameters[$param] : false;
	}

	function display( $context = 'form' ){

		if( method_exists( __CLASS__, 'display_'.$this->type ) )
			$element_display = 'display_' . $this->type;
		else
			$element_display = 'display_default';


		$required = ( $this->get( 'required' ) ? '<span class="required">*</span>' : '' );
		$no_label = array( 'section', 'html', 'antispam' );
		$html ='';

		//These fieldshave no label
		if( !in_array( $this->type, $no_label ) ){
			$html = sprintf(
					'<label class="form8-label" for="form8-field-%1$s"> %2$s</label>',
					$this->id,
					esc_html( $this->get( 'label' ) ) . $required
				);
		}

		$html .= $this->$element_display();

		return $html;
	}	

	function display_customiser(){

		if( method_exists( __CLASS__, $this->type.'_customiser_markup' ) )
			$element_customiser = $this->type.'_customiser_markup';
		else
			$element_customiser = 'default_customiser_markup';

		$html = sprintf(
			'<input type="hidden" name="form8_field[%s][element_type]" id="form8-example-field-type-%1$s" value="%2$s"/>', 
			$this->id, $this->type
		);  
	
		$html .= $this->$element_customiser();

		printf(
			'<li>
				<div id="form8-form-field-%s" class="postbox form8-form-element invisible">
					<div class="handlediv" title="Click to toggle"><br></div>
					<h3 class="hndle"><span>%s</span></h3>
					<div class="inside">%s</div>
				</div>
			</li>',
			 $this->id,
				sprintf(__('%s - ID: %s','form8'), $this->get_element_name(), $this->id ),
				$html
			);
	}


	function display_select(){

		$multiselect = ( $this->type == 'multiselect' );
		$options = $this->get( 'options' );
		
		if( $multiselect ){
			//Get selected values from selected indexes
			$selected_values = array_intersect_key( $this->get( 'options' ), array_flip( $this->get( 'selected' ) ) );
			$selected = isset( $_POST['form8']['reg_field'][$this->id] ) ? $_POST['form8']['reg_field'][$this->id] : $selected_values;
		
		}else{
			//$html .= print_r($options);
			$selected = is_numeric( $this->get( 'selected' ) ) ? $this->get( 'selected' ) : 0;
			$selected_value = $options[$selected];
			$selected = isset( $_POST['form8']['reg_field'][$this->id] ) ? $_POST['form8']['reg_field'][$this->id] : $selected_value;
		}
		
		$value = isset( $_POST['form8']['reg_field']['$this->id'] ) ? $_POST['form8']['reg_field']['$this->id'] : $this->get( 'selected' );
		$html .= form8_select_field(array(
						'id' => 'form8-field-'.$this->id,
						'class' => $this->get( 'class' ),
						'options' => array_combine( $this->get( 'options' ), $this->get( 'options' ) ),
						'multiselect'=> $multiselect,
						'selected' => $selected,
						'help' => $this->get( 'description' ),
						'name' => 'form8[reg_field]['.$this->id.']',
						'echo' => 0
						));
		return $html;
	}

	function select_customiser_markup(){
		$multiselect = ( $this->type == 'multiselect' );
		$html = '<div class="example">';
		$html .= sprintf('<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>',
								$this->id,
								esc_html( $this->get( 'label' ) ),
								$this->get( 'required' ) ? '' : 'style="display:none"'
							);
		$html .= form8_select_field(array(
						'id'=>'form8-example-field-'.$this->id,
						'options' => $this->get( 'options' ),
						'multiselect'=> $multiselect,
						'selected' => $this->get( 'selected' ),
						'help' => $this->get( 'description' ),
						'disabled'=>true,
						'name' => 'form8_example',
						'echo' => 0
						));
		$html .= '</div>';
			
		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_choices_option();
		$html .= $this->element_required_option();
		$html .= $this->element_description_option();
		
		if(  $this->type != 'multiselect' ){
			$html .= sprintf(
				'<tr>
					<th> %2$s  </th>
					<td> <input type="checkbox" value="1" name="form8_field[%1$d][use_as_email_subject]" %3$s> </td>
				</tr>',
				$this->id,
				__( 'Use as email subject', 'form8' ),
				checked( $this->get('use_as_email_subject'), 1, false )
			);
		}
		
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';
		
		return $html;
	}

	function display_input(){
		$value = isset( $_POST['form8']['reg_field'][$this->id] ) ? $_POST['form8']['reg_field'][$this->id] : $this->get( 'value' );
		$html = form8_text_field(array(
						'type' => $this->get( 'type' ),
						'data' => $this->get( 'data' ),
						'class' => $this->get( 'class' ),
						'placeholder' => $this->get( 'placeholder' ),
						'size' => $this->get( 'size' ),
						'min' => $this->get( 'min' ) !== '' ? $this->get( 'min' ) : false ,
						'max' => $this->get( 'max' ) !== '' ? $this->get( 'max' ) : false ,
						'id' => 'form8-field-'.$this->id,
						'style' => $this->get( 'style' ),
						'help' => $this->get( 'description' ),
						'name' =>  'form8[reg_field]['.$this->id.']',
						'value' => $value,
						'echo' => 0
						));
		return $html;
	}

	function input_customiser_markup(){
		$html = '<div class="example">';
		$html .= sprintf('<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>',
								$this->id,
								esc_html( $this->get( 'label' ) ),
								$this->get( 'required' ) ? '' : 'style="display:none"'
							);
		$html .= form8_text_field(array(
						'type' => $this->get( 'type' ),
						'placeholder' => $this->get( 'placeholder' ),
						'size' => $this->get( 'size' ),
						'min' => $this->get( 'min' ) !== false ? $this->get( 'min' ) : null ,
						'max' => $this->get( 'max' ) !== false ? $this->get( 'max' ) : null ,
						'id' => 'form8-example-field-'.$this->id,
						'style' => $this->get( 'style' ),
						'help' => $this->get( 'description' ),
						'disabled'=>true,
						'name' => 'form8_example',
						'echo' => 0
						));
		$html .= '</div>';
	
		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_required_option();
		$html .= $this->element_description_option();
		$html .= $this->element_placeholder_option();
		
		
		$html .= sprintf(
				'<tr>
					<th> %2$s  </th>
					<td> <input type="checkbox" value="1" name="form8_field[%1$d][use_as_email_subject]" %3$s> </td>
				</tr>',
				$this->id,
				__( 'Use as email subject', 'form8' ),
				checked( $this->get('use_as_email_subject'), 1, false )
		);
		

		if( $this->type == 'date' ){

			$html .= sprintf(
				'<tr><th>%s</th><td>%s</td></td>', 
				__('Format','form8'),
				form8_text_field(array(
					'echo'=>0,'class'=>'field-format','id' => $this->id.'-date-format',
					'name'=> sprintf( 'form8_field[%d][format]', $this->id ),
					'value'=> esc_attr( $this->get( 'format' ) )
				))
			);

		}elseif( $this->type == 'time' ){
			$html .= sprintf(
				'<tr><th>%s</th><td>%s</td></td>', 
				__('24 Hour','form8'),
				form8_checkbox_field( array(
					'name' => sprintf( 'form8_field[%d][hour24]', $this->id ),
					'checked' => esc_attr( $this->get( 'hour24' ) ),
					'options' => 1,
					'echo' => 0,'class' => 'field-hour24','id' => $this->id.'-time-hour24',
				) )
			);
		}elseif( $this->type == 'number' ){
			$html .= sprintf(
						'<tr>
							<th> %2$s  </th><td> %3$s: <input type="number" value="%4$s" name="form8_field[%1$d][min]"></br> 
								%5$s: <input type="number" value="%6$s" name="form8_field[%1$d][max]"></td>
						</tr>',
						$this->id,
						__( 'Range', 'form8' ),
						__( 'Min', 'form8' ),
						$this->get( 'min' ),
						__( 'Max', 'form8' ),
						$this->get( 'max' )
			);
		}elseif( $this->type == 'email' ){
			$html .= sprintf(
						'<tr>
							<th> %2$s  </th>
							<td> <input type="checkbox" value="1" name="form8_field[%1$d][use_as_reply_to]" %3$s> </td>
						</tr>',
						$this->id,
						__( 'Use as \'reply to\'', 'form8' ),
						checked( $this->get('use_as_reply_to'), 1, false )
				);
		}
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';

		return$html;
	}

	function display_file_upload(){
		$html = form8_text_field(array(
				'type' => 'file',
				'data' => $this->get( 'data' ),
				'class' => $this->get( 'class' ),
				'id' => 'form8-field-'.$this->id,
				'style' => $this->get( 'style' ),
				'help' => $this->get( 'description' ),
				'name' =>  'form8[reg_field]['.$this->id.']',
				'value' => '',
				'echo' => 0
		));
		return $html;
	}
	
	function get_file_types(){
		$file_types = array(
			//Images
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif' => 'image/gif',
			'png' => 'image/png',
			'bmp' => 'image/bmp',
			'ico' => 'image/x-icon',
				
			// Text formats
			'txt' => 'text/plain',
			'csv' => 'text/csv',
			'ics' => 'text/calendar',
			'rtx' => 'text/richtext',
			'css' => 'text/css',
			'htm|html' => 'text/html',
			'md' => 'text/x-markdown',
		
			// Misc application formats
			'js' => 'application/javascript',
			'pdf' => 'application/pdf',
			'tar' => 'application/x-tar',
			'zip' => 'application/zip',
			'gz|gzip' => 'application/x-gzip',
			'rar' => 'application/rar',
		);
		return $file_types;
	}
	
	function file_upload_customiser_markup(){
		

		$html = '<div class="example">';
		$html .= sprintf('<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>',
				$this->id,
				esc_html( $this->get( 'label' ) ),
				$this->get( 'required' ) ? '' : 'style="display:none"'
		);
		$html .= form8_text_field(array(
				'type' => 'file',
				'id' => 'form8-example-field-'.$this->id,
				'help' => $this->get( 'description' ),
				'disabled'=>true,
				'name' => 'form8_example',
				'echo' => 0
		));
				
		$html .= '</div>';
	
		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_required_option();
		$html .= $this->element_description_option();

		$html .= sprintf(
					'<tr><th>%s</th><td>%s</td></tr>', 
					__('File limit (kB','form8'),
					form8_text_field(array(
						'echo' => 0, 'id' => $this->id.'-file-limit',
						'name' => sprintf( 'form8_field[%s][file_limit]', $this->id ),
						'value' => $this->get( 'file_limit') ? esc_attr( $this->get( 'file_limit') ) : '',
						'placeholder' => 'unlimited',
					))
				);
					
		$html .= '<tr><th>File types</th><td>';
		$html .= '<table>';
		$count = 0;
		
		foreach( $this->get_file_types() as $ext => $type ){

			if( $count%3 == 0 && $count > 0 ){
				$html .= '</tr>';
			}
			if( $count%3 == 0 ){
				$html .= '<tr>';
			}
			
			$parts = explode( '/', $type );
			$label = $parts[1];
			
			
			$html .= sprintf(
				'<td><label><input type="checkbox" name="form8_field[%1$s][file_types][]" %2$s value="%3$s"> %4$s</label></td>',
				$this->id,
				checked( in_array( $ext, $this->get( 'file_types' ) ), true, false ),
				esc_attr( $ext ),
				esc_html( $label )
			);
			$count++;
		}
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</td></tr>';
		
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';
		return $html;
	}
	
	function display_textarea(){

		$value = isset( $_POST['form8']['reg_field'][$this->id] ) ? $_POST['form8']['reg_field'][$this->id] : $this->get( 'value' );
		$required = ( $this->get( 'required' ) ? '<span class="required">*</span>' : '' );
		$html = form8_textarea_field(array(
			'value' => $value,
			'class' => $this->get( 'class' ),
			'id' => 'form8-example-field-'.$field_id,
			'help' => $this->get( 'description' ),
			'echo' => 0,	
			'name' =>  'form8[reg_field]['.$this->id.']',
		));

		return $html;
	}

	function textarea_customiser_markup(){
		$html = sprintf(
			'<div class="example">
				<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>
				<textarea style="width:90%%" disabled="disabled" id="form8-example-field-%1$d"></textarea>
				<p class="description">%4$s</p>
			</div>', 
			$this->id, 
			$this->get( 'label' ), 
			$this->get( 'required' ) ? '' : 'style="display:none"',
			$this->get( 'description' )
		);
			
		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_required_option();
		$html .= $this->element_description_option();
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';

		return $html;
	}

	function display_radiobox(){
			
		$options = $this->get( 'options' );
		$selected_value = $options[$this->get( 'selected' )];
		$selected = isset( $_POST['form8']['reg_field'][$this->id] ) ? $_POST['form8']['reg_field'][$this->id] : $selected_value;
		
		$html = sprintf('<ul id="form8-field-%1$d">', $this->id);
		foreach( $this->get( 'options' ) as $index => $value ) :
			$html .= sprintf(
					'<li><label><input type="radio" name="%5$s" style="%4$s" value="%2$s" %3$s> %2$s</label></li>',
					$index,
					$value,
					checked( $selected, $value, false ),
					$this->get( 'style' ),
					'form8[reg_field]['.$this->id.']'
					);
		endforeach;
		$html .= '</ul>';
		$html .= sprintf('<p class="description">%s</p>',$this->get( 'description' ) ); 
		return $html;
	}

	function radiobox_customiser_markup(){
		$html .= sprintf(
			'<div class="example example-radiobox">
				<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>',
			$this->id, 
			$this->get( 'label' ), 
			$this->get( 'required' ) ? '' : 'style="display:none"'
		);

		$html .= sprintf('<ul id="form8-example-field-%1$d">', $this->id);
		foreach( $this->get( 'options' ) as $index => $value ) :
			$html .= sprintf(
						'<li><input name="form8-example-radio-%1$d" type="radio" disabled="disabled" %3$s><label>%2$s</label></li>',
						$this->id, 
						$value,
						checked( $this->get( 'selected' ), $index, false ) 
					);
		endforeach;
		$html .= '</ul>';
		$html .= sprintf('<p class="description">%s</p>',$this->get( 'description' ) ); 
		$html .= '</div>';
			
		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_choices_option();
		$html .= $this->element_required_option();
		$html .= $this->element_description_option();
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';

		return $html;
	}

	function display_checkbox(){

		//Get selected values from selected indexes
		$selected_values = array_intersect_key( $this->get( 'options' ), array_flip( $this->get( 'checked' ) ) );		
		$selected_values = isset( $_POST['form8']['reg_field'][$this->id] ) ? $_POST['form8']['reg_field'][$this->id] : $selected_values;
		
		$html = sprintf('<ul id="form8-field-%1$d">', $this->id);
		foreach( $this->get( 'options' ) as $index => $value ) :
			$html .= sprintf(
						'<li><label><input type="checkbox" name="%1$s" style="%4$s" value="%2$s" %3$s> %2$s</label></li>',
						'form8[reg_field]['.$this->id.'][]', 
						$value,
						checked( in_array( $value, $selected_values), true, false ),
						$this->get( 'class' )
					);
		endforeach;
		$html .= '</ul>';
		$html .= sprintf('<p class="description">%s</p>',$this->get( 'description' ) ); 
		return $html;
	}

	function checkbox_customiser_markup(){
		$html .= sprintf(
			'<div class="example example-checkbox">
				<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>',
			$this->id, 
			$this->get( 'label' ), 
			$this->get( 'required' ) ? '' : 'style="display:none"'
		);

		$html .= sprintf('<ul id="form8-example-field-%1$d">', $this->id);
		foreach( $this->get( 'options' ) as $index => $value ) :
			$html .= sprintf(
						'<li><input type="checkbox" name="form8-example-radio-%1$d" disabled="disabled" %3$s><label>%2$s</label></li>',
						$this->id, 
						$value,
						checked( in_array( $index, $this->get( 'checked' ) ), true, false )
					);
		endforeach;
		$html .= '</ul>';
		$html .= sprintf('<p class="description">%s</p>',$this->get( 'description' ) ); 
		$html .= '</div>';
			
		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_choices_option();
		$html .= $this->element_required_option();
		$html .= $this->element_description_option();
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';

		return $html;
	}

	function display_html(){
		return $this->get( 'html' );
	}

	function html_customiser_markup(){
		$html = sprintf(
			'<div class="example"><p>%s</p></div>
			<div class="edit"><textarea rows="8" class="field-description large-text"name="form8_field[%d][html]">%s</textarea></div>',
			__( 'This form element allows you to enter any HTML, to be displayed on the form', 'form8' ),
			$this->id,
			$this->get( 'html' )
		);
		$html .= '<div class="edit">';
		$html .= $this->element_delete_option();
		$html .= '</div>';
		
		return $html;
	}

	function display_section(){
		$html = sprintf(
				'<h2 id="form8-field-%1$d" class="form8-section-header"> %2$s</h2><hr class="form8-section-break">',
				$this->id,
				$this->get( 'label' )
		);
		return $html;
	}

	function section_customiser_markup(){
		$html = sprintf(
			'<div class="example">
				<h2 id="form8-example-field-%1$d" class="field-header"> %2$s</h2>
				<hr class="form8-reg-section-break">
			</div>',
			$this->id,
			$this->get( 'label' )
		);
		
		$html .= '<div class="edit">';
		$html .= sprintf(
					'<table class="form-table"><tr><th> %1$s</th><td> %2$s</td></td></table>',
					__('Label','form8'),
					form8_text_field(array(
						'echo'=>0,'class'=>'field-header','id'=>$this->id.'-label',
						'name'=> sprintf('form8_field[%d][label]', $this->id),
						'value'=> esc_attr(  $this->get( 'label' ) ),
					))	
				);
		$html .= $this->element_delete_option();
		$html .= '</div>';
		return $html;
	}

	function display_antispam(){
		$required = ( $this->get( 'required' ) ? '<span class="required">*</span>' : '' );
		$n1 = rand( 1,12 );
		$n2 = rand( 1, 12 );

		$html = sprintf(
			'<label class="form8-label" for="form8-field-%1$d"> %2$s</label></br>',
			$this->id,
			sprintf( __('What is %d + %d?','form8' ), $n1, $n2 )
		);

		$html .= form8_text_field(array(
					'type' => 'number',
					'class' => $this->get( 'class' ),
					'placeholder' => $this->get( 'placeholder' ),
					'size' => $this->get( 'size' ),
					'id' => 'form8-field-'.$field_id,
					'style' => $this->get( 'style' ),
					'help' => $this->get( 'description' ),
					'name' =>  'form8[reg_field]['.$this->id.'][i]',
					'echo' => 0
				));
				$html .= form8_text_field(array(
					'type' => 'hidden',
					'value' => wp_hash( $n1 + $n2 ),
					'id' => 'form8-field-'.$field_id.'-2',
					'name' =>  'form8[reg_field]['.$this->id.'][h]',
					'echo' => 0
				));
		return $html;
	}

	function antispam_customiser_markup(){

		$html .= sprintf(
			'<div class="example">
				<p> %2$s </p>	
				<label class="field-label" for="form8-example-field-%1$d"> %3$s</label><span class="required" style="display:none">*</span></br>	
				<input disabled="disabled" type="number" placeholder="%4$s" id="form8-example-field-%1$d" value=""/>
				<p class="description">%5$s</p>
			</div>',
			$this->id,
			__('This form element asks the user a simple random maths question in order to help prevent spam','form8'),
			sprintf( __('What is %d + %d?','form8' ), rand( 1,12 ), rand( 1, 12 ) ),
			$this->get( 'placeholder' ),
			$this->get( 'description' )
		);

		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_description_option();
		$html .= sprintf( '<input type="hidden" value="1" name="form8_field[%d][required]">', $this->id );
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';

		return $html;
	}

	function display_address(){

		$html = sprintf( '<p class="description"> %1$s </p>', $this->get( 'description' ) );
		$html .='<span class="example-address-components">';
		$html .= sprintf('<p><label class="sub-form-label" %4$s for="%1$s">  %2$s </br> %3$s </label></p>',
						'form8-field-street-address-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id' => 'form8-field-street-address'.$this->id,
							'placeholder'=> __('Street address', 'form8'),
							'name' => 'form8[reg_field]['.$this->id.'][street-address]',
						)),
						__('Street Address','form8'),
						in_array( 'street-address', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p><label class="sub-form-label" for="%1$s" %4$s>  %2$s </br> %3$s </label></p>',
						'form8-field-2nd-line-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-field-2nd-line'.$this->id,
							'placeholder'=>'',
							'disabled'=>true,
							'name' => 'form8[reg_field]['.$this->id.'][2nd-line]',
						)),
						__('Address Line 2','form8'),
						in_array( '2nd-line', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p><label class="sub-form-label" for="%1$s" %4$s>  %2$s </br> %3$s </label></p>',
						'form8-field-city-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-field-city'.$this->id,
							'placeholder'=>__('City', 'form8'),
							'name' => 'form8[reg_field]['.$this->id.'][city]',
						)),
						__('City','form8'),
						in_array( 'city', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p style="overflow:hidden">
							<label class="sub-form-label" for="%1$s" style="float:left; %4$s">  %2$s </br> %3$s </label>
							<label class="sub-form-label" for="%5$s" style="float:left; %8$s">  %6$s </br> %7$s </label>
						</p>',
						'form8-example-field-state-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-field-state'.$this->id,
							'placeholder'=>  __('State/Province','form8'),
							'style'=>'width:90%',
							'name' => 'form8[reg_field]['.$this->id.'][state]',
						)),
						__('State /Province','form8'),
						in_array( 'state', $this->get( 'components' ) ) ? '' : 'style="display:none"',
						'form8-example-field-postcode-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-field-postcode'.$this->id,
							'placeholder'=>__('Post Code','form8'),
							'style'=>'width:90%',
							'name' => 'form8[reg_field]['.$this->id.'][postcode]',
						)),
						__('Post Code','form8'),
						in_array( 'postcode', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p><label class="sub-form-label" for="%1$s" %4$s>  %2$s </br> %3$s </label></p>',
						'form8-example-field-country-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-field-country'.$this->id,
							'placeholder'=>__('Country','form8'),
							'name' => 'form8_example'
						)),
						__('Country','form8'),
						in_array( 'country', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);
		$html .= '</span">';
		return $html;
	}


	function address_customiser_markup(){
						
		$html = '<div class="example">';
		$html .= sprintf(
					'<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>
					<p class="description"> %4$s </p>',
					$this->id, 
					$this->get( 'label' ), 
					$this->get( 'required' ) ? '' : 'style="display:none"',
					$this->get( 'description' )
		); 

		$html .='<span class="example-address-components">';
		$html .= sprintf('<p><label class="sub-form-label" %4$s for="%1$s">  %2$s </br> %3$s </label></p>',
						'form8-example-field-street-address-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id' => 'form8-example-field-street-address'.$this->id,
							'placeholder'=> __('Street address', 'form8'),
							'style'=>$this->get( 'style' ),
							'disabled' => true,
							'name' => 'form8_example'
						)),
						__('Street Address','form8'),
						in_array( 'street-address', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p><label class="sub-form-label" for="%1$s" %4$s>  %2$s </br> %3$s </label></p>',
						'form8-example-field-2nd-line-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-example-field-2nd-line'.$this->id,
							'placeholder'=>'',
							'style'=>$field['style'],
							'disabled'=>true,
							'name' => 'form8_example'
						)),
						__('Address Line 2','form8'),
						in_array( '2nd-line', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p><label class="sub-form-label" for="%1$s" %4$s>  %2$s </br> %3$s </label></p>',
						'form8-example-field-city-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-example-field-city'.$this->id,
							'placeholder'=>__('City', 'form8'),
							'style'=>$field['style'],
							'disabled'=>true,
							'name' => 'form8_example'
						)),
						__('City','form8'),
						in_array( 'city', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p style="overflow:hidden">
							<label class="sub-form-label" for="%1$s" style="float:left; %4$s">  %2$s </br> %3$s </label>
							<label class="sub-form-label" for="%5$s" style="float:left; %8$s">  %6$s </br> %7$s </label>
						</p>',
						'form8-example-field-state-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-example-field-state'.$this->id,
							'placeholder'=>  __('State/Province','form8'),
							'style'=>'width:90%',
							'disabled'=>true,
							'name' => 'form8_example'
						)),
						__('State /Province','form8'),
						in_array( 'state', $this->get( 'components' ) ) ? '' : 'style="display:none"',
						'form8-example-field-postcode-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-example-field-postcode'.$this->id,
							'placeholder'=>__('Post Code','form8'),
							'style'=>'width:90%',
							'disabled'=>true,
							'name' => 'form8_example'
						)),
						__('Post Code','form8'),
						in_array( 'postcode', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);

		$html .= sprintf('<p><label class="sub-form-label" for="%1$s" %4$s>  %2$s </br> %3$s </label></p>',
						'form8-example-field-country-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-example-field-country'.$this->id,
							'placeholder'=>__('Country','form8'),
							'disabled'=>true,
							'name' => 'form8_example'
						)),
						__('Country','form8'),
						in_array( 'country', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);
		$html .= '</span">';
		$html .= '</div>';

		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_required_option();
		$html .= sprintf(
					'<tr><th>%s</th><td>%s</td></td>', 
					__('Address Includes','form8'),
					form8_checkbox_field(array(
						'echo'=>0,'class'=>'field-label','id'=>$this->id.'-address',
						'class'=> 'field-address',
						'name'=> sprintf( 'form8_field[%d][components]', $this->id ),
						'checked'=> $this->get( 'components' ),
						'options'=>array(
							'street-address'=>'Street Address',
							'2nd-line'=>'Second Line',
							'city'=>'City',
							'state'=>'State / Province',
							'postcode'=>'Post Code',
							'country'=>'Country',
						)
					))
				);
		$html .= $this->element_description_option();
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';

		return $html;
	}

	function display_profile_name(){

		$html = sprintf( '<p class="description"> %1$s </p>', $this->get( 'description' ) );
		$html .='<span class="example-profile-name-components">';
		$html .= sprintf('<p style="overflow:hidden">
							<label class="sub-form-label" for="%1$s" style="float:left; %3$s">  %2$s </br></label>
							<label class="sub-form-label" for="%4$s" style="float:left; %6$s">  %5$s </br> </label>
						</p>',
						'form8-field-first-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-field-first-'.$this->id,
							'placeholder'=>  __('First Name','form8'),
							'style'=>'width:90%',
							'name' =>  'form8[reg_field]['.$this->id.'][first]'
						)),
						in_array( 'first', $this->get( 'components' ) ) ? '' : 'style="display:none"',
						'form8-example-field-last-name-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-field-last-'.$this->id,
							'placeholder'=>__('Last Name','form8'),
							'style'=>'width:90%',
							'name' =>  'form8[reg_field]['.$this->id.'][last]'
						)),
						in_array( 'last', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);
		$html .= '</span">';

		return $html;
	}

	function profile_name_customiser_markup(){
		$html = '<div class="example">';
		$html .= sprintf(
					'<label class="field-label" for="form8-example-field-%1$d"> %2$s</label><span class="required" %3$s>*</span></br>
					<p class="description"> %4$s </p>',
					$this->id, 
					$this->get( 'label' ), 
					$this->get( 'required' ) ? '' : 'style="display:none"',
					$this->get( 'description' )
		); 

		$html .='<span class="example-profile-name-components">';
		$html .= sprintf('<p style="overflow:hidden">
							<label class="sub-form-label" for="%1$s" style="float:left; %3$s">  %2$s </br></label>
							<label class="sub-form-label" for="%4$s" style="float:left; %6$s">  %5$s </br> </label>
						</p>',
						'form8-example-field-first-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-example-field-first'.$this->id,
							'placeholder'=>  __('First Name','form8'),
							'style'=>'width:90%',
							'disabled'=>true,
							'name' => 'form8_example'
						)),
						in_array( 'first', $this->get( 'components' ) ) ? '' : 'style="display:none"',
						'form8-example-field-postcode-'.$this->id,
						form8_text_field(array(
							'type'=>'text',
							'echo'=>0,
							'id'=>'form8-example-field-last'.$this->id,
							'placeholder'=>__('Last Name','form8'),
							'style'=>'width:90%',
							'disabled'=>true,
							'name' => 'form8_example'
						)),
						in_array( 'last', $this->get( 'components' ) ) ? '' : 'style="display:none"'
				);
		$html .= '</span">';
		$html .= '</div>';

		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= $this->element_required_option();
		$html .= sprintf(
					'<tr><th>%s</th><td>%s</td></td>', 
					__('Include','form8'),
					form8_checkbox_field(array(
						'echo'=>0, 'class' => 'field-label', 'id' => $this->id.'-profile-name',
						'class'=> 'field-profile-name',
						'name'=> sprintf( 'form8_field[%d][components]', $this->id ),
						'checked'=> $this->get( 'components' ),
						'options'=>array(
							'first'  => 'First name',
							'last'=> 'Last name',
						)
					))
				);
		$html .= $this->element_description_option();
		$html .= '</table>';
		$html .= $this->element_delete_option();
		$html .= '</div>';

		return $html;
	}

	function display_gateway(){
		$gateways = form8_get_enabled_gateways();
		$html = '<fieldset  id="form8_gateways">';
		if( !empty($gateways) && count( $gateways ) == 1 ){
			reset( $gateways );
			$gateway = key( $gateways );

			$html .= sprintf( '<input type="hidden" name="form8[reg_field][%s]" value ="%s">', $this->id, $gateway  );

			if( $gateway == 'offline' ){
				$html .= sprintf("<div class='form8-offline-instructions'><strong> %s </strong> %s </div>",
							__('Offline Payment','form8'),
							form8_get_offline_instructions()
							);
			}

		}elseif(  !empty($gateways) ){
			foreach($gateways as $gateway => $label){
				$html .= form8_display_gateway( $gateway, sprintf( 'form8[reg_field][%s]', $this->id ) );
			}				
			$html.='</fieldset>';
		}

		return $html;
	}


	function gateway_customiser_markup(){
		$html = sprintf(
				'<div class="example example-gateway"><p>%s</p></div>',
				__('This form element only appears if there there are multiple gateways enabled. It cannot be removed but can be placed anywhere in the registration form.','form8')
		);
		$html .= '<div class="edit"><table class="form-table">';
		$html .= $this->element_label_option();
		$html .= '</table></div>';

		return $html;
	}

	function display_default(){

		switch( $this->type ){
			case 'date':
				wp_enqueue_style('form8_jquery_ui');
			case 'email':
			case 'number':
			case 'phone':
			case 'time':
			case 'url':
			case 'profile_aim':
			case 'profile_yahooim':
			case 'profile_googletalk':
			case 'profile_website':
				return $this->display_input();
			break;
			case 'multiselect':
				return $this->display_select();
			break;
			case 'profile_bio':
				return $this->display_textarea();
			break;
		}
		return 'Test: '.$this->type.'--'.$this->id;
	}



	function default_customiser_markup(){

		switch( $this->type ){
			case 'email':
			case 'number':
			case 'phone':
			case 'date':
			case 'time':
			case 'url':
			case 'profile_aim':
			case 'profile_yahooim':
			case 'profile_googletalk':
			case 'profile_website':
				return $this->input_customiser_markup();
			break;
			case 'multiselect':
				return $this->select_customiser_markup();
			break;
			case 'profile_bio':
				return $this->textarea_customiser_markup();
			break;
		}
		return 'default';
	}



	function element_label_option(){
		return sprintf(
					'<tr><th>%s</th><td>%s</td></tr>', 
					__('Label','form8'),
					form8_text_field(array(
						'echo' => 0, 'class' => 'field-label', 'id' => $this->id.'-label',
						'name' => sprintf( 'form8_field[%s][label]', $this->id ),
						'value' => esc_attr( $this->get( 'label') )
					))
		);
	}

	function element_choices_option(){

		$form_type = ( $this->type == 'checkbox' || $this->type == 'multiselect' ? 'checkbox' : 'radio' );
		$options = $this->get( 'options' );
	
		$html = sprintf( '<tr><th> %s </th>', __( 'Options', 'form8' ) );
		$html .= '<td><ul class="field-options field-options-'.$this->type.'">';
				
		if( $options ):
			foreach( $options as $index => $value ){

				if( $this->type == 'checkbox' ){
					$check = in_array( $index, $this->get( 'checked' ) );
					$name = "form8_field[{$this->id}][checked][]";

				}elseif( $this->type == 'multiselect' ){
						$check = in_array( $index, $this->get( 'selected' ) );
						$name = "form8_field[{$this->id}][selected][]";

				}elseif( $this->type == 'radiobox' ){
					$check = ( $this->get( 'selected' ) == $index );
					$name = "form8_field[{$this->id}][selected]";

				}elseif( $this->type == 'select' ){
					$check = ( $this->get( 'selected' ) == $index );
					$name = "form8_field[{$this->id}][selected]";

				}else{
					$check = false;
					$name = "form8_field[{$this->id}][selected]";
				}

				$html .= sprintf(
						'<li>
						<input type="%2$s" class="field-option-selected" name="%6$s" %5$s value="%4$s" id="field-option-selected-%1$d">
						<input type="text" class="field-option-label" name="form8_field[%1$d][options][]" id="field-option-%1$d" value="%3$s" class="field-option">
						<span class="field-option-add" alt="add another choice" style="height: 18px;vertical-align: bottom;margin-bottom: 3px;"></span>
						<span class="field-option-remove" alt="remove another choice" style="height: 18px;vertical-align: bottom;margin-bottom: 3px;"></span>
						</li>',
						$this->id,
						$form_type,
						$value,
						$index,
						checked( $check, true, false ),
						$name
						);
			}
		endif;
		$html .= '</ul></td></tr>';

		return $html;
	}

	function element_required_option(){
		return sprintf('<tr><th>%s</th><td>%s</td></td>', 
				__('Required','form8'),
				form8_checkbox_field(array(
					'name' => sprintf( 'form8_field[%d][required]', $this->id ),
					'checked' => esc_attr( $this->get( 'required' ) ),
					'options'=>1,
					'echo' => 0,'class' => 'field-required','id' => $this->id.'-required',
				))
		);
	}

	function element_delete_option(){
		return sprintf( 
			'<span class="submitbox"><a class="submitdelete" title="%2$s">%1$s</a></span>',
			__( 'Delete Element', 'form8' ),
			__( 'Delete this element', 'form8' )
		);
	}

	function element_placeholder_option(){
		return sprintf( 
				'<tr>
					<th>%s</th>
					<td><input  type="text"  class="field-placeholder regular-text ltr" value="%s" name="form8_field[%s][placeholder]"></td>
				</tr>',
				__( 'Placholder', 'form8' ),
				$this->get( 'placeholder' ),
				$this->id
		);
	}

	function element_description_option(){
		return sprintf( 
					'<tr>
						<th> %s </th>
						<td><textarea rows="4" class="field-description large-text"name="form8_field[%d][description]">%s</textarea></td>
					</tr>',
					__( 'Description', 'form8' ),
					$this->id, 
					$this->get( 'description')
					); 
	}

	function save_data( $user_id, $value ){

		$profile_fields = array_keys( form8_Form::get_element_types( 'profile' ) );

		if( !in_array( $this->type, $profile_fields ) ){
			form8_update_member_meta( $user_id, $this->id, $value );
		}else{
			$userdata  = array ('ID' => $user_id );
			switch( $this->type ){
				case 'profile_name':
					$userdata['first_name'] = sanitize_text_field( $value['first'] );
					$userdata['last_name'] = sanitize_text_field( $value['last'] );
				break;
				case 'profile_website':
					$userdata['user_url'] = sanitize_url( $value );
				break;
				case 'profile_aim':
					$userdata['aim'] = sanitize_text_field( $value );
				break;
				case 'profile_yahooim':
					$userdata['yim'] = sanitize_text_field( $value );
				break;
				case 'profile_googletalk':
					$userdata['jabber'] = sanitize_text_field( $value );
				break;
				case 'profile_bio':
					$userdata['description'] = trim( $value ); //Properly sanitised by WP
				break;
				default:
					return;
			}
			
			$re = wp_update_user( $userdata );
		
		}
	}
	
	
	function validate( $input, &$errors ){
		
		$missing_data = $errors->get_error_data( 'required_field_missing' );
		$invalid_data = $errors->get_error_data( 'invalid_data');
		
		if( is_null( $missing_data ) )
			$missing_data = array();
		if( is_null( $invalid_data ) )
			$invalid_data = array();
		
		if( !$this->get( 'required' ) && $this->type != 'file_upload'  )
			return;
		
		if( empty( $input[$this->id] ) && $this->type != 'file_upload'  ){
			//Empty value for required field
			$missing_data[] = $this->id;
		
		}elseif ( in_array( $this->type, array( 'address', 'profile_name', 'antispam' ) ) && !array_filter( $input[$this->id] ) ) {
			//Profile name, antispam & address should give us an array
			$missing_data[] = $this->id;
		
		}else{
			$valid = false;

			switch( $this->type ):
			
				case 'antispam':
					$i = $input[$this->id]['i']; //User input
					$h = $input[$this->id]['h']; //Hash of the answer
					$valid = ( is_numeric( $i ) && wp_hash( $i ) === $h );
				break;
			
				case 'email':
					$valid = is_email( $input[$this->id] );
				break;
			
				case 'number':
					$valid = is_int( $input[$this->id] );
				break;
			
				case 'url':
					$valid = ( $input[$this->id] ==  esc_url_raw( $input[$this->id] ) );
				break;
				
				case 'file_upload':
					
					$element_id = $this->id;
					
					$error = $_FILES['form8']['error']['reg_field'][$element_id];
					$tmp_file = $_FILES['form8']['tmp_name']['reg_field'][$element_id];
					
					if( $error == 4 || empty( $tmp_file ) ){
						//No upload. Valid unless file is required
						$valid = ( !$this->get( 'required' ) );
						
					}elseif( $error != 0 ){
						//Some error:
						$valid = false;
						
					}else{
						//File seems valids, now check if it meets our file type whitelist and upload limit.
						
						$file_types = $this->get_file_types();
						$selected_types = array_intersect_key( $file_types, array_flip( $this->get( 'file_types' ) ) );
						
						$allowed_extensions = explode( '|', implode( '|', array_keys( $selected_types ) ) );
						$allowed_types = array_values( $selected_types );
						$size_limit = $this->get('file_limit');
					
						//Get the uploaded file information
						$size = $_FILES['form8']['size']['reg_field'][$element_id] / 1024;
						$type = $_FILES['form8']['type']['reg_field'][$element_id];
						$name = $_FILES['form8']['name']['reg_field'][$element_id];
					
						$ext = substr( $name, strrpos( $name, '.' ) + 1 );
					
						if( !in_array( $type, $allowed_types ) || !in_array( $ext, $allowed_extensions ) ){
							$errors->add( 
								'invalid-file-type', 
								'<strong>ERROR:</strong> Invalid file type. Allowed types: .' .implode( ', .', $allowed_extensions ) 
							);
							$valid = false;
						
						}elseif( $size > $size_limit ){
							$errors->add(
								'file-to-big',
								"<strong>ERROR:</strong> File is too large. Files must be under {$size_limit}kB" 
							);
							$valid = false;
						
						}else{
							$valid = true;	
						}
					}
				break;
				
				case 'checkbox':
				case 'radiobox':
				case 'select':
				case 'date':
				case 'input':
				case 'time':
				case 'multiselect':
				case 'date':
				case 'input':
				case 'time':
				case 'phone':
				case 'address':
				default:
					$valid = true;
				break;
			endswitch;
		
			if ( !$valid )
				$invalid_data[] = $this->id;
		}
		
		
		//Add or update 'missing data' error
		if ( array_filter( $missing_data ) ){
			if( $errors->get_error_messages( 'required_field_missing' ) ){
				$errors->add_data( $missing_data, 'required_field_missing' );
			}else{
				$errors->add( 'required_field_missing', __( '<strong>ERROR</strong>: Please fill in all required fields', 'form8' ), $missing_data );
			}			
		}
		
		//Add or update 'invalid data' error
		if ( array_filter( $invalid_data ) ){
			if( $errors->get_error_messages( 'invalid_data' ) ){
				$errors->add_data( $invalid_data, 'invalid_data' );
			}else{
				$errors->add( 'invalid_data', __( '<strong>ERROR</strong>: Some fields are not valid', 'form8' ), $invalid_data );
			}
		}
		
		do_action_ref_array( 'form8_validate_element_input', array( $input, $this->id, &$errors ) );
	}

	function get_element_name(){
		$elements = form8_Form::get_element_types();
		return apply_filters( 'form8_element_type_name', $elements[$this->type], $this->type );
	}

}



//Add registration customiser metaboxes
function form8_reg_cust_add_metabox() {

	$screen_id = 'tools_page_form8-admin';

	foreach( form8_Form::$elements as $mb => $elements ){
	
		//Ignore 'hidden' metaboxes
		if( $mb[0] == '_' )
			continue;

		if( method_exists('form8_Form','metabox_'.$mb ) )
			$callback =  array( 'form8_Form', 'metabox_'.$mb );
		else
			$callback = array( 'form8_Form', 'metabox_default' );

		add_meta_box( 'form8-form-elements-'.$mb, form8_Form::$metaboxes[$mb], $callback , $screen_id , 'side' );
	}	
}
add_action( 'add_meta_boxes_tools_page_form8-admin', 'form8_reg_cust_add_metabox' );



function _form8_add_element_to_form(){
	
	$element_type = $_REQUEST['element_type'];

	//Ensure we get a unique ID
	$element_id = form8_get_option( 'element_id' );
	$element_id++;

	$element = new form8_Form_Element( $element_type, array( 'element_id' => $element_id ) );
	$element->display_customiser();

	//Store the increment ID back the options
	form8_update_option( 'element_id', $element_id );
	exit();
}
add_action( 'wp_ajax_form8-form-add-element','_form8_add_element_to_form' );



add_action( 'init', '_form8_form_submission_listener' );
function _form8_form_submission_listener(){

	if( isset( $_POST['action'] ) && 'form8-submit-form' == $_POST['action'] && !empty( $_POST['form8-form-id'] ) ){
		$input = $_POST['form8']['reg_field'];
		$form_id = (int) $_POST['form8-form-id'];
		
		do_action( 'form8_form_submission', $input, $form_id );
		
	}
	
}


function _form8_form_submission_handler( $input, $form_id ){
	global $form8_form_errors;
	
	$form = new Form8_Form($form_id);

	$errors = $form->validate_form( $input );
	
	if( $errors->get_error_messages() ){
		if( !is_array( $form8_form_errors ) )
			$form8_form_errors[$form_id] = $errors;
		
	}else{
		$form->process_form( $input );
	
		do_action( 'form8_processed_form', $input, $form_id, $form );
		
		//Redirect based on options
		$url = get_permalink( $_POST['form8-page-id'] );
		wp_redirect( add_query_arg( 'submission', 'successful', $url ) );
		exit();
	}
}
add_action( 'form8_form_submission', '_form8_form_submission_handler', 10, 2 );


function _form8_email_form_submssion( $input, $id, $form ){
	
	$message = '';
	$subject = '[form submission '.form8_get_form_name( $id ).']';
	$headers = array(
		'content-type:text/html'
	);
	$attachments = array();
	
	foreach( $form->get_elements() as $element_id => $element ){
		
		$element['element_id'] = $element_id;-
		$element = new form8_Form_Element( $element['element_type'], $element );
		
		$label = trim( $element->get('label'), ':' );
		$value = isset( $input[$element_id] ) ? stripslashes( $input[$element_id] ) : '';
		
		if( $element->get( 'use_as_reply_to' ) && sanitize_email( $value ) ){
			
			$removed_from = remove_filter('wp_mail_from','wp_mail_smtp_mail_from');
			$removed_from_name = remove_filter('wp_mail_from_name','wp_mail_smtp_mail_from_name');
			
			$headers[] = 'from:'.sanitize_email( $value );
			$headers[] = 'reply-to:'.sanitize_email( $value );
			$value = sanitize_email( $value );

		}elseif( $element->get( 'use_as_email_subject' ) && $value ){
			$subject = " ".$value;
		
		}elseif( $element->type == 'file_upload' ){
			
			$error = $_FILES['form8']['tmp_name']['error'][$element_id];
			$tmp_file = $_FILES['form8']['tmp_name']['reg_field'][$element_id];
			$size = $_FILES['form8']['size']['reg_field'][$element_id];
			$type = $_FILES['form8']['type']['reg_field'][$element_id];
			$name = $_FILES['form8']['name']['reg_field'][$element_id];

			//Skip if there's an error..
			if( $error !=0 || empty( $tmp_file ) )
				continue;
			
			$value = "See attachment: ".$name."<br/>";
		
			if ( copy( $tmp_file , $name ) ) 
				$attachments[] = $name; //chunk_split(base64_encode(file_get_contents($tmp_file)));
			
		}
		
		$message .= "<strong>$label:</strong>" . "<br/>". $value . "<br/>" . "<br/>";
	
	}
	
	//Add note to where submission came from
	$message .= '<hr/>';
	$message .= '<small>' . 'Form submission: ' . form8_get_form_name( $id ) . '</small>';
	
	wp_mail( get_option('admin_email'), $subject, $message, $headers, $attachments );

	if( $removed_from )
		add_filter('wp_mail_from','wp_mail_smtp_mail_from');
	
	if( $removed_from_name )
		add_filter('wp_mail_from_name','wp_mail_smtp_mail_from_name');
	
}
add_action( 'form8_processed_form', '_form8_email_form_submssion', 10, 3 );
