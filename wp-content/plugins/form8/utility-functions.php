<?php
/**
 * Utility functions
 */

/**
 * A wrapper for get_option
 * 
 * @param string $option Name of the option
 * @param string $default A default value in case option is not found
 * @return mixed The value of the option or the $default value given
 */
function form8_get_option( $option, $default = false ){

	$defaults = array(
			'element_id' => 2,
	);
	$options = get_option( 'form8', $defaults );
	
	$options = wp_parse_args( $options, $defaults );

	if( !isset($options[$option]) )
		return $default;

	return $options[$option];
}

/**
 * A wrapper for get_option
 *
 * @param string $option Name of the option
 * @param string $value The value to change the option to
 * @return bool Whether the option was updated
 */
function form8_update_option( $option, $value ){

	$options = get_option( 'form8', array() );
	$options[$option] = $value;

	return update_option( 'form8', $options );
}

/**
 * Returns an array of forms. A wrapper for get_posts().
 *
 * @return array of post objects of 'form8_form' type.
 */
function form8_get_forms(){
	return get_posts( array( 'post_type' => 'form8_form' ) );
}

/**
 * Returns a form's name (slug).
 * @param int $id The ID of the form
 * @return string The name of the form
 */
function form8_get_form_name( $id ){
	$form = get_post( $id );
	return $form->post_name;
}


/**
 * Returns an form's ID given its name (slug).
 * @param string The name of the form
 * @return int|bool $id The ID of the form or false if it could not be found
 */
function form8_get_id_from_name( $slug ) {
	$forms = get_posts(array(
					'name' => $slug,
					'post_type' => 'form8_form'
			));
	if( !$forms )
		return false;

	$form = array_shift( $forms );

	return (int) $form->ID;
}

/**
 * Return or print mark-up for a select form of existing forms.
 * @uses form8_select_field()
 * @param bool $echo Whether to print the markup
 * @return string The drop-down select field markup
 */
function form8_forms_dropdown( $echo = true ){
	$forms = form8_get_forms();
	$form_id = !empty( $_GET['form_id'] ) ? (int) $_GET['form_id'] : 0;
	$form_options = array();
	
	foreach( $forms as $form ){
		$form_options[$form->ID] = $form->post_name.' ( ID: '.$form->ID.')';
	}
	
	$html = form8_select_field( array(
		'selected' => $form_id,
		'options' => $form_options, 
		'name' => 'form8-edit-form',
		'echo' => 0,
	));
	
	if( $echo )
		echo $html;
	
	return $html;
}

/**
 * Utility function for printing/returning radio boxes
 *
 * The $args array - excepts the following keys
 *
 * * **id** - The id of the radiobox (alias: label_for)
 * * **name** - The name of the radiobox
 * * **checked** - The the value to have checked
 * * **options** - Array of options in 'value'=>'Label' format
 * * **label** - The label for the radiobox field set
 * * **class** - Class to be added to the radiobox field set
 * * **echo** - Whether to print the mark-up or just return it
 * * **help** - Optional, help text to accompany the field.
 *
 * @access private
 * @param $args array The array of arguments
 */
function form8_radio_field( $args ){

	$args = wp_parse_args($args,array(
			'checked'=>'', 'help' => '', 'options'=>'', 'name'=>'', 'echo'=>1,
			'class'=>'', 'label' => '','label_for'=>''
	));

	$id = ( !empty($args['id']) ? $args['id'] : $args['label_for']);
	$name = isset($args['name']) ?  $args['name'] : '';
	$checked = $args['checked'];
	$label = !empty($args['label']) ? '<legend><label>'.esc_html($args['label']).'</label></legend>' : '';
	$class =  !empty($args['class']) ? 'class="'.sanitize_html_class($args['class']).'"'  : '';

	$html = sprintf('<fieldset %s> %s', $class, $label);
	if( !empty($args['options']) ){
		foreach ($args['options'] as $value => $opt_label ){
			$html .= sprintf('<label for="%1$s"><input type="radio" id="%1$s" name="%3$s" value="%4$s" %2$s> <span> %5$s </span></label><br>',
					esc_attr($id.'_'.$value),
					checked($value, $checked, false),
					esc_attr($name),
					esc_attr($value),
					esc_html($opt_label));
		}
	}
	if(!empty($args['help'])){
		$html .= '<p class="description">'.esc_html($args['help']).'</p>';
	}
	$html .= '</fieldset>';

	if( $args['echo'] )
		echo $html;

	return $html;
}


/**
 * Utility function for printing/returning select field
 *
 * The $args array - excepts the following keys
 *
 * * **id** - The id of the select box (alias: label_for)
 * * **name** - The name of the select box
 * * **selected** - The the value to have selected
 * * **options** - Array of options in 'value'=>'Label' format
 * * **multiselect** True or False for multi-select
 * * **label** - The label for the radiobox field set
 * * **class** - Class to be added to the radiobox field set
 * * **echo** - Whether to print the mark-up or just return it
 * * **help** - Optional, help text to accompany the field.
 *
 * @access private
 * @param $args array The array of arguments
 */
function form8_select_field($args){

	$args = wp_parse_args($args,array(
			'selected'=>'', 'help' => null, 'options'=>'', 'name'=>'', 'echo'=>1,
			'label_for'=>'','class'=>'','disabled'=>false,'multiselect'=>false,
			'inline_help' => false
	));

	$id = ( !empty($args['id']) ? $args['id'] : $args['label_for']);
	$name = isset($args['name']) ?  $args['name'] : '';
	$selected = $args['selected'];
	$classes = array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) );
	$class = implode( ' ', $classes );
	$multiselect = ($args['multiselect'] ? 'multiple' : '' );
	$disabled = ($args['disabled'] ? 'disabled="disabled"' : '' );

	$html = sprintf('<select %s name="%s" id="%s" %s>',
			!empty( $class ) ? 'class="'.$class.'"'  : '',
			esc_attr($name),
			esc_attr($id),
			$multiselect.' '.$disabled
	);
	if( !empty( $args['show_option_all'] ) ){
		$html .= sprintf('<option value="" %s> %s </option>',selected( empty($selected), true, false ), esc_html( $args['show_option_all'] ) );
	}

	if( !empty($args['options']) ){
		foreach ($args['options'] as $value => $label ){
			if( $args['multiselect'] && is_array($selected) )
				$_selected = selected( in_array($value, $selected), true, false);
			else
				$_selected =  selected($selected, $value, false);

			$html .= sprintf('<option value="%s" %s> %s </option>',esc_attr($value),$_selected, esc_html($label));
		}
	}
	$html .= '</select>'. $args['inline_help'];

	if( isset( $args['help'] ) ){
		$html .= '<p class="description">'.esc_html($args['help']).'</p>';
	}

	if( $args['echo'] )
		echo $html;

	return $html;
}


/**
 * Utility function for printing/returning text field
 *
 * The $args array - excepts the following keys
 *
 * * **id** - The id of the select box (alias: label_for)
 * * **name** - The name of the select box
 * * **value** - The value of the text field
 * * **type** - The type  of the text field (e.g. 'text','hidden','password')
 * * **options** - Array of options in 'value'=>'Label' format
 * * **label** - The label for the radiobox field set
 * * **class** - Class to be added to the radiobox field set
 * * **echo** - Whether to print the mark-up or just return it
 * * **help** - Optional, help text to accompany the field.
 *
 * @access private
 * @param $args array The array of arguments
 */
function form8_text_field($args){

	$args = wp_parse_args( $args,
			array(
					'type' => 'text', 'value'=>'', 'placeholder' => '','label_for'=>'', 'inline_help' => false,
					'size'=>false, 'min' => false, 'max' => false, 'style'=>false, 'echo'=>true, 'data'=>false,
					'class' => false
			)
	);

	$id = ( !empty($args['id']) ? $args['id'] : $args['label_for']);
	$name = isset($args['name']) ?  $args['name'] : '';
	$value = $args['value'];
	$type = $args['type'];
	$classes = array_map( 'sanitize_html_class', explode( ' ', $args['class'] ) );
	$class = implode( ' ', $classes );

	$min = (  $args['min'] !== false ?  sprintf('min="%d"', $args['min']) : '' );
	$max = (  $args['max'] !== false ?  sprintf('max="%d"', $args['max']) : '' );
	$size = (  !empty($args['size']) ?  sprintf('size="%d"', $args['size']) : '' );
	$style = (  !empty($args['style']) ?  sprintf('style="%s"', $args['style']) : '' );
	$placeholder = ( !empty($args['placeholder']) ? sprintf('placeholder="%s"', $args['placeholder']) : '');
	$disabled = ( !empty($args['disabled']) ? 'disabled="disabled"' : '' );

	//Custom data-* attributes
	$data = '';
	if( !empty( $args['data'] ) && is_array( $args['data'] ) ){
		foreach( $args['data'] as $key => $attr_value ){
			$data .= sprintf( 'data-%s="%s"', esc_attr( $key ), esc_attr( $attr_value ) );
		}
	}

	$attributes = array_filter( array($min,$max,$size,$placeholder,$disabled, $style, $data ) );

	$html = sprintf('<input type="%s" name="%s" class="%s regular-text ltr" id="%s" value="%s" autocomplete="off" %s /> %s',
			esc_attr( $type ),
			esc_attr( $name ),
			$class,
			esc_attr( $id ),
			esc_attr( $value ),
			implode(' ', $attributes),
		 $args['inline_help']
	);

	if( isset($args['help']) ){
		$html .= '<p class="description">'.$args['help'].'</p>';
	}

	if( $args['echo'] )
		echo $html;

	return $html;
}


/**
 * Utility function for printing/returning text field
 *
 * The $args array - excepts the following keys
 *
 * * **id** - The id of the checkbox (alias: label_for)
 * * **name** - The name of the select box
 * * **options** - Single or Array of options in 'value'=>'Label' format
 * * **values** - The values of the text field
 * * **type** - The type  of the text field (e.g. 'text','hidden','password')

 * * **label** - The label for the radiobox field set
 * * **class** - Class to be added to the radiobox field set
 * * **echo** - Whether to print the mark-up or just return it
 * * **help** - Optional, help text to accompany the field.
 *
 * @access private
 * @param $args array The array of arguments
 */
function form8_checkbox_field($args=array()){

	$args = wp_parse_args($args,array(
			'help' => '','name'=>'', 'class'=>'',
			'checked'=>'', 'echo'=>true,'multiselect'=>false
	));

	$id = ( !empty($args['id']) ? $args['id'] : $args['label_for']);
	$name = isset($args['name']) ?  $args['name'] : '';
	$class = ( $args['class'] ? "class='".sanitize_html_class($args['class'])."'"  :"" );

	/* $options and $checked are either both arrays or they are both strings. */
	$options =  isset($args['options']) ? $args['options'] : false;
	$checked =  isset($args['checked']) ? $args['checked'] : 1;

	$html ='';
	if( is_array($options) ){
		foreach( $options as $value => $opt_label ){
			$html .= sprintf('<label for="%1$s">
								<input type="checkbox" name="%2$s" id="%1$s" value="%3$s" %4$s %5$s>
								%6$s </br>
							</label>',
					esc_attr($id.'_'.$value),
					esc_attr(trim($name).'[]'),
					esc_attr($value),
					checked( in_array($value, $checked), true, false ),
					$class,
					esc_attr($opt_label)
			);
		}
	}else{
		$html .= sprintf('<input type="checkbox" id="%1$s" name="%2$s" %3$s %4$s value="%5$s">',
				esc_attr($id),
				esc_attr($name),
				checked( $checked, $options, false ),
				$class,
				esc_attr($options)
		);
	}

	if(!empty($args['help'])){
		$html .= '<p class="description">'.$args['help'].'</p>';
	}

	if( $args['echo'] )
		echo $html;

	return $html;
}



/**
 * Utility function for printing/returning text area
 *
 * The $args array - excepts the following keys
 *
 * * **id** - The id of the checkbox (alias: label_for)
 * * **name** - The name of the select box
 * * **options** - Single or Array of options in 'value'=>'Label' format
 * * **tinymce** Whether to use the TinyMCE editor. The TinyMCE prints directly.
 * * **value** - The value of the text area
 * * **rows** - The number of rows. Default 5.
 * * **cols** - The number of columns. Default 50.
 * * **class** - Class to be added to the textarea
 * * **echo** - Whether to print the mark-up or just return it
 * * **help** - Optional, help text to accompany the field.
 *
 * @access private
 * @param $args array The array of arguments
 */
function form8_textarea_field($args){

	$args = wp_parse_args($args,array(
	 	'type' => 'text', 'value'=>'', 'tinymce' => '', 'help' => '',
			'class'=>'large-text', 'echo'=>true,'rows'=>5, 'cols'=>50
	));

	$id = ( !empty($args['id']) ? $args['id'] : $args['label_for']);
	$name = isset($args['name']) ?  $args['name'] : '';
	$value = $args['value'];
	$class = $args['class'];
	$html ='';

	if( $args['tinymce'] ){
		wp_editor( $value, esc_attr($id) ,array(
		'textarea_name'=>$name,
		'media_buttons'=>false,
		));
	}else{
		$html .= sprintf('<textarea cols="%s" rows="%d" name="%s" class="%s large-text" id="%s">%s</textarea>',
				intval($args['cols']),
				intval($args['rows']),
				esc_attr($name),
				sanitize_html_class($class),
				esc_attr($id),
				esc_textarea($value)
		);
	}

	if(!empty($args['help'])){
		$html .= '<p class="description">'.$args['help'].'</p>';
	}

	if( $args['echo'] )
		echo $html;

	return $html;
}

function form8_php2jquerydate( $phpformat = "" ){

	$php2jquerydate = array(
			'Y'=>'yy','y'=>'y','L'=>''/*Not Supported*/,'o'=>'',/*Not Supported*/
			'j'=>'d','d'=>'dd','D'=>'D','DD'=>'dddd','N'=>'',/*NS*/ 'S' => ''/*NS*/,
			'w'=>'', /*NS*/ 'z'=>'o',/*NS*/ 'W'=>'w',
			'F'=>'MM','m'=>'mm','M'=>'M','n'=>'m','t'=>'',/*NS*/
			'a'=>''/*NS*/,'A'=>''/*NS*/,
			'B'=>'',/*NS*/'g'=>''/*NS*/,'G'=>''/*NS*/,'h'=>''/*NS*/,'H'=>''/*NS*/,'u'=>'fff',
			'i'=>''/*NS*/,'s'=>''/*NS*/,
			'O'=>''/*NS*/, 'P'=>''/*NS*/,
	);

	$jqueryformat="";

	for($i=0;  $i< strlen($phpformat); $i++){

		//Handle backslash excape
		if($phpformat[$i]=="\\"){
			$jqueryformat .= "\\".$phpformat[$i+1];
			$i++;
			continue;
		}

		if(isset($php2jquerydate[$phpformat[$i]])){
			$jqueryformat .= $php2jquerydate[$phpformat[$i]];
		}else{
			$jqueryformat .= $phpformat[$i];
		}
	}
	return $jqueryformat;
}
?>
