<?php
/**
 * Admin Functions
 * 
 * Handles the admin pages, and admin actions (create/edit/delete form handlers)
 */


/**
 * Adds the form admin page to the menu (under tools).
 * Hooked onto admin_menu
 */
function form8_add_admin_pages(){
	add_management_page( 'Forms', 'Forms', 'manage_options', 'form8-admin', 'form8_form_management_page' );
}
add_action( 'admin_menu', 'form8_add_admin_pages');


/**
 * Registers admin scripts/styles. Hooked onto init.
 */
function form8_register_admin_scripts(){
	wp_register_script( 'form8-form-customiser',FORM8_URL.'form-customiser.js',
		array(
			'jquery','jquery-ui-draggable','jquery-ui-sortable','jquery-ui-droppable',
			'jquery-ui-mouse','jquery-ui-button','jquery-ui-widget'
		) 
	);
	
	/* Register jQuery UI style */
	if ( 'classic' == get_user_option( 'admin_color') )
		wp_register_style('form8-jquery-ui-style', FORM8_URL.'css/form8-admin-classic.css',array());
	else
		wp_register_style('form8-jquery-ui-style', FORM8_URL.'css/form8-admin-fresh.css',array());
	
	wp_register_style( 'form8-form-customiser',FORM8_URL.'css/form-customiser.css', array( 'form8-jquery-ui-style' ) );
}
add_action( 'admin_init', 'form8_register_admin_scripts' );


/**
 * Loads admin scripts/styles on the admin page. Hooked onto admin_enqueue_scripts.
 */
function form8_admin_enqueue_scripts($hook) {
	if( 'tools_page_form8-admin' == $hook ){
		wp_enqueue_style( 'form8-form-customiser' );
		wp_enqueue_script( 'form8-form-customiser' );
	}
}
add_action( 'admin_enqueue_scripts', 'form8_admin_enqueue_scripts' );


/**
 * Admin action listener
 * 
 * Handles admin page requests to edite/create/delete forms. Hooked onto admin_init.
 * @TODO check nonces
 */
function _form8_action_listener(){
	
	if( isset($_POST['form8-save-form']) ){
		
		$fields = !empty( $_POST['form8_field'] ) ?  $_POST['form8_field'] : array();
		$form_settings = !empty( $_POST['form8_settings'] ) ?  $_POST['form8_settings'] : array();
		$form_name = !empty( $_POST['form8-form-name'] ) ?  sanitize_text_field( $_POST['form8-form-name'] ) : '';
		$form_id = (int) $_POST['form8-form-id'];
		
		if( $form_id && $fields ){
			update_post_meta( $form_id, '_form8_fields', $fields );
		}
		
		$form_submitted = $form_settings['form_submitted'];
		$form_button = $form_settings['form_button'];
		$form_submitted_classes = $form_settings['form_submitted_classes'];
		$form_error_classes = $form_settings['form_error_classes'];
		$form_button_classes = $form_settings['form_button_classes'];
		update_post_meta( $form_id, '_form8_form_submitted', $form_submitted );
		update_post_meta( $form_id, '_form8_form_submitted_classes', $form_submitted_classes );
		update_post_meta( $form_id, '_form8_form_error_classes', $form_error_classes );
		update_post_meta( $form_id, '_form8_form_button', $form_button );
		update_post_meta( $form_id, '_form8_form_button_classes', $form_button_classes );
		
		//delete_post_meta( $form_id, '_form8_fields' );

		wp_update_post( array( 'ID' => $form_id, 'post_name' => $form_name, 'post_title' => $form_name ) );
		
		//wp_die(var_dump($form_name));
		
		$redirect = add_query_arg( 'form_id', $form_id, admin_url( 'tools.php?page=form8-admin' ) );
		wp_redirect( $redirect );
		exit();
		
	}elseif( isset($_REQUEST['action']) && 'form8-create-form' == $_REQUEST['action'] ){
		$id = wp_insert_post( array( 'post_status' => 'publish', 'post_type' => 'form8_form', 'post_title' => 'New-Form' ) );
		$redirect = add_query_arg( 'form_id', $id, admin_url( 'tools.php?page=form8-admin' ) );
		wp_redirect( $redirect );
		exit();
		
	}elseif( !empty($_POST['form8-edit-form']) ){
		$id = (int) $_POST['form8-edit-form'];
		$redirect = add_query_arg( 'form_id', $id, admin_url( 'tools.php?page=form8-admin' ) );
		wp_redirect( $redirect );
		exit();
		
	}elseif( !empty($_GET['form8-delete-form']) ){
		$id = (int) $_GET['form8-delete-form'];
		wp_delete_post( $id, true );
		wp_redirect( admin_url( 'tools.php?page=form8-admin' ) );
		exit();
	}
}
add_action( 'admin_init', '_form8_action_listener');

/**
 * Function responisible for displaying the admin page.
 */
function form8_form_management_page(){

	echo '<div class="wrap">';
	
		screen_icon();
		
		printf(
				'<h2> %s <a href="%s" class="add-new-h2" > %s </a></h2>',
				esc_html__('Forms','form8'),
				esc_url( wp_nonce_url( admin_url( 'tools.php?page=form8-admin&action=form8-create-form'), 'form8-create-form' ) ),
				esc_html__('Add New','form8')
		);
	 
	    echo '<form name="form8" method="post">';
	    $form_id = !empty( $_GET['form_id'] ) ? (int) $_GET['form_id'] : 0;
	    $form = new form8_Form( $form_id );
		$form->display_customiser();
	  	echo '</form>';
	 
	echo '</div><!-- .wrap -->';
}