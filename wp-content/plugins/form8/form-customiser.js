(function($) {

form8FormCustomiser = {
	init : function(){

	if( $("#form8-form-fields").length > 0 ){
   	 	$( window ).on( 'scroll click.postboxes', function( event ) {
			var y =  parseInt( $(this).scrollTop() );
			var footer_top = parseInt( $( '#wpfooter' ).position().top );
			var fixed_offset = parseInt( $("#form8-form-fields").offset().top );
			var metabox_height = parseInt( $('#form8-form-fixed-mb').height() );
			var bar = y + fixed_offset + metabox_height;

        	if ( footer_top - bar >  -20 ) {
				 $('#form8-form-fixed-mb').css({
               	   			 position: 'fixed',
               	   		  	top: $("#form8-form-fields").offset().top
                		});
			 } else {
              			$('#form8-form-fixed-mb').css({
                  			 position: 'absolute',
                  	  		top: ( footer_top - metabox_height  ) + 'px'
                		});
			}
		}).trigger( 'click.postboxes' );
	}
	if( $('#form8-toggle-form').length > 0){
		$('#form8-toggle-form').css('font-weight','bold').click(function(e){
			e.preventDefault();
			$(this).css('font-weight','bold');
			$('#form8-toggle-settings').css('font-weight','normal');
			$('#form8-settings').hide();
			$('#form8-form').show();
		});
		$('#form8-toggle-settings').click(function(e){
			e.preventDefault();
			$(this).css('font-weight','bold');
			-$('#form8-toggle-form').css('font-weight','normal');
			$('#form8-form').hide();
			$('#form8-settings').show();
		})
	}
		/* Init the sortable */
		$( "#form8-form-fields ul#form8-form" ).sortable({
			revert: true,
			axis: 'y',
			handle:'h3',
			containment:'#form8-form-fields',
			placeholder: "sortable-placeholder field-placeholder"
		});

			$("span.field-option-add").button({
                		icons: {primary: "ui-icon-plusthick"},
                		text: false
            		});
			$("span.field-option-remove").button({
                		icons: {primary: "ui-icon-minusthick"},
				text: false
            		});

		/* Toggle the visibility of edit options */
		$('#form8-form-fields').on('click','.postbox .inside, .handlediv',function(e){
			e.preventDefault();
			$(this).parents('.postbox').find('.edit').toggle();
			$("html, body").animate({ scrollTop: $(this).parents('.postbox').offset().top-50 }, 400);
		});

		$('#form8-form-fields').on('click','.postbox .inside .edit', function (e){
			e.stopPropagation();
		});

		/* Listen for selecting the default option of radio/selected/checkbox */
		$('#form8-form-fields').on('click','.postbox .inside .edit input.field-option-selected',function(e){
			var index = $(this).parent('li').index();
			var check = $(this).is(":checked");
			$(this).parents('.inside').find('.example.example-radiobox li:eq('+index+') input:radio').attr('checked', check);
			$(this).parents('.inside').find('.example option:eq('+index+')').attr('selected', check);
			$(this).parents('.inside').find('.example select').focus().blur();
			$(this).parents('.inside').find('.example.example-checkbox li:eq('+index+') input:checkbox').attr('checked', check);
		}).trigger('click');

		/* Address field: listen for select / deselect of address compontent */
		$('#form8-form-fields').on('click','.postbox .inside .edit input.field-address',function(e){
			var index = $(this).parent('label').index();
			var check = $(this).is(":checked");
			$(this).parents('.inside').find('.example .example-address-components label:eq('+index+')').toggle(check);
		});

		/* Address field: listen for select / deselect of name compontent */
		$('#form8-form-fields').on('click','.postbox .inside .edit input.field-profile-name',function(e){
			var index = $(this).parent('label').index();
			var check = $(this).is(":checked");
			$(this).parents('.inside').find('.example .example-profile-name-components label:eq('+index+')').toggle(check);
		});
		

		/* Adding choice */
		$('#form8-form-fields').on('click','.postbox .inside .edit .field-option-add',function(e){
			var id = $(this).parent('li').find('.field-option-label').attr('id').substr(13);
			var index = parseInt($(this).parent('li').index());
			//The remove buttons are hidden when we're down to one item. We have it least two no, so ensure its shown
			$(this).parents('ul.field-options').find('.field-option-remove').show();
			$(this).parent('li').after('<li>'
				+'<input type="radio" class="field-option-selected" name="form8_field['+id+'][selected]" id="field-option-selected-'+id+' value="'+(index+1)+'">'
				+' <input type="text" id="field-option-'+id+'" value="New Option" class="field-option-label" name="form8_field['+id+'][options][]">'
				+' <span class="field-option-add" alt="add another choice" style="height: 18px;vertical-align: bottom;margin-bottom: 3px;"></span>'
				+' <span class="field-option-remove" alt="remove another choice" style="height: 18px;vertical-align: bottom;margin-bottom: 3px;"></span>'
				+'</li>');
			$(this).parents('.inside').find('.example option:eq('+index+')').after('<option value="New Option">New Option</option>'); 
			$(this).parents('.inside').find('.example.example-radiobox li:eq('+index+')').after('<li><input type="radio" name="form8-example-radio-'+id+'" disabled="disabled"><label>New Option</label></li>'); 
			$(this).parents('.inside').find('.example.example-checkbox li:eq('+index+')').after('<li><input type="checkbox" name="form8-example-radio-'+id+'" disabled="disabled"><label>New Option</label></li>'); 
			$("span.field-option-add").button({
                		icons: {primary: "ui-icon-plusthick"},
                		text: false
            		});
			$("span.field-option-remove").button({
                		icons: {primary: "ui-icon-minusthick"},
				text: false
            		});
		});

		/*Update description, label & place holder and header*/
		$('#form8-form-fields').on('keyup','.postbox .edit .field-placeholder, .postbox .edit .field-description, .postbox .edit .field-label, .postbox .edit .field-option-label, .postbox .edit .field-header',function(e){

			if( $(this).hasClass('field-placeholder') ){
				$(this).parents('.inside').find('.example input').attr('placeholder',$(this).val());

			}else if( $(this).hasClass('field-description') ){
				$(this).parents('.inside').find('.example p.description').html($(this).val());

			}else if( $(this).hasClass('field-label') ){
				$(this).parents('.inside').find('.example label.field-label').text($(this).val());

			}else if( $(this).hasClass('field-header') ){
				$(this).parents('.inside').find('.example h2.field-header').text($(this).val());

			}else if( $(this).hasClass('field-option-label') ){
				var index = $(this).parent('li').index();
				$(this).parents('.inside').find('.example li:eq('+index+') label').text($(this).val());
				$(this).parents('.inside').find('.example option:eq('+index+')').text($(this).val());
				$(this).parents('.inside').find('.example option:eq('+index+')').val($(this).val());
			}
		});

		$('#form8-form-fields').on('click','.postbox .edit .field-required',function(e){
				$(this).parents('.inside').find('.example span.required').toggle($(this).checked);
		});

		/* Removing dropdown choice */
		$('#form8-form-fields').on('click','.postbox .inside .edit .field-option-remove',function(e){
			var index = $(this).parent('li').index();
			var number = $(this).parents('ul.field-options').find('li').length;
			if( number == 2 ){
				//Once removed, they'll only be one item left. So hide the removal button.
				$(this).parents('ul.field-options').find('.field-option-remove').hide();
			}else if( number <=1 ){
				//This is the last item, don't remove it!
				return false;
			}
			$(this).parents('.inside').find('.example li:eq('+index+')').remove();
			$(this).parents('.inside').find('.example option:eq('+index+')').remove();
			$(this).parent('li').remove();
		});

		/* Removing field */
		$('#form8-form').on('click','.submitdelete',function(e){
			e.preventDefault();
			var r=confirm("Are you sure you wish to delete this field?\n\n Deleting this field will delete all corresponding data from existing bookings.");
			if (r==true){	
				$(this).parents('li').remove();
			  }
			});
		

		/* Removing form */
		$('#form8-customiser-header').on('click','.submitdelete',function(e){
			var r=confirm("Are you sure you wish to delete this form?\n\n");
			if( !r ){
				e.preventDefault();
			}
			
		});


		/* The 'bin' of form elements */
		$( "#form8-form-fixed-mb .form8-field-bin li" ).click(function(e){
			e.preventDefault();
			var field_type = $(this).attr('id').substring(17);
			$.post(ajaxurl, {
					action: 'form8-form-add-element',
					element_type: field_type
				}, function (response) {
					$('#form8-form-fields ul#form8-form').append(response);
					$("html, body").animate({ scrollTop: $('#form8-form-fields ul#form8-form').prop("scrollHeight")-50 }, 400);

					$("span.field-option-add").button({
		                		icons: {primary: "ui-icon-plusthick"},
		                		text: false
		            		});
					$("span.field-option-remove").button({
		                		icons: {primary: "ui-icon-minusthick"},
						text: false
		            		});
                		});
			});
	}
};
$(document).ready(function(){
	$('#side-sortables .postbox h3, #side-sortables .postbox .handlediv').bind('click.postboxes', function() {
		var p = $(this).parent('.postbox');
		p.toggleClass('closed');
	});
	$('#side-sortables .postbox :gt(0)').toggleClass('closed');
	form8FormCustomiser.init();
});
})(jQuery);
