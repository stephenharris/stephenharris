<?php
/**
 * Plugin Name: Series Shortcodes
 * Description: Adds useful shortcodes to be used with the 'Series' plug-in by Justin Tadlock.
 * Version: 0.1.0
 * Author: Stephen Harris
 * Author URI: http://stephenharris.ifno
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation; either version 2 of the License, 
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write 
 * to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

/* Register shortcodes on 'init'. */
add_action( 'init', 'series_shortcodes_register_shortcodes' );

/**
 * Registers the plugin's shortcodes with WordPress.
 */
function series_shortcodes_register_shortcodes() {
	add_shortcode( 'in-this-series', 'series_shortcode_in_this_series_shortcode' );
}

/**
 * Gets the series terms of the current post and displays them. *
 */
function series_shortcode_in_this_series_shortcode( $attr ) {

	$terms = get_the_terms( get_the_ID(), 'series' );

 	if(  !$terms || is_wp_error( $terms ) ){
		return;
	}

	$series = array_shift( $terms );

	$posts = get_posts( array(
		'order'		=> 'asc',
		'tax_query' 	=> array(
			array(
				'taxonomy' 	=> 'series',
				'field'			=> 'term_id',
				'terms'		=> (int) $series->term_id
			)
		)
	) );

	$html = sprintf( '<ol class="series series-%s series-id-%d">', $series->slug, $series->term_id );

	foreach( $posts as $post ){

		if( $post->ID == get_the_ID() ){
			$html .= sprintf( '<li class="current-post-in-series">%s</a></li>', get_the_title( $post->ID ) );
		}else{
			$html .= sprintf( '<li><a href="%s">%s</a></li>', get_permalink($post->ID), get_the_title( $post->ID ) );
		}

	}

	$html .= '</ol>';

	return $html;
}
