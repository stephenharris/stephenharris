<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Publish
 * @since Publish 1.0
 */

get_header(); ?>

		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="entry-content">
						<video width="525" controls>
							<source src="<?php echo esc_url( wp_get_attachment_url( get_the_ID() ) ); ?>" type='<?php echo esc_attr( get_post_mime_type( $ID ) );?>'></source>
							<?php //this are here as a bit of hack to get the video to render. No 3gp love? // ?>
							<source src="<?php echo esc_url( wp_get_attachment_url( get_the_ID() ) ); ?>" type='video/mp4'></source>
							<source src="<?php echo esc_url( wp_get_attachment_url( get_the_ID() ) ); ?>" type='video/ogg'></source>
						</video>
						<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'publish' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'publish' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post-<?php the_ID(); ?> -->

				<?php publish_content_nav( 'nav-below' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template( '', true );
				?>

			<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
