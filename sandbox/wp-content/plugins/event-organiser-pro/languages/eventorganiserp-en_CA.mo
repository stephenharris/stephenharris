��    Q      �  m   ,      �     �     �               3  
   B     M  
   [  
   f  )   q     �     �     �     �     �     �     �     �     �               '     5     G     V     d     p  0   y  #   �  (   �  "   �  3   	     N	     ^	     p	     �	     �	     �	     �	  C   �	     �	     
     
     *
     A
     a
     s
     �
     �
  '   �
     �
     �
  %   �
     #     8  .   H     w     �     �     �  T   �  E     =   Z  W   �     �  �   	  \   �  �   �  R   z     �     �     �     �                0  �   >  /   �     !  (   @  p  i     �     �     �     
       
   +     6  
   D  
   O     Z     z          �  
   �     �  	   �     �     �  	   �  
   �  	   �  
   �     �                    &  -   ,     Z     s     �  .   �     �     �     �  
                  #  @   0     q  	   �     �     �     �     �     �     �     �          *     8  %   R     x     �  +   �     �     �     �     �  Q   �  B   D  ;   �  X   �       ~   7  P   �  �     N   �     �  	   �                     3  
   C  �   N  &   �  !   !     C     P          ;   B   !   J   /      L   	         A         ?       '   M          :             *   7                 +   &   I   <           N   6   Q   
   G   -            @   E   8   C   2                                        K   ,      D   F                  "          #   9   %      4       )       H      5                 >      .   1   3                  O       =       $   (   0              A booking is confirmed A new booking is made Add New Booking Add New Booking Form All categories All cities All countries All states All venues Allow Logged-out Users to place bookings? Book Bookee Booking Booking (#%d) Booking Confirmed Booking Date Booking Date (UTC) Booking Details Booking Form Booking Forms Booking Meta Booking Notes Booking Reference Booking Status Booking email Booking ref Bookings Bookings are no longer available for this event. Bookings for event &#8220;%s&#8221; Bookings for event &#8220;%s&#8221; (%s) Bookings for user &#8220;%s&#8221; Bookings will be closed when an occurrences starts. Cancel Bookings Download bookings Edit Booking Forms Edit Bookings Email Email Bookees Export bookings In the e-mail template you can use booking information placeholders New Booking Forms New Bookings New event booking No Booking Forms found No Booking Forms found in Trash No Bookings found No Bookings found in Trash No bookings No bookings found. Only logged in users can place bookings Orphaned booking Please provide a name. Please provide a valid email address. Search Booking Forms Search Bookings Select booking fields to be included in export State/Province Street Address Street address Thank you for your booking The event associated with this booking could not be found. It may have been deleted. The occurrence date for which this booking was made no longer exists. This booking could not be confirmed for the following reasons This email is already registered, please choose another one or log-in with that account This event has sold out. This form element allows the user to select their ticket(s). It cannot be removed but can be placed anywhere in the booking form. This form element asks the user a simple random maths question in order to help prevent spam This form element only appears if there there are multiple gateways enabled. It cannot be removed but can be placed anywhere in the booking form. This will appear on the booking form, informing the bookee on how to make payment. Username / Email View Booking View Booking Form View all cities View all countries View all states View bookings You can add additional fields to your booking form using the buttons to the right, and then arrange them by dragging the fields. Click the added form field to reveal its options  You have already made a booking for this event. You have not made any bookings You must be logged in to place a booking Project-Id-Version: Event Organiser Pro
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-13 10:15+0100
PO-Revision-Date: 2013-07-02 12:56:15+0000
Last-Translator: Erica <ericaeide@gmail.com>
Language-Team: 
Language: English (Canada)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 An RSVP is made A new RSVP is made Add New RSVP Add new RSVP Form All Categories All Cities All Countries All States All Venues Allow logged-out users to RSVP? RSVP RSVP'r RSVP RSVP (#%d) RSVP Confirmed RSVP Date RSVP Date (UTC) RSVP Details RSVP Form RSVP Forms RSVP Meta RSVP Notes RSVP Reference RSVP Status RSVP e-mail RSVP ref RSVPs RSVPs are no longer available for this event. RSVPs for event “%s” RSVPs for event “%s” (%s) RSVPs for user “%s” RSVPs will be closed when an occurence starts. Cancel RSVPs Download RSVPs Edit RSVP Forms Edit RSVPs E-mail E-mail RSVPs Export RSVPs In the e-mail template you can use RSVP information placeholders New RSVP Forms New RSVPs New Event RSVP No RSVP Forms found No RSVP Forms found in trash. No RSVPs found No RSVPs found in trash No RSVPs No RSVPs found. Only logged in users can RSVP. Orphaned RSVP Please provide your name. Please provide a valid e-mail address Search RSVP Forms Search RSVPs Select RSVP fields to be included in export State Address Address Thank you for your RSVP! The event associated with this RSVP could not be found. It may have been deleted. The occurrence date for which this RSVP was made no longer exists. This RSVP could not be confirmed for the following reasons: This e-mail is already registered, please choose another one or log-in with that account Sorry, this event is full. This form element allows the user to select their ticket(s). It cannot be removed but can be placed anywhere in the RSVP form. This form element asks the user a simple math question in order to prevent spam. This form element only appears if there there are multiple gateways enabled. It cannot be removed but can be placed anywhere in the RSVP form. This will appear on the RSVP form, informing the bookee how to make a payment. Username / E-mail View RSVP View RSVP Form View all Cities View all Countries View all States View RSVPs You can add additional fields to your RSVP form using the buttons to the right, then arrange them by dragging the fields. Click the added form field to reveal its options. You have already RSVP'd to this event. You have not RSVP'd to any events You must be logged in to RSVP 