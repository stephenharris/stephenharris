<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<ul id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id );?>" class="eo-booking-field-checkbox-list">	
	<?php foreach( $this->get_options() as $value => $label ) : ?>
		<li>
			<label>
				<input 
					type="checkbox"
					name="<?php echo esc_attr( $this->get_name() );?>[]"
					class="<?php echo esc_attr( $this->get_class() );?>"
					value="<?php echo esc_attr( $value );?>"
					style="<?php echo esc_attr( $this->element->get( 'style' ) );?>"
					<?php $this->checked( $value ); ?>
	 				/>
	 			<?php echo esc_html( $label ); ?>
			</label>
		</li>
	<?php endforeach; ?>
</ul>
	
<?php include( eo_locate_template( 'eo-booking-form-description.php' ) ); ?>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>