<?php 

?>

<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<?php include( eo_locate_template( 'eo-booking-form-description.php' ) );?>

<span class="eo-booking-form-address-components">

	<?php if( in_array( 'street-address', $this->element->get( 'components' ) ) ): ?>
		<p>
			<label class="eo-booking-sub-label" for="<?php echo esc_attr( 'eo-booking-field-street-address-'.$this->element->id );?>">  
				<?php $this->label( 'street-address' ); ?>
				<?php if( $this->element->is_required( 'street-address' ) ):?>
					<span class="required eo-booking-form-element-required">*</span>
				<?php endif; ?>
			</label> 
			<input
				type="text"
				id="<?php echo esc_attr( 'eo-booking-field-street-address-'.$this->element->id );?>"
				name="<?php echo esc_attr( $this->get_name( 'street-address' ) );?>"
				class="<?php echo esc_attr( $this->get_class() );?>"
				placeholder="<?php esc_attr_e( 'Street address', 'eventorganiserp' ); ?>"
				value="<?php echo esc_attr( $this->element->get_value( 'street-address' ) );?>"
			/>
		</p>
	<?php endif; ?>
		
	<?php if( in_array( '2nd-line', $this->element->get( 'components' ) ) ): ?>
		<p>
			<label class="eo-booking-sub-label" for="<?php echo esc_attr( 'eo-booking-field-2nd-line-'.$this->element->id );?>">  
				<?php $this->label( '2nd-line' ); ?>
				<?php if( $this->element->is_required( '2nd-line' ) ):?>
					<span class="required eo-booking-form-element-required">*</span>
				<?php endif; ?>
			</label> 
			<input
				type="text"
				id="<?php echo esc_attr( 'eo-booking-field-2nd-line-'.$this->element->id );?>"
				name="<?php echo esc_attr( $this->get_name( '2nd-line' ) );?>"
				class="<?php echo esc_attr( $this->get_class() );?>"
				value="<?php echo esc_attr( $this->element->get_value( '2nd-line' ) );?>"
			/>
		</p>
	<?php endif; ?>
		
	<?php if( in_array( 'city', $this->element->get( 'components' ) ) ): ?>
		<p>
			<label class="eo-booking-sub-label" for="<?php echo esc_attr( 'eo-booking-field-city-'.$this->element->id );?>">  
				<?php $this->label( 'city' ); ?>
				<?php if( $this->element->is_required( 'city' ) ):?>
					<span class="required eo-booking-form-element-required">*</span>
				<?php endif; ?>
			</label> 
			<input
				type="text"
				id="<?php echo esc_attr( 'eo-booking-field-city-'.$this->element->id );?>"
				name="<?php echo esc_attr( $this->get_name( 'city' ) );?>"
				class="<?php echo esc_attr( $this->get_class() );?>"
				placeholder="<?php esc_attr_e( 'City', 'eventorganiser' ); ?>"
				value="<?php echo esc_attr( $this->element->get_value( 'city' ) );?>"
			/>
		</p>
	<?php endif; ?>
		
	<p class="eo-booking-form-address-component-state-postcode">
	
		<?php if( in_array( 'state', $this->element->get( 'components' ) ) ): ?>
		<span class="eo-booking-form-address-component-state">
			<label class="eo-booking-sub-label" for="<?php echo esc_attr( 'eo-booking-field-state-'.$this->element->id );?>">  
				<?php $this->label( 'state' ); ?>
				<?php if( $this->element->is_required( 'state' ) ):?>
					<span class="required eo-booking-form-element-required">*</span>
				<?php endif; ?>
			</label> 
			<input
				type="text"
				id="<?php echo esc_attr( 'eo-booking-field-state-'.$this->element->id );?>"
				name="<?php echo esc_attr( $this->get_name( 'state' ) );?>"
				class="<?php echo esc_attr( $this->get_class() );?>"
				placeholder="<?php esc_attr_e( 'State/Province', 'eventorganiserp' ); ?>"
				value="<?php echo esc_attr( $this->element->get_value( 'state' ) );?>"
			/>
		</span>
		<?php endif; ?>
		
		<?php if( in_array( 'postcode', $this->element->get( 'components' ) ) ): ?>
		<span class="eo-booking-form-address-component-postcode">
			<label class="eo-booking-sub-label" for="<?php echo esc_attr( 'eo-booking-field-postcode-'.$this->element->id );?>">  
				<?php $this->label( 'postcode' ); ?>
				<?php if( $this->element->is_required( 'postcode' ) ):?>
					<span class="required eo-booking-form-element-required">*</span>
				<?php endif; ?>
			</label> 
			<input
				type="text"
				id="<?php echo esc_attr( 'eo-booking-field-postcode-'.$this->element->id );?>"
				name="<?php echo esc_attr( $this->get_name( 'postcode' ) );?>"
				class="<?php echo esc_attr( $this->get_class() );?>"
				placeholder="<?php esc_attr_e( 'Postcode', 'eventorganiserp' ); ?>"
				value="<?php echo esc_attr( $this->element->get_value( 'postcode' ) );?>"
			/>
		</span>
		<?php endif; ?>
	</p>		

	<?php if( in_array( 'country', $this->element->get( 'components' ) ) ): ?>
		<p>
			<label class="eo-booking-sub-label" for="<?php echo esc_attr( 'eo-booking-field-country-'.$this->element->id );?>">  
				<?php $this->label( 'country' ); ?>
				<?php if( $this->element->is_required( 'country' ) ):?>
					<span class="required eo-booking-form-element-required">*</span>
				<?php endif; ?>
			</label> 
			<input
				type="text"
				id="<?php echo esc_attr( 'eo-booking-field-country-'.$this->element->id );?>"
				name="<?php echo esc_attr( $this->get_name( 'country' ) );?>"
				class="<?php echo esc_attr( $this->get_class() );?>"
				placeholder="<?php esc_attr_e( 'Country', 'eventorganiser' ); ?>"
				value="<?php echo esc_attr( $this->element->get_value( 'country' ) );?>"
			/>
		</p>
	<?php endif; ?>

</span>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>