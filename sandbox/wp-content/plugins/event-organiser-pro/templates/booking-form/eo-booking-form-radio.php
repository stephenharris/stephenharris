<?php if( $this->element->show_label  ): ?>
	<label class="eo-booking-label" for="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id );?>">
		<?php $required = ( $this->element->is_required() ? '<span class="required eo-booking-form-element-required">*</span>' : '' ); ?>
		<?php echo esc_html( $this->element->get( 'label' ) ) . $required; ?>				
	</label>
<?php endif; ?>

<ul id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id );?>">	
	<?php foreach( $this->get_options() as $value => $label ) : ?>
	<li>
		<label>
			<input 
				type="radio"
				name="<?php echo esc_attr( $this->get_name() );?>"
				class="<?php echo esc_attr( $this->get_class() );?>"
				value="<?php echo esc_attr( $value );?>"
				style="<?php echo esc_attr( $this->element->get( 'style' ) );?>"
				<?php $this->checked( $value ) ?>
	 		/>
	 		<?php echo esc_html( $label ); ?>
		</label>
	</li>
	<?php endforeach; ?>
</ul>

<?php include( eo_locate_template( 'eo-booking-form-description.php' ) ); ?>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>