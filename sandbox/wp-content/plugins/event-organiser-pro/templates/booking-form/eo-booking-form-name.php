<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<?php if( !$this->element->get( 'lname' ) ): ?>

<input
	type="text"
	id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id.'-fname' );?>"
	name="<?php echo esc_attr( $this->get_name( 'fname' ) );?>"
	class="<?php echo esc_attr( $this->get_class() );?>"
	placeholder="<?php echo esc_attr( $this->element->get( 'placeholder' ) );?>"
	value="<?php echo esc_attr( $this->element->get_value( 'fname' ) );?>"
	style="<?php echo esc_attr( $this->element->get( 'style' ) );?>"
<?php if( $this->element->is_required() ):?>
	required="required"
<?php endif;?>
/>


<?php else: ?>

<p class="eo-booking-field-name-subfields">

	<span class="eo-booking-field-name-subfield">
		<label class="eo-booking-sub-label eo-booking-sub-label-fname" for="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id.'-fname' );?>"> 
			<?php echo esc_html( $this->element->get( 'label_fname' ) ); ?> 
		</label> 
		
		<input
			type="text"
			id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id.'-fname' );?>"
			name="<?php echo esc_attr( $this->get_name( 'fname' ) );?>"
			class="<?php echo esc_attr( $this->get_class() );?>"
			placeholder="<?php esc_attr_e( 'First Name', 'eventorganiserp' );?>"
			value="<?php echo esc_attr( $this->element->get_value( 'fname' ) );?>"
			style="<?php echo esc_attr( $this->element->get( 'style' ) );?>"
		<?php if( $this->element->is_required() ):?>
			required="required"
		<?php endif;?>
		/>
  
	</span>
	
	<span class="eo-booking-field-name-subfield">
		<label class="eo-booking-sub-label eo-booking-sub-label-lname" for="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id.'-lname' );?>"> 
			<?php echo esc_html( $this->element->get( 'label_lname' ) ); ?> 
		</label> 
		
		<input
			type="text"
			id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id.'-lname' );?>"
			name="<?php echo esc_attr( $this->get_name( 'lname' ) );?>"
			class="<?php echo esc_attr( $this->get_class() );?>"
			placeholder="<?php esc_attr_e( 'Last Name', 'eventorganiserp' );?>"
			value="<?php echo esc_attr( $this->element->get_value( 'lname' ) );?>"
			style="<?php echo esc_attr( $this->element->get( 'style' ) );?>"
		<?php if( $this->element->get( 'lname_required' ) ):?>
			required="required"
		<?php endif;?>
		/>  
	</span>

</p>


<?php endif; ?>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>