<?php if( $this->element->has_errors() ): ?>
	<?php foreach( $this->element->get_error_codes() as $code  ): ?>
		<div class="eo-booking-form-field-errors">
			<p><?php echo $this->element->get_error_message( $code ); ?></p>
		</div>
	<?php endforeach; ?>
<?php endif; ?>