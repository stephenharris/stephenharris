
<p>
	<button type="submit" class="<?php echo esc_attr( $this->get_class() );?>"><?php echo esc_attr( $this->element->get_button_text() );?></button>
	<img class="eo-booking-form-waiting" src="<?php $this->waiting_img_src();?>" style="display:none" />
</p>
