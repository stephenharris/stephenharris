<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<select
	id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id );?>"
	name="<?php echo esc_attr( $this->get_name() );?>"
	class="<?php echo esc_attr( $this->get_class() );?>"
	style="<?php echo esc_attr( $this->element->get( 'style' ) );?>"
	>
<?php foreach( $this->get_options() as $option => $label ): ?>
	<option value="<?php echo esc_attr( $option ); ?>" <?php $this->selected( $option ) ?>>
	 	<?php echo esc_html( $label ); ?> 
	</option>
<?php endforeach; ?>
</select>

<?php include( eo_locate_template( 'eo-booking-form-description.php' ) );?>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>