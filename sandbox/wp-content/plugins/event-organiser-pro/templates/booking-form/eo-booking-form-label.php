<?php if( $this->element->show_label  ): ?>
	<label class="eo-booking-label" for="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id );?>">
		<?php $required = ( $this->element->is_required() ? '<span class="required eo-booking-form-element-required">*</span>' : '' ); ?>
		<?php echo esc_html( $this->element->get( 'label' ) ) . $required; ?> 
	</label>
<?php endif; ?>