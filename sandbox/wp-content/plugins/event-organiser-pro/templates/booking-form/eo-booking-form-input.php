<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<input
	type="<?php echo esc_attr( $this->element->get_field_type() );?>"
	id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id );?>"
	name="<?php echo esc_attr( $this->get_name() );?>"
	class="<?php echo esc_attr( $this->get_class() );?>"
	placeholder="<?php echo esc_attr( $this->element->get( 'placeholder' ) );?>"
	value="<?php echo esc_attr( $this->get_value() );?>"
	style="<?php echo esc_attr( $this->element->get( 'style' ) );?>"

<?php if( $this->element->is_required() ):?>
	required="required"
<?php endif;?>

<?php if( $this->element->get_data() ): ?>
	<?php foreach( $this->element->get_data() as $key => $attr_value ): ?>
		data-<?php echo esc_attr( $key )?>="<?php echo esc_attr( $attr_value );?>"
	<?php endforeach;?>
<?php endif;?>

<?php if( $this->element->get( 'size' ) ): ?>
	size="<?php echo esc_attr( $this->element->get( 'size' ) );?>"
<?php endif; ?>

<?php if( $this->element->get( 'min' ) != '' ): ?>
	min="<?php echo esc_attr( $this->element->get( 'min' ) );?>"
<?php endif; ?>

<?php if( $this->element->get( 'max' ) != '' ): ?>
	max="<?php echo esc_attr( $this->element->get( 'max' ) );?>"
<?php endif; ?>
/>

<?php include( eo_locate_template( 'eo-booking-form-description.php' ) ); ?>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>