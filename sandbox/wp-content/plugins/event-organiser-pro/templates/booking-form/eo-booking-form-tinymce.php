<?php 

?>	
<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<?php 
	$tinymce_args = is_array( $args['tinymce'] ) ? $args['tinymce'] : array();
	$tinymce_args = array_merge( array(
		'textarea_name' => $this->get_name(),
		'media_buttons' => false,
	), $tinymce_args );
		
	wp_editor( $this->get_value(), 'eo-booking-field-'.$this->element->id, $tinymce_args );
?>
	
<?php include( eo_locate_template( 'eo-booking-form-description.php' ) );?>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>