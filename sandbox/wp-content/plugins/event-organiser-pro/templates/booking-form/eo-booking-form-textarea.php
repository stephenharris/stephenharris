<?php 

?>	
<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>
			
<textarea
	id="<?php echo esc_attr( 'eo-booking-field-'.$this->element->id );?>"
	name="<?php echo esc_attr( $this->get_name() );?>"
	class="<?php echo esc_attr( $this->get_class() );?>"

<?php if( $this->element->get( 'rows' ) ): ?>
	rows="<?php echo intval( $this->element->get( 'rows' ) );?>"
<?php endif; ?>

<?php if( $this->element->get( 'cols' ) ): ?>
	cols="<?php echo intval( $this->element->get( 'cols' ) );?>"
<?php endif; ?>

<?php if( $this->element->get( 'data' ) ): ?>
	<?php foreach( $this->element->get( 'data' ) as $key => $attr_value ): ?>
		data-<?php echo esc_attr( $key )?>="<?php echo esc_attr( $attr_value );?>"
	<?php endforeach;?>
<?php endif;?>
><?php echo esc_textarea( $this->get_value() );?></textarea>

<?php include( eo_locate_template( 'eo-booking-form-description.php' ) );?>

<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>