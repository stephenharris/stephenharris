<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<div class="eo-booking-field-terms-conditions-text">
	<?php echo $this->element->get( 'terms' ); ?>
</div>
<label>
	<input type="checkbox" name="<?php echo esc_attr( $this->get_name() );?>" class="<?php echo esc_attr( $this->get_class() );?>" value="1" /> 
	<?php echo esc_html( $this->element->get( 'terms_accepted_label' ) ); ?>
</label>
