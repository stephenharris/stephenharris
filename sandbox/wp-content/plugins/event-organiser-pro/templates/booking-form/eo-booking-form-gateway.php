<?php 

?>

<?php include( eo_locate_template( 'eo-booking-form-label.php' ) ); ?>

<?php foreach ( $this->element->get_enabled_gateways() as $gateway => $label ): ?>
	
	<label for="gateway_<?php echo esc_attr( $gateway );?>" class="eo-booking-sub-label">
		<input
			type="radio"
			id="gateway_<?php echo esc_attr( $gateway );?>"
			name="<?php echo esc_attr( $this->get_name() );?>"
			class="<?php echo esc_attr( $this->get_class() );?>"
			value="<?php echo esc_attr( $gateway );?>"
			
			<?php $this->checked( $gateway ); ?>
		/>
		<?php echo $label; ?>
	</label>
	
	<?php 
		if( 'offline' == $gateway ){
			echo eventorganiser_pro_get_option( 'offline_instructions' );
		}
	?>
	
<?php endforeach; ?>
	
<?php include( eo_locate_template( 'eo-booking-form-errors.php' ) ); ?>