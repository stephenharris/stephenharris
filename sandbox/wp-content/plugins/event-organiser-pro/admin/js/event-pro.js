if ( typeof EO_SCRIPT_DEBUG === 'undefined') { EO_SCRIPT_DEBUG = true;}

jQuery(document).ready(function($) {
/* Occurrence Picker Handler */
$.widget("eventorganiser.eo_occurrencepicker", {
	
	// Default options.
    options: {
        value: 0,
        ticket: false,
        selected: [-1],
        deslected: [],
    },
    
    year: false,
    month: false,
    rule: {},
    
    /**
     * Set or retreive the ticket ID.
     * 
     * This is not the tickets ID but rather a temporary and unique ID. This may change
     * between page loads.
     * 
     * If no argument is passed the function acts as a getter. Othewerwise it sets
     * the ticket ID to passed value
     * 
     * @param int id The ticket ID
     * @returns null or the current ticket ID if used as a getter.
     */
    ticket: function( id ) {
    	 
        // No value passed, act as a getter.
        if ( id === undefined ) {
            return this.options.ticket;
        }
 
        // Value passed, act as a setter.
        this.options.ticket = this._absint( id );
    },
    
    
    /**
     * Create: set-up callbacks. Retrieve selected/deselected dates from DOM.
     */
    _create: function(){
    	
    	//Set pre-defined
		this.options.onSelect = this.add_or_remove_date;
		this.options.beforeShowDay = this.pre_show_date;
		this.options.onChangeMonthYear = this.generate_dates;
		
	
    },
    
    /**
     * Init: create datepicker instance and set year/month being viewed
     */
	_init: function() {
		var date = this.element.datepicker("getDate");
		date = ( ( typeof date == 'undefined' || date === null ) ? $("#from_date").datepicker("getDate") : date );

		this.options.defaultDate = date;
		this.year =  date.getFullYear();
		this.month = date.getMonth() + 1 ;
		
		this.set_occurrences( this.year, this.month );
		this.element.datepicker(this.options);
	},
	
	/**
	 * Gets the day being viewed and re-sets the occurrences.
	 * Refreshes the datepicker
	 */
	refresh: function(){

        //Set occurrences
        this.set_occurrences( this.year, this.month );
	
        //Refresh calendar
		this.element.datepicker( "refresh" );	
	},
	
    /**
     * Set the occurrences that have been selected.
     * 
     * If no argument is passed the function acts as a getter. Othewerwise it sets
     * the selected occurrences to those passed.
     * 
     * @param array value If not passed the function returns the current selected dates
     * @returns null if acting as setter. The current selected dates if acting as a getter.
     */
    selected: function( value ) {
 
        // No value passed, act as a getter.
        if ( value === undefined ) {
            return this.options.selected;
        }
 
        // Value passed, act as a setter.
        this.options.selected = this._array_filter( value );
		$(' #eop_ticket_selected_'+ this.ticket() ).val( this.options.selected.join(","));
		this.options.dateSelected( this.options.selected );
    },
    
    /**
     * Set the occurrences that have been deselected.
     * 
     * If no argument is passed the function acts as a getter. Othewerwise it sets
     * the deselected occurrences to those passed.
     * 
     * @param array value If not passed the function returns the current deselected dates
     * @returns null if acting as setter. The current deselected dates if acting as a getter.
     */
    deselected: function( value ) {

        // No value passed, act as a getter.
        if ( value === undefined ) {
            return this.options.deselected;
        }
 
        // Value passed, act as a setter.
        this.options.deselected = this._array_filter( value );
        $(' #eop_ticket_deselected_'+ this.ticket() ).val( this.options.deselected.join(","));
        this.options.dateDeselected( this.options.deselected );
    },

	select_all: function(){
		this.selected( [-1] );
		this.deselected( [] );
		this.element.datepicker( "refresh" );
	},
	deselect_all: function(){
		this.selected( [""] );
		this.deselected( [] );
		this.element.datepicker( "refresh" );
	},

	pre_show_date: function( date ) {

		var date_str = $.datepicker.formatDate('dd-mm-yy', date);
		
		var selected = $(this).eo_occurrencepicker('selected');
		var deselected = $(this).eo_occurrencepicker('deselected');
	
		var isEventful = $(this).eo_occurrencepicker( 'is_date_eventful', date );
	    
        if ( isEventful[0] ) {
        	var index = $.inArray( date_str, selected );
        	var de_index =  $.inArray( date_str , deselected );
        	if( ( index>-1 && selected[0] != "-1" ) || ( selected[0] == "-1" && de_index == -1) ){
            	return [true, "eo-pro-selected", ""];
        	}else{
       	    	return [true, "eo-pro-not-selected", ""];
			}
        }
        return [false, "ui-state-disabled", ''];
	},

	
	add_or_remove_date: function(date){
	
		var selected = $(this).eo_occurrencepicker( 'selected' );
		var deselected = $(this).eo_occurrencepicker( 'deselected' );
				
		var index =  $.inArray( date, selected );
		var de_index =  $.inArray( date, deselected );
		
		if( selected[0] == "-1" && de_index > -1 ){
			deselected.splice(de_index, 1);
		
		}else if( selected[0] == "-1"  ){
			deselected.push(date);

		}else if( index >-1 ){
			selected.splice(index, 1);
		}else{
			selected.push(date);
		}
		
		$(this).eo_occurrencepicker( 'selected', selected );
		$(this).eo_occurrencepicker( 'deselected', deselected );
	},

	is_date_eventful: function(date_obj) {
		
	    var date = $.datepicker.formatDate( 'dd-mm-yy', date_obj );
	    //Included/exlude arrays expect Y-m-d format
	    var ymd_date = $.datepicker.formatDate( 'yy-mm-dd', date_obj );
	    
		var index = $.inArray( date, this.options.occurrences_by_rule );
        if (index > -1) {
        	//Occurs by rule - is it excluded manually?
           	var excluded = $.inArray( ymd_date, eo_exclude_dates );
            if (excluded > -1) {
				return [false, excluded];
            } else {
                return [true, -1];
            }
        } else {
          	//Doesn't occurs by rule - is it included manually?
            var included = $.inArray( ymd_date, eo_include_dates );
            if (included > -1) {
              	return [true, included];
            } else {
               	return [false, -1];
            }
        }
	},
	

	/**
	 * 
	 * @param int year - 4 digit interger
	 * @param in month - 1-12 (Jan-Dec)
	 * @param objecct inst
	 */
	generate_dates: function (year, month, inst ){        	
        $(this).eo_occurrencepicker( 'set_occurrences', year, month  );
	},
	
	/**
	 * Sets the occurrences for a given month
	 * @param month_start
	 * @param month_end
	 */
	set_occurrences: function( year, month ){

        //Get month start/end dates. Date expects month 0-11.
        var month_start = new Date(year, month-1, 1);
        var nxt_mon = new Date(year, month, 1);
        var month_end = new Date(nxt_mon - 1);
        
		this.rule = rule = {
	        	start: $("#from_date").datepicker("getDate"),
        		end: $("#recend").datepicker("getDate"),
        		schedule: $('#HWSEventInput_Req').val(),
        		frequency: parseInt( $('#HWSEvent_freq').val(), 10 )
		};
		
		this.year = month_start.getFullYear();
		this.month = month_start.getMonth() + 1;
		
		if (rule.end >= month_start && rule.start <= month_end) {
			this.options.occurrences_by_rule  = this.generate_dates_by_rule( rule, month_start,month_end);
		}else{
			this.options.occurrences_by_rule = [];
		}
	},
	
	/**
	 * Generates an array of dates present in a given month (between month_start &
	 * month_end), according to a rule:
	 * 
	 * rule = {
	 *  start: start date (may be outside given month)
	 *  end: end date (may be outside given month)
	 *  schedule: once|custom|daily|weekly|monthly|yearly
	 *  frequency: integer frequency of schedule. 
	 * }
	 * 
	 * Returns an array of dates that match the rule, and lie between month and start
	 */
	generate_dates_by_rule: function( rule ,month_start , month_end ){
		var occurrences = [];

		var streams = [];
		var pointer = false;
		var start =  rule.start;
		var end =  rule.end;
		var schedule =  rule.schedule;
		var frequency =  rule.frequency;

	    //If event starts in previous month - how many days from start to first occurrence in current month?
	    // Depends on occurrence (and 'stream' for weekly events.
	    switch (schedule) {
	    	case 'once':
		    case 'custom':
		    	var formateddate = $.datepicker.formatDate('dd-mm-yy', start);
		    	occurrences.push(formateddate);
		    	return occurrences;
		    //break

		    case 'daily':
		    	var count_days = 0;
		    	
				if (start < month_start) {
					//Days from schedule start to month start
					count_days = Math.round(Math.abs((month_start - start) / (1000 * 60 * 60 * 24)));
					count_days = (frequency - count_days % frequency);
				} else {
					count_days = parseInt( start.getDate(), 10 ) - 1;
				}
            	var skip = frequency;
            	var start_stream = new Date(month_start);
				start_stream.setDate(month_start.getDate() + count_days);
				streams.push(start_stream);
				//We iterate over the streams after switch statement
			break;

			case 'weekly':
            	var selected = $("#dayofweekrepeat :checkbox:checked");

				var ical_weekdays = new Array("SU", "MO", "TU", "WE", "TH", "FR", "SA");
				selected.each(function(index) {
					index = ical_weekdays.indexOf($(this).val());
					var start_stream = new Date(start);
					start_stream.setDate(start.getDate() + (index - start.getDay() + 7) % 7);

					if (start_stream < month_start) {
						var count_days = Math.abs((month_start - start) / (1000 * 60 * 60 * 24));
						count_days = count_days - count_days % (frequency * 7);
						start_stream.setDate(start_stream.getDate() + count_days);
					}
					streams.push(start_stream);
				});
				//We iterate over the streams after switch statement
				skip = 7 * frequency;
            break;

            //These are easy - can be at most date.          
			case 'monthly':
				var month_difference = (month_start.getFullYear() - start.getFullYear()) * 12 + (month_start.getMonth() -  start.getMonth() );
            	if ( month_difference % frequency !== 0 ) {
            		return occurrences;
            	}
           	 	var meta = $('input[name="eo_input[schedule_meta]"]:checked').val();
            	if (meta == 'BYMONTHDAY=') {
                	var day = start.getDate();
                	var daysinmonth = month_end.getDate();
                	if (day <= daysinmonth) {
                		//If valid date
						pointer = new Date(month_start.getFullYear(), month_start.getMonth() , day);
                	}
				} else {
                	//e.g. 3rd friday of month:
					var n = Math.ceil(start.getDate() / 7), occurrence_day = start.getDay(), occurence_date;
					if (n >= 5) {
                    	//Last day
                  		var month_end_day = month_end.getDay();
                   		occurence_date = month_end.getDate() + (occurrence_day - month_end_day - 7) % 7;
					} else {
						var month_start_day = month_start.getDay();
						var offset = (occurrence_day - month_start_day + 7) % 7;
                    	occurence_date = offset + (n - 1) * 7 + 1;
                	}
				    pointer = new Date(month_start);
				    pointer.setDate(occurence_date);
				}

				if (pointer <= end) {
					//If before end
					formateddate = $.datepicker.formatDate('dd-mm-yy', pointer);
                	occurrences.push(formateddate);
            	}
            	return occurrences;
            //break;

			case 'yearly':
				var year_difference = (month_start.getFullYear() - start.getFullYear());
           		if ( year_difference % frequency !== 0 ) {
               		return occurrences;
           		}

           		//Does the date in this year make sense (e.g. leap years!). If it doesn't the date will overflow
            	var dateCheck = new Date( month_start.getFullYear(), start.getMonth(), start.getDate() );
            	if ( dateCheck.getMonth() == start.getMonth() && dateCheck.getMonth() == start.getMonth()) {
                	pointer = new Date(start);
                	pointer.setYear( month_start.getFullYear() );
                	if ( pointer <= end ){
                		//If before end
                		formateddate = $.datepicker.formatDate('dd-mm-yy', pointer);
                    	occurrences.push(formateddate);
                	}
            	}
				return occurrences;
			//break;

			default:
				return occurrences;
			//break;
        }
	    //End switch
	    
	    //For daily / monthly schedules,
	    //while in current month, and event has not finished - generate occurrences.
		for ( var x in streams) {
			pointer = new Date(streams[x]);
			while (pointer <= month_end && pointer <= end) {
			formateddate = $.datepicker.formatDate('dd-mm-yy', pointer);
               		occurrences.push(formateddate);
               		pointer.setDate(pointer.getDate() + skip);
            	}
		}

		return occurrences;
    },
    
    
    /**
	 * Casts input as a positive integer
	 * @param value
	 * @return int Positive integer
	 */
    _absint: function( value ){
    	return Math.abs( parseInt( value, 10 ) );
    },
        
    
    /**
	 * Cast value as a positive integer
	 * @param arr
	 * @returns
	 */
    _array_filter: function( arr ){
    	return $.grep(arr,function(n){ return(n); });
    },

});


});

(function($) {

eo = eo || {};

var ticketManager = eo.tm = {
		Model: {},
		View: {},
		Collection: {},
		models: {},
		views: {},
	};

console.log(eo);

//===============================================================
//Models
//===============================================================
ticketManager.Model.EOTicketsController = Backbone.Model.extend();

ticketManager.Model.EOTicket = Backbone.Model.extend({

	idAttribute: 'mid',
	
	defaults: {
		name:       "New Ticket",
		selected:   "-1",
		deselected: "",
		spaces:     1,
		price:      0,
		from:       "",
		from_time:  "",
		to:         "",
		to_time:    "",
		order:      0,
	},
	
	initialize: function(){
				
	},
	
	toJSON: function() {
	  var json = Backbone.Model.prototype.toJSON.apply(this, arguments);
	  json.cid = this.cid;
	  json.mid = ( this.get('mid') ? this.get('mid') : 0 );
	  json.from = ( this.get('from') ? this.get('from') : "" );
	  json.to = ( this.get('to') ? this.get('to') : "" );
	  return json;
	},

	is_first: function(){
		var firstModel = ticketManager.models.tickets.get_first();
		return firstModel && ( firstModel.cid === this.cid );
	},
	
	is_last: function(){
		var lastModel = ticketManager.models.tickets.get_last();
		return lastModel && ( lastModel.cid === this.cid );
	},
	
});


ticketManager.Collection.EOTickets = Backbone.Collection.extend({
	
	model: ticketManager.Model.EOTicket,
	
	comparator: 'order',
	
	initialize: function( models ){
		var clientid = 0;
		var collection = this;
		if( models && models.length > 0 ){
			_.each( models, function( model ){
				model.clientid = clientid;
				clientid++;
			});
		}

		this.on( 'ticket-moved-down', this.sort, this );
		this.on( 'ticket-moved-up', this.sort, this );
	},
	
	move_ticket_down: function( ticket ){
		var theirTicket = this.get_next_visible( ticket );
		var theirOrder = theirTicket.get( 'order' );
		var thisOrder = ticket.get('order');
		
		ticket.set( 'order', theirOrder, {silent:true} ); 
		theirTicket.set( 'order', thisOrder, {silent:true} );
		
		this.trigger( 'ticket-moved-down', ticket, theirTicket );	
		ticket.trigger( 'change:order' );
		theirTicket.trigger( 'change:order' );
	},
	
	move_ticket_up: function( ticket ){
		var theirTicket = this.get_prev_visible( ticket );
		var theirOrder = theirTicket.get( 'order' );
		var thisOrder = ticket.get('order');
		
		ticket.set( 'order', theirOrder, {silent:true} ); 
		theirTicket.set( 'order', thisOrder, {silent:true} );
		
		//console.log( ticket.cid );
		//console.log( theirTicket.cid );
		//console.log( theirOrder );
		//console.log( thisOrder );
		
		this.trigger( 'ticket-moved-up', ticket, theirTicket );		
		ticket.trigger( 'change:order' );
		theirTicket.trigger( 'change:order' );
	},
	
	get_first: function(){
		
		var visible_tickets = this.get_visible();
		if( visible_tickets ){
			return visible_tickets[0];
		}else{
			return false;
		}
		
	},
	
	get_last: function(){
		
		var visible_tickets = this.get_visible();
		if( visible_tickets ){
			return visible_tickets[visible_tickets.length-1];
		}else{
			return false;
		}
		
	},
	
	get_visible: function(){
		return this.filter( function(ticket) { return ( ticket.get('action') != 'delete' ); } );	
	},
	
	get_next_visible: function( model ){
		return this.get_adj_visible( model, 'next' );
	},
	
	get_prev_visible: function( model ){
		return this.get_adj_visible( model, 'previous' );
	},
	
	get_adj_visible: function( model, adjacent ){
		
		var tickets = this.filter(function(ticket) {
			
			var visible = ( ticket.get('action') != 'delete' );
			
			if( adjacent == 'next' ){
				return visible && ( ticket.get('order') > model.get('order') );
			}else{
				return visible && ( ticket.get('order') < model.get('order') );
			}
		});
		
		if( !tickets ){
			return false;
		}
		
		if( adjacent == 'next' ){
			return tickets[0];
		}else{
			return tickets[tickets.length-1];
		}
		
	}
	
	
});


//===============================================================
//Views
//===============================================================
ticketManager.View.EOTicketsView = Backbone.View.extend({
	
	el: '#eo-tickets-table',
	
	events: {
		'click #eo-add-ticket': 'addNewTicket',
	},
		
	template: _.template( $( '#tmpl-' + 'eo-tickets-table' ).html( ), null, {
		evaluate:    /<#([\s\S]+?)#>/g,
		interpolate: /\{\{\{([\s\S]+?)\}\}\}/g,
		escape:      /\{\{([^\}]+?)\}\}(?!\})/g
	}),
	
    initialize: function() {
    	
    	_.bindAll( this, 'render', 'appendTicket' );
    	
    	this.render();
    	
    	this.collection.bind( 'add', this.appendTicket );
    	this.collection.on( 'ticket-moved-down', this.moveTicketBelow );
    	this.collection.on( 'ticket-moved-up', this.moveTicketAbove );
	},
	
	render: function(){
		this.$el.html( this.template({}) );
		
		ticketManager.views.ticket = [];
		
		var self = this;
		this.collection.each(function( ticket ){
			self.appendTicket( ticket );
		});
		return this;
	},
	
	addNewTicket: function( ev ){
		ev.preventDefault();

		var ticket = new ticketManager.Model.EOTicket({
			order: this.collection.length
		});

		this.collection.add( ticket );
		//this.collection.trigger('add', ticket);
		//$('#eo-ticket-row-' + ticket.cid + ' .eo-edit-ticket').trigger('click');

	},
	
	appendTicket: function( ticket ){
		$( 'tr.no-items', this.el ).remove();
		var ticketView = new ticketManager.View.EOTicketView({ model: ticket });
		ticketManager.views.ticket.push( ticketView );
        $( 'table.eo-event-ticket-table', self.el ).append( ticketView.render().el );
	},
	
	moveTicketBelow: function( ticket, targetTicket ){
		$('#eo-ticket-row-'+ticket.cid).insertAfter( $('#eo-ticket-row-'+targetTicket.cid) );
	},	

	moveTicketAbove: function( ticket, targetTicket ){
		console.log( ticket.cid );
		console.log( targetTicket.cid );
		console.log( $('#eo-ticket-row-'+ticket.cid).length );
		console.log( $('#eo-ticket-row-'+targetTicket.cid).length );
		$('#eo-ticket-row-'+ticket.cid).insertBefore( $('#eo-ticket-row-'+targetTicket.cid) );
	},	
	
});

ticketManager.View.EOTicketView = Backbone.View.extend({
	
	tagName: 'tbody',
	
	className: 'eo-ticket-row',
	
	events: {
		'click .eo-edit-ticket': 'toggleEdit',
		'click .eo-delete-ticket': 'deleteTicket',
		'click .eo-move-ticket-up': 'moveTicketUp',
		'click .eo-move-ticket-down': 'moveTicketDown',
		'click .eo-ticket-input-to': 'renderDatePicker',
		'click .eo-ticket-input-from': 'renderDatePicker',
		'click .eo-ticket-input-to-time': 'renderTimePicker',
		'click .eo-ticket-input-from-time': 'renderTimePicker',
		
		'keyup .eo-ticket-input-name': 'liveUpdateName',
		'keyup .eo-ticket-input-price': 'liveUpdatePrice',
		'change .eo-ticket-input-price': 'autoCorrectPrice',
		'change .eo-ticket-input-spaces': 'liveUpdateSpaces',
		'change .eo-ticket-input-sale-period': 'updateSalePeriod',
		'change .eo-ticket-dates': 'updateTicketDates',
	},

	template: _.template( $( '#tmpl-' + 'eo-ticket' ).html( ), null, {
		evaluate:    /<#([\s\S]+?)#>/g,
		interpolate: /\{\{\{([\s\S]+?)\}\}\}/g,
		escape:      /\{\{([^\}]+?)\}\}(?!\})/g
	}),
	
    initialize: function() {
    	var view = this;
		this.model.on( 'change:name', function(){
			$('.eo-ticket-name', view.el ).text( this.get( 'name' ) );
		});
		this.model.on( 'change:price', function(){
			$('.eo-ticket-price', view.el ).text( this.get( 'price' ) );
		});
		this.model.on( 'change:spaces', function(){
			$('.eo-ticket-spaces', view.el ).text( this.get( 'spaces' ) );
		});
		
		this.model.on( 'change:order', function(){
			console.log('render! order changed');
			view.render();
		});
	},
	
	render: function(){
		var json = this.model.toJSON();
		json._first = this.model.is_first();
		json._last = this.model.is_last();
		this.$el.html( this.template( json ) );

		$(this.el).attr( 'id', 'eo-ticket-row-' + this.model.cid );
		$(this.el).data( 'eo-ticket-row-id', this.model.cid );
		$(this.el).data( 'eo-ticket-id', this.model.id  );
		
		//Inline help
		$('.eo-inline-help', this.el ).each(function() {
			var id = $(this).attr('id').substr(15);
			$(this).click(function(e){e.preventDefault();});
			$(this).qtip({
				content: {
					text: eoHelp[id].content,
					title: {
						text: eoHelp[id].title
					}
				},
				show: {
					solo: true 
				},
				hide: 'unfocus',
				style: {
					classes: 'qtip-wiki qtip-light qtip-shadow'
				},
				position : {
					 viewport: $(window)
				}
			});
		});
		
		//Select/Deselect all for occurrence picker
		$( this.$el ).on('click','.eo-select-all, .eo-deselect-all',function(e){
			e.preventDefault();
			var row = $(this).parents('tbody.eo-ticket-row');

			var op = row.find('.eo-ticket-occurrences-input');

			if( $(this).hasClass('eo-select-all') ){
				op.eo_occurrencepicker( 'select_all' );
			}else if( $(this).hasClass('eo-deselect-all') ){
				op.eo_occurrencepicker( 'deselect_all' );
			}
		});
		
		return this;
	},
	
	renderDatePicker: function( ev ){
		$(ev.target).datepicker({
			dateFormat: eo_pro.format,
			changeMonth: true,
			changeYear: true,
			monthNamesShort: eo_pro.locale.monthAbbrev,
			dayNamesMin: eo_pro.locale.dayAbbrev,
			firstDay: parseInt( eo_pro.startday, 10 ),
			/*onSelect: function(selectedDate) {
				var option = this.id == "eo-ticket-dialog-from" ? "minDate": "maxDate",
				instance = $(this).data("datepicker"),
				date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
				availability_dates.not(this).datepicker("option", option, date);
			}*/
		}).datepicker( "show" );
		
	},	
	
	renderTimePicker: function( ev ){
		var options = eo_pro;
		$(ev.target).timepicker({
		    showPeriodLabels: !options.is24hour,
		    showPeriod: !options.is24hour,
		    showLeadingZero: options.is24hour,
		    periodSeparator: '',
		    amPmText: options.locale.meridian,
		    hourText: options.locale.hour,
		    minuteText: options.locale.minute
		}).timepicker( "show" );
	},
	
	toggleEdit: function( ev ){
		ev.preventDefault();
		
		//Editting a ticket, open dialog and populate fields		
		var $settings_row = $('.eo-ticket-settings', this.el );
		
		$settings_row.toggle();
		
		if( $settings_row.is(":visible") ){
			$(this.el).addClass('eo-ticket-row-expanded');
			
			var ticket = this.model;
			
			$('.eo-settings-toggle-arrow', this.el ).html('&#x25B2;');
			
			var selected = $(' #eop_ticket_selected_'+ ticket.cid ).val().split(",");
			var deselected = $(' #eop_ticket_deselected_'+ ticket.cid ).val().split(",");
		
			if( selected.length === 0 && deselected.length === 0 ){
				selected = [-1];
				deselected = [];	
			}
			
			
			var op = $('.eo-ticket-occurrences-input', this.el );
			var options = {
				dateFormat:      "dd-mm-yy",
				changeMonth:     true,
				changeYear:      true,
				monthNamesShort: eo_pro.locale.monthAbbrev,
				dayNamesMin:     eo_pro.locale.dayAbbrev,
				firstDay:        parseInt( eo_pro.startday, 10 ),
				ticket:          this.model.cid,
				dateSelected: function( selected ){
					ticket.set( 'selected', selected.join(",") );
				},
				dateDeselected: function( deselected ){
					ticket.set( 'deselected', deselected.join(",") );
				},
				selected: selected,
				deselected: deselected,
			};
			op.eo_occurrencepicker( options );
					
		}else{
			$(this.el).removeClass('eo-ticket-row-expanded');
			$('.eo-settings-toggle-arrow', this.el ).html('&#x25BC;');
		}
	},
	

	deleteTicket: function( ev ){
		ev.preventDefault();
		this.model.set( 'action', 'delete' );
		
		$('.eo-ticket-action', this.el ).val( 'delete' );
		$( this.el ).css('background-color','red').fadeOut( 400 );
	},


	moveTicketUp: function( ev ){
		ev.preventDefault();
		ticketManager.models.tickets.move_ticket_up( this.model );
	},
	

	moveTicketDown: function( ev ){
		ev.preventDefault();
		ticketManager.models.tickets.move_ticket_down( this.model );
	},
	
	liveUpdateName: function( ev ){
		this.model.set( 'name',  $(ev.target).val() );
	},

	liveUpdatePrice: function( ev ){
		//Remove commas...
		var price = $(ev.target).val().replace(/,/g, '');
		$(ev.target).val( price );
		
		this.model.set( 'price', price );
	},
	
	updateSalePeriod: function( ev ){
		this.model.set( $(ev.target).data( 'eo-sale-period' ), $(ev.target).val() );
	},

	updateTicketDates: function( ev ){
		console.log( ev );
		this.model.set( $(ev.target).data( 'eo-ticket-dates' ), $(ev.target).val() );
	},
	
	autoCorrectPrice: function( ev ){
		//Correct decimal places
		var price = parseFloat( $(ev.target).val() );		
		price = ( Math.floor( price ) != price ) ? price.toFixed( 2 ) :  Math.floor( price );
		$(ev.target).val( price );
		this.model.set( 'price',  price );
	},
	
	liveUpdateSpaces: function( ev ){
		this.model.set( 'spaces',  $(ev.target).val() );
	},
	
	
});

//======================================
//Initialize
//======================================
ticketManager.models.tickets = new ticketManager.Collection.EOTickets( eo.tickets );

$(document).ready(function(){
		
	var ticketManagerView = new ticketManager.View.EOTicketsView({ collection: ticketManager.models.tickets });
	


});
})(jQuery);