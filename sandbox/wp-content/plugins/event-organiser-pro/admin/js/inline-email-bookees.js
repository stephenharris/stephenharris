var eventorganiser = eventorganiser || {};
/**
 * Simply compares two string version values.
 * 
 * Example:
 * versionCompare('1.1', '1.2') => -1
 * versionCompare('1.1', '1.1') =>  0
 * versionCompare('1.2', '1.1') =>  1
 * versionCompare('2.23.3', '2.22.3') => 1
 * 
 * Returns:
 * -1 = left is LOWER than right
 *  0 = they are equal
 *  1 = left is GREATER = right is LOWER
 *  And FALSE if one of input versions are not valid
 *
 * @function
 * @param {String} left  Version #1
 * @param {String} right Version #2
 * @return {Integer|Boolean}
 * @author Alexey Bass (albass)
 * @since 2011-07-14
 */
eventorganiser.versionCompare = function(left, right) {
    if (typeof left + typeof right != 'stringstring')
        return false;
    
    var a = left.split('.'),   b = right.split('.'),   i = 0, len = Math.max(a.length, b.length);
        
    for (; i < len; i++) {
        if ((a[i] && !b[i] && parseInt(a[i], 10 ) > 0) || (parseInt(a[i], 10 ) > parseInt(b[i], 10 ))) {
            return 1;
        } else if ((b[i] && !a[i] && parseInt(b[i], 10 ) > 0) || (parseInt(a[i], 10 ) < parseInt(b[i], 10 ))) {
            return -1;
        }
    }
    
    return 0;
};

eventorganiser.format_date = function( format, timestamp ){
	var that = this;
	var jsdate, f;
			
	//trailing backslash -> (dropped)
	// a backslash followed by any character (including backslash) -> the character
	// empty string -> empty string
	var formatChr = /\\?(.?)/gi;
	var formatChrCb = function(t, s) {
	    return f[t] ? f[t]() : s;
	};
	var _pad = function(n, c) {
		n = String(n);
		while (n.length < c) {
			n = '0' + n;
		}
		return n;
	};
	
	var months = [ 'January', 'February', 'March', 'April', 'May', 'June',
	              'July', 'August', 'September', 'October', 'November', 'December'
	              ];
	var days = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
		
	f = {
		//Day
		d: function() { // Day of month w/leading 0; 01..31
			return _pad(f.j(), 2);
		},
	    D: function() { // Shorthand day name; Mon...Sun
	        return f.l().slice(0, 3);
	    },
	    j: function() { // Day of month; 1..31
	    	return jsdate.getDate();
	    },
	    l: function() { // Full day name; Monday...Sunday
	        return days[f.w()];
	    },
	    N: function() { // ISO-8601 day of week; 1[Mon]..7[Sun]
	    	return f.w() || 7;
	    },
	    S: function() { // Ordinal suffix for day of month; st, nd, rd, th
	        var j = f.j();
	        var i = j % 10;
	        if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
	          i = 0;
	        }
	        return ['st', 'nd', 'rd'][i - 1] || 'th';
	    },
	    w: function() { // Day of week; 0[Sun]..6[Sat]
	    	return jsdate.getDay();
	    },
		// Month
		m: function() { // Month w/leading 0; 01...12
			return _pad(f.n(), 2);
		},
		n: function() { // Month; 1...12
			return jsdate.getMonth() + 1;
		},
		F: function() { // Month w/leading 0; 01...12
			return months[f.n()-1];
		},
	    M: function() { // Shorthand month name; Jan...Dec
	        return f.F().slice(0, 3);
	    },
		//Year
		Y: function() { // Full year; e.g. 1980...2010
			return jsdate.getFullYear();
		},
	    y: function() { // Last two digits of year; 00...99
	        return f.Y().toString().slice(-2);
	    },
		// Time
		a: function() { // am or pm
			return jsdate.getHours() > 11 ? 'pm' : 'am';
		},
		A: function() { // AM or PM
			return f.a().toUpperCase();
		},
		g: function() { // 12-Hours; 1..12
			return f.G() % 12 || 12;
		},
		G: function() { // 24-Hours; 0..23
			return jsdate.getHours();
		},
		h: function() { // 12-Hours w/leading 0; 01..12
			return _pad(f.g(), 2);
		},
		H: function() { // 24-Hours w/leading 0; 00..23
			return _pad(f.G(), 2);
		},
		i: function() { // Minutes w/leading 0; 00..59
			return _pad(jsdate.getMinutes(), 2);
		},
	};
		
	this.date = function(format, timestamp) {
		that = this;
		jsdate = (timestamp === undefined ? new Date() : // Not provided
			(timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
				new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
		);
		return format.replace(formatChr, formatChrCb);
	};
	return this.date(format, timestamp);
};

(function( $ ){
	  var methods = {
	     init : function( options ) {
	    	 this.hide();
	    	 var original = this;
	    	 var id = this.attr('id');
	    	 var groups = $.map( this.children('optgroup'), function(e) { 
	    	 var values = $.map( $(e).children('option'), function(e) { 
	    			 return { value: $(e).val(), label: $(e).text() };
		    	 });
	    		 return { values: values, label:$(e).attr('label') };
	    	 } );
	    	
	    	 var ul = $('<ul id="'+id+'-es" class="eo-multiselect-checkboxes ui-helper-reset"></ul>').insertAfter(this);
	    	 
	    	 $.each( groups, function(prop, group) {
	    		 	var subgroup = $('<ul></ul>');
	    			$("<li class='eo-multiselect-optgroup-label'><a class='ui-widget-header ui-corner-all eo-multiselect-header ui-helper-clearfix' href='#'>"+group.label+"</a></li>")
	    				.children('a').toggle(function() {
	    					 $(this).parent().find('li input').attr('checked',true).trigger('change');
	    				}, function() {
	    					$(this).parent().find('li input').attr('checked',false).trigger('change');
	    				})
	    				.parent().appendTo(ul).append(subgroup);
	    			
	    			$.each( group.values, function(index, option) {
	    				subgroup.append('<li class="eo-multiselect-option">'+
	    						'<label class="ui-corner-all">'
	    						+'<input type="checkbox" value="'+option.value+'" title="'+option.label+'">'
	    						+'<span> '+option.label+' </span>'
	    						+'</label>'
	    						+'</li>'
	    				);
	    			});
	    	});
	    	 
	    	 jQuery.expr[':'].containsInsensitive = function(a,i,m){
	    		 return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	    	 };
	    	 
	    	 var filter =$('<input type="text" class="eo-multiselect-filter" placeholder="Filter">');
	    	 ul.wrap('<div class="eo-multiselect ui-widget ui-widget-content ui-corner-all" />').before(filter);
	    	 				
	    	 filter.keyup(function(event) {
	    		 event.preventDefault();
	    		 var filter = $(this).val();
	    		 if (filter) {
	    			 ul.find("li ul li label span:not(:containsInsensitive(" + filter + "))").parents('li.eo-multiselect-option').hide();
	    			 ul.find("li ul li label span:containsInsensitive(" + filter + ")").parents('li.eo-multiselect-option').show();
	    		 } else {
	    			 ul.find('li.eo-multiselect-option').show();
	    		 }
	    	 });
	
	    	 ul.find( 'li ul li').hover(function() {
	    	      $(this).addClass('ui-state-hover');
	    	   }, function() {
	    	      $(this).removeClass('ui-state-hover');
	    	   }).find('input:checkbox').change(function(){
	    		   	var value = $(this).val();
	    		   	original.find('option[value="'+value+'"]').attr("selected",this.checked);
	     		});
	    	 	   	
	       return this;
	     }
	  };

	  $.fn.easySelect = function( method ) {
	    if ( methods[method] ) {
	      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
	    } else if ( typeof method === 'object' || ! method ) {
	      return methods.init.apply( this, arguments );
	    } else {
	      $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
	    }    
	  };
	})( jQuery );
	
(function($) {
inlineEditBooking = {

	init : function(){
		var t = this,emailRow=$('#bulk-email'),downloadSettings=$('#bulk-download');
		t.what = '#booking-';

		//revert on escape or clicking cancel
		emailRow.keyup(function(e){
			if (e.which == 27)
				return inlineEditBooking.revert();
		});
		downloadSettings.keyup(function(e){
			if (e.which == 27)
				return inlineEditBooking.revert();
		});

		//Deleting booking
		$('#eo-bookings-table').on('click','.delete .submitdelete',function(e){
			var r=confirm("Are you sure you wish to delete this booking?\n\n This action cannot be undone.");
			if ( r!== true ){	
				e.preventDefault();	
			  }
		});

		//Cancel bulk email
		$('a.cancel', emailRow).click(function(){
			return inlineEditBooking.revert();
		});
		//Cancel bulk email
		$('a.cancel', downloadSettings).click(function(){
			return inlineEditBooking.revert();
		});

		$('#eo-meta-labels').easySelect();
		//On bulk actions drop-down selecting edit, trigger bulk edit
		$('#download-bookings-trigger').click(function(e){
			e.preventDefault();
			if ( $('form#posts-filter tr.inline-editor').length > 0 ) {
				t.revert();
			}else{
				t.showBookingsDownloadOptionss();
			}
		});

		//On bulk actions drop-down selecting edit, trigger bulk edit
		$('#doaction, #doaction2').click(function(e){
			var n = $(this).attr('id').substr(2);
			 if ( $( 'select[name="'+n+'"]' ).val() == 'email' ){
				e.preventDefault();
				t.setEmail();
			}else if (  $( 'select[name="'+n+'"]' ).val() == 'delete' ) {
				var r=confirm("Are you sure you wish to delete these bookings?\n\n This action cannot be undone.");
				if ( r !== true ){	
					e.preventDefault();	
	 			 }
				t.revert();

			} else if ( $('form#posts-filter tr.inline-editor').length > 0 ) {
				t.revert();
			}
		});

		//On filter, revert quick/bulk edit and remove action.
		$('#post-query-submit').mousedown(function(e){
			t.revert();
			$('select[name^="action"]').val('-1');
		});
	},

	toggle : function(el){
		var t = this;
		if( $(t.what+t.getId(el)).css('display') == 'none' ){
			t.revert();
		}else{
			t.edit(el);
		}
	},

	showBookingsDownloadOptionss : function(){

		this.revert();
		
		//Show the bookings download
		$('#bulk-download td').attr('colspan', $('.widefat:first thead th:visible').length);
		$('table.widefat tbody').prepend( $('#bulk-download') );
		$('#bulk-download').addClass('inline-editor').show();
		
		$('html, body').animate( { scrollTop: 0 }, 'fast' );
	},
	
	setEmail : function(){
		var te = '', c = true;
		this.revert();

		//Show the bulk editor
		$('#bulk-email td').attr('colspan', $('.widefat:first thead th:visible').length);
		$('table.widefat tbody').prepend( $('#bulk-email') );
		$('#bulk-email').addClass('inline-editor').show();

		//Add the checked users to the edit list
		var eo_mail_list = [];
		$('tbody th.check-column input[type="checkbox"]').each(function(i){
			if ( $(this).prop('checked') ) {
				c = false;
				var id = $(this).val(), bookee;
				bookee = $('#inline_'+id+' .username').text() || false;
				if( bookee ){
					te += '<div id="ttleemail'+id+'"><a id="_'+id+'" class="ntdelbutton" title="click to remove">X</a>'+bookee+'</div>';
				      eo_mail_list.push(bookee);
				}
			}
		});

		if ( c )
			return this.revert();

		$('#bulk-email-titles').html(te);

		//When a user is removed from the bulk edit list,uncheck them.
		$('#bulk-email-titles a').click(function(){
			var id = $(this).attr('id').substr(1);
			$('table.widefat input[value="' + id + '"]').prop('checked', false);
			$('#ttleemail'+id).remove();
		});

		$('html, body').animate( { scrollTop: 0 }, 'fast' );

		},

	revert : function(){
		var id = $('table.widefat tr.inline-editor').attr('id');

		if ( id ) {
			$('table.widefat .inline-edit-save .waiting').hide();

			if ( 'bulk-edit' == id ) {
				$('table.widefat #bulk-edit').removeClass('inline-editor').hide();
				$('#bulk-titles').html('');
				$('#inlineedit').append( $('#bulk-edit') );
			}else if ('bulk-email' == id ){
				$('table.widefat #bulk-email').removeClass('inline-editor').hide();
				$('#bulk-email-titles').html('');
				$('#inlineedit').append( $('#bulk-email') );
			}else if( 'bulk-download' == id ){ 		
				$('table.widefat .inline-edit-save .waiting').hide();
				$('table.widefat #bulk-download').removeClass('inline-editor').hide();
				$('#bulk-download-edit').append( $('#bulk-download') );
			} else {
				$('#'+id).remove();
				id = id.substr( id.lastIndexOf('-') + 1 );
				$(this.what+id).show();
			}
		}

		return false;
	},

	getId : function(o) {
		var id = $(o).closest('tr').attr('id'),
		parts = id.split('-');
		return parts[parts.length - 1];
	}
};

$(document).ready(function(){
	inlineEditBooking.init();
	
	if( $('#booking-search-input').length > 0 ){
	
	$( "#booking-search-input" ).autocomplete({
		delay: 0,
		source: function(req, response) {
			$.getJSON(ajaxurl + "?callback=?&action=eo-search-bookings", req, function(data) {
				response($.map(data, function(item) {
					item.label = item.event;
					item.value = '#' + item.booking_id;
					return item;
				}));
			});
		},
		select: function( event, ui ) {  
	        window.location.href = ui.item.edit_link;  
	 }
	}).addClass("ui-widget-content ui-corner-left");
	
	
	/* Backwards compat with WP 3.3-3.5 (UI 1.8.16-1.8.2)*/ 
	var jquery_ui_version = $.ui ? $.ui.version || 0 : -1;
	var namespace = ( eventorganiser.versionCompare( jquery_ui_version, '1.9' ) >= 0 ? 'ui-autocomplete' : 'autocomplete' );
	var itemNamespace = ( eventorganiser.versionCompare( jquery_ui_version, '1.9' ) >= 0 ? 'ui-autocomplete-item' : 'item.autocomplete' );
	
	$('#booking-search-input').data(namespace)._renderItem = function(ul, item) {
		if( item.booking_id === 0 ){
			return $("<li></li>").data( itemNamespace, item ).append( item.label).appendTo(ul);
		}
		
		var term = (this.term+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\\<\>\|\:])/g, "\\$1");
		if( term.charAt(0) == '#' ){
			term = term.substr(1);
		}
		var re = new RegExp( "(" + term+ ")" , 'gi' );
		
		item.booking_id = item.booking_id.replace( re, "<span style='font-weight:bold;'>$1</span>" );
		item.bookee = item.bookee.replace( re, "<span style='font-weight:bold;'>$1</span>" );
		item.event = item.event.replace( re, "<span style='font-weight:bold;'>$1</span>" );
		item.bookee_email = item.bookee_email.replace( re, "<span style='font-weight:bold;'>$1</span>" );
		
		return $("<li></li>").data("item.autocomplete", item).append( 
					"<a style='overflow:auto'>"
						+ "<span style='float:left; margin-right:10px;'>" 
							+ "<span style='color:#333;line-height:50px;font-size:14px;'>#" + item.booking_id + "</span>"
						+ "</span>"
						+ "<span style='float:left'>" 
							+ item.event + "</br>"
							+ "<span style='font-size: 0.8em'><em>" + item.bookee + " <br/>"+ item.bookee_email + "</em></span>"
						+ "</span>"
						+ "<span style='clear:both'></span>"
					+ "</a>"
				).appendTo(ul);
	};
	
	}
	
	
	if( $('#event-bookings').length > 0 ){
		
		var $filterWrap = $('.eo-booking-filters');
		var $eventID = $filterWrap.find( '[name="event_id"]');
		var $filterButton = $filterWrap.find( '#event-bookings-filter' );
		var $occurrence_select = $('#event-occurrence-bookings');
		var $spinner           = $('#event-search-spinner');
		
		
		$( "#event-bookings" ).autocomplete({
			delay: 0,
			source: function(req, response) {
				$.getJSON(ajaxurl + "?action=eo-search-events", req, function(data) {
					response($.map(data.results, function(item) {
						item.label = item.event;
						item.value = item.event;
						//item.value = '#' + item.event_id;
						return item;
					}));
				});
			},
			select: function( event, ui ) {  
				
				$eventID.val( ui.item.event_id );
				
				if( $occurrence_select.length > 0 ){

					$occurrence_select.html( "" );
					$spinner.show();
					
					$.getJSON(
						ajaxurl + "?action=eo-search-occurrences", 
						{event_id: ui.item.event_id}, 
						function(data) {
							$.map(data.results, function( occurrence ) {
								var date = new Date( occurrence.start );
								$occurrence_select.append( 
									"<option value='"+occurrence.occurrence_id+"'>" 
										+ eventorganiser.format_date( 'jS F Y', date )
									+ "</option>" 
								);
							});
					
							$spinner.hide();
							$occurrence_select.show();
						}
					);
				
				}
				
				$filterButton.show();
			}
		}).addClass("ui-widget-content ui-corner-left");
		
	}
	
});
})(jQuery);