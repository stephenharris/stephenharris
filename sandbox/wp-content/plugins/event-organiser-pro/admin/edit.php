<?php

/**
 * Register event metaboxes
 */
function eventorganiser_pro_add_meta_boxes() {
	add_meta_box( 'eventorganiser_tickets', __( 'Event Tickets', 'eventorganiserp' ), 'eventorganiser_tickets_metabox', 'event', 'normal' );
}
add_action( 'add_meta_boxes_event', 'eventorganiser_pro_add_meta_boxes' );

/**
 * Apend warning notice to recurring events
 */
function eventorganiser_pro_booking_warning( $notices ) {
	if ( !empty( $notices ) ) $notices .= '</br>';
	return $notices . 
		esc_html__( 'If you edit the event\'s dates you will need to update any event tickets. If you remove a date, any bookings for that date will be ignored until you re-assign them an event date.', 'eventorganiserp' );
}
add_filter( 'eventorganiser_event_metabox_notice', 'eventorganiser_pro_booking_warning', 10 );


/**
 * Display ticket metabox
 * @param object $post The event post
 */
function eventorganiser_tickets_metabox( $post ) {

	// Use nonce for verification
	wp_nonce_field( 'event_organiser_pro_edit_event_'.$post->ID, '_eventorganiser_pro_nonce', false, true );
	$currency = eventorganiser_pro_get_option('currency','US');
		
	?>
	<script type="text/template" id="tmpl-eo-tickets-table">
	<table class="wp-list-table eo-event-ticket-table eo-ticket-table tickets">
		<thead class="eo-tickets-header">
			<tr>
				<th scope="col" id="ticket" class="manage-column column-ticket">
					<?php esc_html_e('Ticket', 'eventorganiserp' ); ?>
				</th>
				<th scope="col" id="ticket_spaces" class="manage-column column-ticket_spaces">
					<?php esc_html_e( 'Spaces', 'eventorganiserp' );?>
				</th>
				<th scope="col" id="ticket_price" class="manage-column column-ticket_price">
					<?php printf( esc_html__( 'Price (%s)', 'eventorganiserp' ), eventorganiser_get_currency_symbol( $currency ) );?>
				</th>
				<th scope="col" id="ticket_actions" class="manage-column column-ticket_actions"></th>
			</tr>
		</thead>
		
		<tr class="no-items">
			<td class="colspanchange" colspan="4">
				<?php esc_html_e( 'No tickets', 'eventorganiserp' ); ?>
			</td>
		</tr>			
	</table>

	<p>
		<a href="#" class="button" id="eo-add-ticket">
			<?php esc_html_e( 'Add Ticket', 'eventorganiserp' ); ?> 
			<span class="eo-add-ticket-plus">&#10010;</span>
		</a>
	</p>
	</script>
	
	<script type="text/template"  id="tmpl-eo-ticket">
		<tr id="ticket-{{cid}}" class="eo-ticket" data-eo-ticket-row-id="{{cid}}">
		
			<td class="eo-ticket-cell ticket column-ticket">
				<input type="hidden" value="update" class="eo-ticket-action" name="eventorganiser_pro[ticket][{{ cid }}][action]">
				<input type="hidden" value="{{ order }}" class="eo-ticket-order" name="eventorganiser_pro[ticket][{{ cid }}][order]">
				<input type="hidden" value="{{ mid }}" class="eo-ticket-id" name="eventorganiser_pro[ticket][{{ cid }}][ticket_id]"> 
				<span class="eo-ticket-name">{{ name }}</span>
			</td>
		
			<td class="eo-ticket-cell ticket_spaces column-ticket_spaces">
				<span class="eo-ticket-spaces">{{ spaces }}</span>
			</td>
		
			<td class="eo-ticket-cell ticket_price column-ticket_price">
				<span class="eo-ticket-price"> {{ price }}</span>
			</td>
		
			<td class="eo-ticket-cell ticket_actions column-ticket_actions">
				<span class="eo-ticket-actions-wrap">
					<a href="#" class="eo-edit-ticket"> 
						<?php esc_html_e('Edit', 'eventorganiserp' );?> 
						<span class="eo-settings-toggle-arrow">&#x25BC;</span>
					</a>
					<a href="#" class="eo-delete-ticket" style="color:#b94a48;">
						<?php esc_html_e('Delete', 'eventorganiserp' );?> 
						<span class="eo-delete-ticket-symbol">&#10006;</span>
					</a>
					
					<a href="#" class="eo-move-ticket-up" style="<# if( _first ){ #> visibility: hidden; <# } #>font-weight:bold;font-size: 20px;vertical-align: middle;"> &#11014; </a>
					<a href="#" class="eo-move-ticket-down" style="<# if( _last ){ #> visibility: hidden; <# } #>font-weight:bold;font-size: 20px;vertical-align: middle;"> &#11015; </a>
				
				</span>
			</td>
		</tr>

		<tr id="ticket-{{cid }}-settings" class="eo-ticket-settings" style="display:none">

			<td colspan="5">			
				<div id="eo-ticket-form-dialog" style="width: 50%;float: left;">

				<table class="ticket-settings-table form-table">
				<tbody>
					<tr>
						<th class="eo-ticket-cell"> <?php esc_html_e('Ticket name:', 'eventorganiserp' );?> </th>
						<td class="eo-ticket-cell">
							<input type="text" placeholder="Concession" name="eventorganiser_pro[ticket][{{cid }}][name]" class="eo-ticket-input-name ui-autocomplete-input ui-widget-content ui-corner-all" value="{{ name }}">
						</td>
					</tr>
					<tr>
						<th class="eo-ticket-cell"> <?php esc_html_e('Price:', 'eventorganiserp' );?> </th>
						<td class="eo-ticket-cell">
							<span class="eo-ticket-input-currency">
								<?php echo eventorganiser_get_currency_symbol( eventorganiser_pro_get_option( 'currency' ) ); ?>
							</span>
							<input type="text" placeholder="0.00" name="eventorganiser_pro[ticket][{{cid }}][price]" class="eo-ticket-input-price ui-autocomplete-input ui-widget-content ui-corner-all" value="{{ price }}">
						</td>
					</tr>
					<tr>
						<th class="eo-ticket-cell"> <?php esc_html_e('Spaces:', 'eventorganiserp' );?> </th>
						<td class="eo-ticket-cell">
							<input type="number" placeholder="20" name="eventorganiser_pro[ticket][{{cid }}][spaces]" class="eo-ticket-input-spaces" value="{{ spaces }}" style="width:auto;" max="999999" min="0">
							<?php eventorganiser_inline_help( 
								__('Spaces', 'eventorganiserp' ),
								__( 'This indicates the quanity available of this ticket type. If you are selling tickets for individual occurrences, then this limit applies to each occurrence.', 'eventorganiserp')
								.sprintf(
									'<p><a href="%s">%s</a>.</p>',
									'http://wp-event-organiser.com/pro-features/flexible-ticket-options/',
									__('See this page for more details', 'eventorganiserp' )
 								),
 								true
							); ?>
						</td>
					</tr>
					<tr>
						<th class="eo-ticket-cell"> <?php esc_html_e('Tickets on sale:', 'eventorganiserp' );?> </th>
						
						<td class="eo-ticket-cell">
							<div>
								<div> 
									<?php _e( 'Sale starts', 'eventorganiserp' ); ?>
									<?php eventorganiser_inline_help( 
									__('Ticket availability', 'eventorganiserp' ),
									__( 'You can specify a date when this ticket will go on sale, and when it is no longer available for purchase. Leave blank for no restriction. Tickets will automatically come off sale when the event starts', 'eventorganiserp')
									.sprintf(
										'<p><a href="%s">%s</a>.</p>',
										'http://wp-event-organiser.com/pro-features/flexible-ticket-options/',
										__('See this page for more details', 'eventorganiserp' )
 									),
 									true
									); ?> 
								</div>
						
								<input type="text" data-eo-sale-period="from" name="eventorganiser_pro[ticket][{{cid }}][from]" value="{{ from }}" size="10" class="eo-ticket-input-sale-period eo-ticket-input-from ui-autocomplete-input ui-widget-content ui-corner-all">
								<input type="text" data-eo-sale-period="from_time" name="eventorganiser_pro[ticket][{{cid }}][from_time]" value="{{ from_time }}" size="5" class="eo-ticket-input-sale-period eo-ticket-input-from-time ui-autocomplete-input ui-widget-content ui-corner-all">
							</div>
				
							<div>
								<div> <?php _e( 'Sale ends', 'eventorganiserp' ); ?> </div>
								<input type="text" data-eo-sale-period="to" name="eventorganiser_pro[ticket][{{cid }}][to]" value="{{ to }}" size="10" class="eo-ticket-input-sale-period eo-ticket-input-to ui-autocomplete-input ui-widget-content ui-corner-all">
								<input type="text" data-eo-sale-period="to_time" name="eventorganiser_pro[ticket][{{cid }}][to_time]" value="{{ to_time }}" size="5" class="eo-ticket-input-sale-period eo-ticket-input-to-time ui-autocomplete-input ui-widget-content ui-corner-all">
							</div>
				
							<p class="description">
								<?php _e( 'Bookings will be closed when an occurrences starts.', 'eventorganiserp' ); ?>
							</p>

						</td>
					</tr>

					<?php do_action( 'eventorganiser_ticket_options_after' ); ?>
				</tbody>
				</table>
			</div>
	
			<div style="text-align:center;width: 50%;float: left;<?php if( eventorganiser_pro_get_option('book_series') ){ echo "display:none;"; } ?>">
					
				<div class="eo-ticket-occurrences-input"></div>
				
				<textarea style="display:none;" data-eo-ticket-dates="selected" class="eo-ticket-input-dates" id="eop_ticket_selected_{{cid }}" name="eventorganiser_pro[ticket][{{cid }}][selected_dates]">{{ selected }}</textarea>
				<textarea style="display:none;" data-eo-ticket-dates="deselected" class="eo-ticket-input-dates" id="eop_ticket_deselected_{{cid }}" name="eventorganiser_pro[ticket][{{cid }}][deselected_dates]">{{ deselected }}</textarea>
				
				<a href="#" class="eo-select-all"><?php esc_html_e('Select all', 'eventorganiserp' );?></a> &nbsp;|&nbsp; <a href="#" class="eo-deselect-all"><?php esc_html_e('Deselect all', 'eventorganiserp' );?></a>
				
				<p class="description">
					<?php esc_html_e('Select the dates of the event for which you want this ticket to be sold. (Default: all dates).', 'eventorganiserp' );?> 
					
					<?php eventorganiser_inline_help( 
						__('Ticket dates', 'eventorganiserp' ),
						__( 'You have the option of selling tickets for only specific dates, for instance you may wish to make a cheaper tickets available for only the first occurrence.', 'eventorganiserp')
						.sprintf(
							'<p><a href="%s">%s</a>.</p>',
							'http://wp-event-organiser.com/pro-features/flexible-ticket-options/',
							__('See this page for more details', 'eventorganiserp' )
						),
						true
					); ?>
				</p>				
			</div>
			
		</td>
	</tr>
	</script>
	
	
	<div id="eo-tickets-table"></div>
	
	<?php

	$cap = (int) get_post_meta( $post->ID, '_eventorganiser_booking_cap', true );

	printf(
		'<p>'.esc_html__( 'Cap booking at %s places', 'eventorganiserp' ).'</p>',
		eventorganiser_text_field( array(
				'id' => 'eo-cap-booking',
				'name' => 'eo_cap_booking',
				'placeholder' => __( 'Do not cap', 'eventorganiserp' ),
				'echo' => 0,
				'type' => 'number',
				'size' => 3,
				'style' => 'width:auto;',
				'min' => 0,
				'value' => $cap ? $cap : null,
			) )
	);
	
	printf( '<label> %s', __( 'Select a booking form:', 'eventorganiserp' ) ) . ' ';
	eventorganiser_forms_dropdown( array(
		'selected' => (int) get_post_meta( $post->ID, '_eventorganiser_booking_form', true ),
		'name' => 'eo_booking_form',
		'echo' => '1'
	));
	echo '</label>';
	
	do_action( 'eventorganiser_booking_metabox_bottom', $post->ID );
}


/**
 * Save event tickets.
 * Hooked onto to eventorganiser_save_event
 * @param int $post_id Event post ID
 */
function eventorganiser_pro_save_event( $post_id ) {

	// verify this is not an auto save routine.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	//authentication checks
	if ( !current_user_can( 'edit_event', $post_id ) ) return;

	//Sanity check
	if ( !isset( $_POST['eventorganiser_pro'] ) || !isset( $_POST['_eventorganiser_pro_nonce'] ) ) return;

	//Nonce check
	if( !wp_verify_nonce( $_POST['_eventorganiser_pro_nonce'], 'event_organiser_pro_edit_event_'.$post_id ) ) return;


	$schedule = eo_get_event_schedule( $post_id );

	/* Expect ticket dates to be in d-m-Y format! See default value of eo_format_datetime()*/
	$occurrences = array_map( 'eo_format_datetime', $schedule['_occurrences'] );

	$cap = isset( $_POST['eo_cap_booking'] ) ? intval( $_POST['eo_cap_booking'] ) : 0;
	$booking_form = isset( $_POST['eo_booking_form'] ) ? intval( $_POST['eo_booking_form'] ) : 0;
	update_post_meta( $post_id, '_eventorganiser_booking_cap', $cap );
	update_post_meta( $post_id, '_eventorganiser_booking_form', $booking_form );

	if( !empty($_POST['eventorganiser_pro']['ticket']) ){
		$tickets = $_POST['eventorganiser_pro']['ticket'];
		
		foreach ( $tickets as $ticket_id => $ticket_data ) {

			if ( !isset( $ticket_data['action'] ) || 'delete' == $ticket_data['action'] ) {
				//Delete ticket

				//If ticket hasn't been created yet, skip
				if ( empty( $ticket_data['ticket_id'] ) )
					continue;

				//Delete ticket
				$ticket_id = (int) $ticket_data['ticket_id'];
				eventorganiser_delete_ticket( $ticket_id );

			}else {
								
				if ( !empty( $ticket_data['to'] ) ) {
					$time = !empty( $ticket_data['to_time'] ) ? $ticket_data['to_time'] : '23:59';
					//Potentially need to parse 24
					$time = date( "H:i", strtotime( $time ) );
					$ticket_data['to'] = _eventorganiser_check_datetime( $ticket_data['to'] . ' ' . $time );
				}else {
					$ticket_data['to'] = false;
				}

				if ( !empty( $ticket_data['from'] ) ) {
					$time = !empty( $ticket_data['from_time'] ) ? $ticket_data['from_time'] : '00:00';
					//Potentially need to parse 24
					$time = date( "H:i", strtotime( $time ) );
					
					$ticket_data['from'] = _eventorganiser_check_datetime( $ticket_data['from'] . ' ' . $time );
				}else {
					$ticket_data['from'] = false;
				}
				

				//For each ticket... insert/update ticket information
				if ( !empty( $ticket_data['ticket_id'] ) ) {
					//update
					$ticket_id = (int) $ticket_data['ticket_id'];
	
					//Get selected occurrence dates
					if( $ticket_data['selected_dates'] == -1 ){
						//When creating a ticket for the first time, we deselect dates rather than select them
						$excluded_dates = array_filter( explode( ',', $ticket_data['deselected_dates'] ) );
						$selected_dates =array_diff( $occurrences, $excluded_dates );

					}else{
						$selected_dates = array_filter( explode( ',', $ticket_data['selected_dates'] ) );
						$selected_dates = array_intersect( $occurrences, $selected_dates );
					}
					
					//Array of occurrence IDs for which this ticket is available
					$selected_ids = array_keys( $selected_dates );
					$ticket_data['occurrence_ids'] = $selected_ids;

					eventorganiser_update_ticket( $ticket_id, $ticket_data );

				}else {
					
					//Used to remove ticket from $_POST, see below.
					$index = $ticket_id; 
					
					//When creating a ticket for the first time, we deselect dates rather than select them
					$excluded_dates = array_filter( explode( ',', $ticket_data['deselected_dates'] ) );
					$selected_dates =array_diff( $occurrences, $excluded_dates );

					//Array ofccurrence IDs for which this ticket is available
					$selected_ids = array_keys( $selected_dates );
					$ticket_data['occurrence_ids'] = $selected_ids;
					$ticket_id = eventorganiser_insert_ticket( $post_id, $ticket_data );
					
					//Remove ticket from $_POST, @see http://wp-event-organiser.com/forums/topic/tickets-being-duplicated/
					unset( $_POST['eventorganiser_pro']['ticket'][$index] );
				}
				
			}//If deleting / updating
			
		}//Foreach ticket
		
	}//If tickets
	
}
add_action( 'eventorganiser_save_event', 'eventorganiser_pro_save_event' );