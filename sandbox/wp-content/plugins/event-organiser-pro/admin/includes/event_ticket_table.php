<?php
class EO_Booking_Tickets_Table{

	function __construct( $booking_id ){
		//Set parent defaults
		$this->_args = array(
			'singular'   => 'booking-ticket',
			'plural'     => 'booking-tickets',
			'booking_id' => $booking_id,
			'count'		 => 0,
		);
	}
	
	function no_items(){
		esc_html_e( 'No tickets', 'eventorganiserp' );
	}
	
	function setup_columns(){
		$columns = array(
			'ticketref'      => __( 'Ticket Reference', 'eventorganiserp' ),
			'ticket'         => __( 'Ticket', 'eventorganiserp' ),
			'ticket_price'   => __( 'Price', 'eventorganiserp' ),
			'ticket_actions hide-if-no-js' => '',
		);

		$this->_columns = apply_filters( 'eventorganiser_booking_tickets_table', $columns );
	}

	function column_ticketref( $item ){
		echo esc_html( $item->ticket_reference );
	}
	
	function column_ticket( $item ){
		echo esc_html( $item->ticket_name );
	}
	
	function column_ticket_price( $item ){
		echo esc_html( eo_format_price( $item->ticket_price, true ) );
	}
	
	function column_default( $item, $column_name ){

		if( 'ticket_actions hide-if-no-js' == $column_name ){
			printf(
				'<div class="row-actions">
					<span class="delete"><a href="#" class="submitdelete deletion">%s</a></span>
					<input type="checkbox" style="display:none" class="eo-delete-ticket-cb" name="eo_delete_ticket[]" value="%d">
				</div>', 
				esc_html__( 'Remove', 'eventorganiserp' ), 
				$item->booking_ticket_id 
			);
		}else{
			do_action( 'eventorganiser_booking_tickets_table_column', $column_name, $item ); 
		}
	}

	function single_row( $item ) {
		$row_id = 'id="ticket-'.intval( $item->booking_ticket_id ).'"';
		echo '<tr '.$row_id.' >';
		echo $this->single_row_columns( $item );
		echo '</tr>';
		$this->_args['count']++;
	}

	/**
	 * Prepare booing tickets for display
	 * @uses eo_get_booking_tickets()1
	 */
	function prepare_items() {
		$this->setup_columns();
		$booking_id = $this->_args['booking_id'];
		$this->items = eo_get_booking_tickets( $booking_id, false );
		$this->_args['tickets'] = count( $this->items );
	}

	/**
	 * Get a list of CSS classes for the <table> tag
	 *
	 * @since 3.1.0
	 * @access protected
	 *
	 * @return array
	 */
	function get_table_classes() {
		return array( 'eo-ticket-table eo-bookings-ticket-table', $this->_args['plural'] );
	}

	function display_tablenav( $which ){}
	
	function display() {
		$this->prepare_items();
	
		?>
		<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
			<thead>
				<tr><?php $this->print_column_headers(); ?></tr>
			</thead>
			<tbody id="eo-ticket-list">
				<?php $this->display_rows(); ?>
			</tbody>
		</table>
	
		<?php
	}
	
	/**
	 * Print column headers, accounting for hidden and sortable columns.
	 *
	 * @since 1.0
	 * @access protected
	 *
	 * @param bool $with_id Whether to set the id attribute or not
	 */
	function print_column_headers( $with_id = true ) {
	
		$columns = $this->_columns;
	
		foreach ( $columns as $column_key => $column_display_name ) {
			$class = array( 'manage-column', sanitize_html_class( "column-$column_key" ) );
			$id    = $with_id ? "id='".esc_attr( $column_key ) . "'" : '';
	
			if ( !empty( $class ) ){
				$class = "class='" . join( ' ', $class ) . "'";
			}
	
			printf(
				"<th scope='col' {$id} {$class} >%s</th>",
				esc_html( $column_display_name )
			);
		}

	}
		
	/**
	 * Generate the table rows
	 *
	 * @since 1.0
	 * @access protected
	 */
	function display_rows() {
		foreach ( $this->items as $item ){
			$this->single_row( $item );
		}
	}
	
	/**
	 * Generates the columns for a single row of the table
	 *
	 * @since 3.1.0
	 * @access protected
	 *
	 * @param object $item The current item
	 */
	function single_row_columns( $item ) {
	
		$columns = $this->_columns;

		foreach( $columns as $column_name => $column_display_name ){
			
			$class = sprintf( 'class="%1$s column-%1$s"', sanitize_html_class( $column_name ) );

			if( method_exists( $this, 'column_' . $column_name ) ){
				echo "<td $class>";
				call_user_func( array( $this, 'column_' . $column_name ), $item );
				echo '</td>';
			}else{
				echo "<td $class>";
				$this->column_default( $item, $column_name );
				echo '</td>';
			}
		}

	}	
} 
?>