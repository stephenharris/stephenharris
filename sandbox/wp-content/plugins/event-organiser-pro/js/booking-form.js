//TODO Future
//Target Address (requires 'contains')?
//Toggle Section / Hook / Gateway / Email / Name / Ticketpicker?
if ( typeof EO_SCRIPT_DEBUG === 'undefined') { EO_SCRIPT_DEBUG = true;}
var eobookingform = eobookingform || {};
eobookingform.Model = {};
eobookingform.models = {}; 
eobookingform.View = {};
eobookingform.views = {};

(function($) {
//======================================
// Models
//======================================
eobookingform.Model.EOFormElement = Backbone.Model.extend({
	
	initialize: function() {
		if( this.get('elements') ){
			_.each( this.get('elements'), function( el ) {
				eobookingform.models[el.id] = new eobookingform.Model.EOFormElement( el );
				eobookingform.views[el.id] = new eobookingform.Model.EOFormElementView( { model: eobookingform.models[el.id] } );	
			});
		}
    },
	
	is_visible: function(){
		
		var conditional = this.get('conditional');
		var action, gate, numConditions, trueConditions, conditionsMet;
		
		if( !conditional || !conditional.conditions ){
			return true;
		}
		
		gate           = ( conditional.gate == 'any' ? 'any' : 'all' );
		action         = ( conditional.action == 'show' ? 'show' : 'hide' );
		numConditions  = conditional.conditions.length;
		trueConditions = 0;
		
		for( var i=0; i < numConditions; i++ ){
			
			var target   = conditional.conditions[i].target;
			var operator = conditional.conditions[i].operator;
			var value    = conditional.conditions[i].value;
			//get value
			var targetValue = eobookingform.models[target].get('value');
			
			if( !target ){
				trueConditions++;
			}else{
				switch( operator ){
					case 'equals':
						if( targetValue == value ){
							trueConditions++;
						}
						break;
					case 'notequals':
						if( targetValue != value ){
							trueConditions++;
						}
						break;
					case 'greaterthan':
						if( targetValue > value ){
							trueConditions++;
						}
						break;
					case 'lessthan':
						if( targetValue < value ){
							trueConditions++;
						}
						break;
				}				
			}
		}//endfor
		
		conditionsMet = ( gate == 'all' ? ( numConditions == trueConditions ) : trueConditions > 0 );
		return ( conditionsMet == ( action == 'show' ) );
	},
	 
});


//======================================
// Views
//======================================
eobookingform.Model.EOFormElementView = Backbone.View.extend({
    
	events: {
		'change input': 'valueChanged',
		'change select': 'valueChanged',
		'change textarea': 'valueChanged',
	},
	
	initialize: function() {
		
		if( $('#eo-booking-field-'+this.model.id ).parents('.eo-booking-field').length > 0 ){
			this.setElement( $('#eo-booking-field-'+this.model.id ).closest('.eo-booking-field') );
			
		}else if( this.model.get('type') == 'gateway' ){
			this.setElement( $( '#eo-booking-form-element-wrap-gateway' ) );
		
		}else{
			this.setElement( $('#eo-booking-form-element-wrap-'+this.model.id) );
		}

    },
    
    valueChanged: function( ev ){
    	this.model.set( 'value', $(ev.target).val() );
    },

    toggleVisbility: function(){
    	if( this.model.is_visible() ){
    		this.$el.fadeIn( 1000 );
    	}else{
    		this.$el.fadeOut( 700 );
    	}
    }
});

eobookingform.Model.EOFormElementCheckboxView = eobookingform.Model.EOFormElementView.extend({
	valueChanged: function( ev ){
    	var values = this.$el.find( ':checked').map(function(){return $(this).val();}).get();
    	this.model.set( 'value', values );
    },
});

eobookingform.Model.EOFormElementRadioView = eobookingform.Model.EOFormElementView.extend({
	valueChanged: function( ev ){
    	var value = this.$el.find( 'input[type=radio]:checked').val();
    	this.model.set( 'value', value );
    },
});

eobookingform.Model.EOFormElementTermsConditionsView = eobookingform.Model.EOFormElementView.extend({
	valueChanged: function( ev ){
    	var value = this.$el.find( '.eo-booking-field-terms-conditions').is(':checked');
    	this.model.set( 'value', value );
    }, 
});

eobookingform.Model.EOFormElementNameView = eobookingform.Model.EOFormElementView.extend({
	valueChanged: function( ev ){
		var values = this.$el.find( 'input.eo-booking-field-name' ).map(function(){return $(this).val();}).get();
    	this.model.set( 'value', values );
    },
});
})(jQuery);


//======================================
//Initialize
//======================================
jQuery(document).ready(function($) {

	//Initiate views and models
	for( var index in eobookingform.form.elements ){
		var el = eobookingform.form.elements[index];
		eobookingform.models[el.id] = new eobookingform.Model.EOFormElement( el );
		
		switch( eobookingform.models[el.id].get('type') ){
		
			case 'checkbox':
				eobookingform.views[el.id] = new eobookingform.Model.EOFormElementCheckboxView( { model: eobookingform.models[el.id] } );
				break;
			case 'radio':
				eobookingform.views[el.id] = new eobookingform.Model.EOFormElementRadioView( { model: eobookingform.models[el.id] } );
				break;
			case 'terms_conditions':
				eobookingform.views[el.id] = new eobookingform.Model.EOFormElementTermsConditionsView( { model: eobookingform.models[el.id] } );
				break;
			case 'name':
				eobookingform.views[el.id] = new eobookingform.Model.EOFormElementNameView( { model: eobookingform.models[el.id] } );
				break;
				
			default:
				eobookingform.views[el.id] = new eobookingform.Model.EOFormElementView( { model: eobookingform.models[el.id] } );
		
		}
		
	}	

	for( var id in eobookingform.models ){
		
		var element = eobookingform.models[id];
		
		var conditional = element.get('conditional');
		
		if( !conditional || !conditional.conditions ){
			continue;
		}
		
		for( var i=0; i < conditional.conditions.length; i++ ){
			var condition = conditional.conditions[i];			
			if ( eobookingform.models.hasOwnProperty( condition.target ) ) {
				eobookingform.views[id].listenTo( eobookingform.models[condition.target], 'change:value', eobookingform.views[id].toggleVisbility );
			}		
		}
	}	
});