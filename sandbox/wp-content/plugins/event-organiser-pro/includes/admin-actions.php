<?php

function eventorganiser_admin_action(){
	if ( is_admin() && !empty( $_REQUEST['eo-action'] ) ){
		do_action( 'eventorganiser_admin_action_' . $_REQUEST['eo-action'] );
	}
}
add_action( 'admin_init','eventorganiser_admin_action', 12 );


function eventorganiser_init_action(){
	if ( !empty( $_REQUEST['eo-action'] ) ){
		do_action( 'eventorganiser_action_' . $_REQUEST['eo-action'] );
	}
}
add_action( 'wp_loaded', 'eventorganiser_init_action', 12 );

function eventorganiser_csv_export_listener(){

	require_once( EVENT_ORGANISER_PRO_DIR . 'includes/class-eo-export-csv.php' );

	$occurrence_id = ( !empty( $_REQUEST['occurrence_id'] )  ? intval( $_REQUEST['occurrence_id'] ) : 0 );
	$event_id      = ( !empty( $_REQUEST['event_id'] )  ? intval( $_REQUEST['event_id'] ) : 0 );
	$bookee_id     = ( !empty( $_REQUEST['bookee_id'] )  ? intval( $_REQUEST['bookee_id'] ) : 0 );
	$status        = ( !empty( $_REQUEST['status'] )  ? $_REQUEST['status'] : '' );
	$meta          = ( !empty( $_REQUEST['meta'] )  ? $_REQUEST['meta'] : '' );
	$args           = compact( 'occurrence_id', 'event_id', 'bookee_id', 'status', 'meta' );
	
	if( $event_id ){
		if( !current_user_can( 'edit_event', $event_id ) ){
			return;
		}
	}else{
		if( !current_user_can( 'manage_others_eo_bookings' ) ){
			return;
		}
	}

	$delimiter = isset( $_REQUEST['delimiter'] ) ? stripslashes( $_REQUEST['delimiter'] ) : ',';
	$text_delimiter = isset(  $_REQUEST['text_delimiter'] ) ? stripslashes( $_REQUEST['text_delimiter'] ) : '"';

	if( 'export-bookings' == $_REQUEST['eo-action'] ){
		$csv = new EO_Export_Bookings_CSV( $args );
		$csv->init();
		$csv->set_delimiter( $delimiter );
		$csv->set_text_delimiter( $text_delimiter );
		$csv->export();

	}elseif( 'export-tickets' == $_REQUEST['eo-action'] ){
		$csv = new EO_Export_Tickets_CSV( $args );
		$csv->init();
		$csv->set_delimiter( $delimiter );
		$csv->set_text_delimiter( $text_delimiter );
		$csv->export();
	}
}

add_action( 'eventorganiser_admin_action_export-bookings', 'eventorganiser_csv_export_listener' );
add_action( 'eventorganiser_admin_action_export-tickets', 'eventorganiser_csv_export_listener' );
