<?php
/**
 * Handles creation of new and 'existing' (in database) forms.
 * 
 * Ensures the create/retrieved form has the required fields. 
 * Performs backwards-compatability routines for old data sets.
 * 
 * //Existing booking form
 * $form = EO_Booking_Form_Factory::get_form( $form_id );
 * 
 * //New booking form
 * $form = EO_Booking_Form_Factory::get_form( array(
 *    'id'    => 'my-form-id',
 *    'title' => 'Bookings',
 *    'elements' => array(
 *        'my-element-id' => array(
 *            'type' => 'input',
 *            'label' => 'Foobar'
 *        ),
 *        ...
 *    )
 * ) );
 */
class EO_Booking_Form_Factory{
	
	static $forms = array();
	
	static $event_forms = array();
	
	/**
	 * Create a new form from the given attributes
	 * 
	 * $attributes['elements'] is extracted for creating elements in the
	 * form. Remaining $attributes are passed to EO_Booking_Form::__construct()
	 * 
	 * @param array $attributes
	 */
	static function create( array $attributes ){
		
		if( isset( $attributes['elements'] ) ){
			$elements = $attributes['elements'];
		}
		unset( $attributes['elements'] );
		
		$form = new EO_Booking_Form( $attributes );
		
		if( $elements ){
			foreach( $elements as $key => $element ){
		
				//Backwards compatible (element_type => type, element_id => id )
				if( !empty( $element['element_type'] ) && empty( $element['type'] ) ) {
					//Backwards compatible (radiobox -> radio )
					$element['element_type'] = ( $element['element_type'] == 'radiobox' ? 'radio' : $element['element_type'] );
					$element['type'] = $element['element_type'];
				}
				if( !isset( $element['id'] ) ){
					 $element['id'] = $key;
				}
				$element['element_id'] = $element['id'];
							
				if( 'name' == $element['type'] ){
					unset( $element['label_fname'] );
					unset( $element['label_lname'] );
				}
				
				$element = EO_Booking_Form_Element_Factory::create( $element );
				if( $element ){
					$form->add_element( $element );
				}
			}
		}
		
		//Add required elements if they are not present
		if( !$form->get_element( 'ticketpicker' ) ){
			$element = EO_Booking_Form_Element_Factory::create( array(
				'id'		=> 'ticketpicker',
				'type'		=> 'ticketpicker',
				'required'	=> 1,
				'position'	=> count( $form->get_elements() ),
			) );
			$form->add_element( $element );
		}
		
		if( !$form->get_element( 'gateway' ) ){
			$element = EO_Booking_Form_Element_Factory::create( array(
				'id'		=> 'gateway',
				'type'		=> 'gateway',
				'required'	=> 1,
				'label'		=> __( 'Select a payment gateway', 'eventorganiserp' ),
				'position'	=> count( $form->get_elements() )
			) );
			$form->add_element( $element );
		}
		
		if( !$form->get_element( 'submit' ) ){
			$element = EO_Booking_Form_Element_Factory::create( array(
				'id'		=> 'submit',
				'type'		=> 'button',
				'required'	=> 1,
				'label'		=> __( 'Book', 'eventorganiserp' ),
				'position'	=> count( $form->get_elements() )
			) );
			$form->add_element( $element );
		}
		
		if( !$form->get_element( 'email' ) ){
			$element = EO_Booking_Form_Element_Factory::create( array(
				'id'         => 'email',
				'type'       => 'email',
				'field_name' => 'email',
				'required'   => true,
				'label'      => __( 'Email', 'eventorganiserp' )
			) );
			$form->add_element( $element, array( 'at' => 0 ) );
		}
		
		if( !$form->get_element( 'name' ) ){
			$element = EO_Booking_Form_Element_Factory::create( array(
				'id'         => 'name',
				'type'       => 'name',
				'field_name' => 'name',
				'required'   => true,
				'label'      => __( 'Name', 'eventorganiserp' )
			) );
			$form->add_element( $element, array( 'at' => 0 ) );
		}

		return $form;
	}
	
	/**
	 * Retrieve an existing form by ID from the database
	 *
	 * Booking forms are stored as eo_booking_form post types. The data relating
	 * to the form is stored as post meta. Direct access is strongly discouraged.
	 * 
	 * This method retrieves that data and generates the booking form.
	 *
	 * @param int $form_id Form (eo_booking_form post type)
	 */
	static function get_form( $form_id ){
		
		if( $form_id <= 0 ){
			trigger_error( 'Invalid booking form ID', E_USER_WARNING );
		}
						
		if( 'eo_booking_form' !== get_post_type( $form_id ) ){
			return false;
		}
		
		if( isset( self::$forms[$form_id] ) ){
			return self::$forms[$form_id];
		}
		
		$attributes = array(
			'id' => $form_id,
		);
		
		//Elements
		$raw_elements           = get_post_meta( $form_id, '_eo_booking_form_fields', true );
		$attributes['elements'] = ( $raw_elements ? $raw_elements : array() );
		
		//Create form
		$form = self::create( $attributes );
		
		/* Set form name - admin use only */
		$form_obj = get_post( $form->id );
		$form->set( 'name', $form_obj->post_name );
		
		/* Set form title */
		$meta = get_post_custom( $form->id );
		if( !isset( $meta['_eventorganiser_booking_form_title'] ) ){
			$title = esc_html__( 'Booking', 'eventorganiserp' );
		}else{
			$title = $meta['_eventorganiser_booking_form_title'][0];
		}
		$form->set( 'title', $title );
		
		/* Form error & notices classes. These are filterable. See eventorganiser_booking_error_classes / eventorganiser_booking_notice_classes*/
		$classes = get_post_meta( $form->id, '_eventorganiser_booking_notice_classes', true );
		if( empty( $classes ) ) $classes = 'eo-booking-notice';
		$classes = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $classes ) ) );
		$form->set( 'notice_classes', $classes );
		
		$classes = get_post_meta( $form->id, '_eventorganiser_booking_error_classes', true );
		if( empty( $classes ) ) $classes = 'eo-booking-error';
		$classes = implode( ' ', array_map( 'sanitize_html_class', explode( ' ', $classes ) ) );
		$form->set( 'error_classes', $classes );
		
		/* Button text & class. TODO remove */
		$text = get_post_meta( $form->id, '_eventorganiser_booking_button_text', true );
		if( empty( $text ) ){
			$text = esc_attr__( 'Book', 'eventorganiserp' );
		}
		$form->set( 'button_text', $text );
		
		$class = get_post_meta( $form->id, '_eventorganiser_booking_button_classes', true );
		if( empty( $class ) ){
			$class = 'eo-booking-button';
		}
		$form->set( 'button_classes', $class );
		
		self::$forms[$form_id] = $form;
		
		return $form;
	} 
	
	
	/**
	 * Retrieves a booking form for a specific event.
	 *
	 * This gives the booking form context. It adds hidden fields
	 * for event ID & occurrence ID and populate data from $_POST
	 *
	 * @param int $form_id Form (eo_booking_form post type)
	 */
	static function get_event_form( $form_id, $event_id = false ){
		
		$event_id = $event_id ? $event_id : get_the_ID();
		
		if( isset( self::$event_forms[$form_id][$event_id] ) ){
			return self::$event_forms[$form_id][$event_id];
		}
		
		$form = self::get_form( $form_id );

		if( !$form ){
			return false;
		}
		
		$form->set( 'event_id', $event_id );
		
		//Set occurrence ID - 0 if we are booking a series
		if ( eventorganiser_pro_get_option( 'book_series' ) ) {
			$occurrence_id = 0;
			$disabled = false;
			
		}elseif ( 'event' == get_post_type() && !eo_reoccurs() ) {
			global $post;
			$occurrence_id = $post->occurrence_id;
			$disabled = false;
			
		}else {
			$occurrence_id = isset( $_REQUEST['eventorganiser']['booking']['occurrence_id'] ) ? (int) $_REQUEST['eventorganiser']['booking']['occurrence_id'] : '';
			$disabled = true;
		}
	
		//Add occurrence ID hidden field
		$form->add_element( new EO_Booking_Form_Element_Hidden( array(
			'id'		=> 'occurrence_id',
			'field_id' 	=> 'eo-booking-occurrence-id',
			'disabled' 	=> $disabled, //Disable then enable with js
			'class' 	=> ( $disabled ? 'eo-enable-if-js' : '' ),
			'value' 	=> $occurrence_id,
		)));
		
		//TODO - should this be removed?
		if( !is_user_logged_in() && 2 == eventorganiser_pro_get_option( 'allow_guest_booking' ) ){
			$email = $form->get_element( 'email' );
			if( $email ){
				$position = intval( $email->get( 'position' ) ) + 1;
				$parent   = $email->get_parent();
				$parent   = ( $parent ? $parent->id : false );
			
				$account_checkbox = new EO_Booking_Form_Element_Checkbox( array(
					'id'         => 'create-account',
					'field_name' => 'account',
					'name'       => 'account',
		    		'options'    => array( 1 => __( 'Create an account (optional)', 'eventorganiserp' ) ),
				));
				$form->add_element( $account_checkbox, array( 'at' => $position, 'parent' => $parent ) );
			}
		}	

		do_action_ref_array( 'eventorganiser_get_event_booking_form', array( &$form, $event_id ) );
		
		if( isset( $_POST['eventorganiser'] ) && isset( $_POST['eventorganiser']['booking'] ) ){

			$flattened = $form->flatten_elements();
			if( $flattened ){
			
				$input = $_POST;
			
				foreach( $flattened as $element ){
				
					$value      = null;
					$name       = $element->get_field_name();
					$name_parts = preg_split( '/[\[\]]+/', $name, -1, PREG_SPLIT_NO_EMPTY );
				
					$ref = &$input;

					while ( $name_parts ) {
						$part = array_shift( $name_parts );

						if( $name_parts && !isset( $ref[$part] ) ){
							break;
						}

						if ( !isset( $ref[$part] ) ) {
							break;
						}

						if( !$name_parts && isset( $ref[$part] ) ){
							$value = $ref[$part];
							break;
						}

						$ref = &$ref[$part];            		
					}
					
					if( isset( $value ) ){
						$element->set_value( stripslashes_deep( $value ) );	
					}
				}//foreach form elements
			}//if form elements
		}//if $_POST

		self::$event_forms[$form_id][$event_id] = $form;
		
		return $form;
	}
	
}
