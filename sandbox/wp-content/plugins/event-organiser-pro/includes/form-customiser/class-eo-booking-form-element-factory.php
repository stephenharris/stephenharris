<?php
/**
 * Handles creation of new and 'existing' (in database) forms
 * @ignore
 */
class EO_Booking_Form_Element_Factory{
	
	private static $types = array(
	
		//Standard
		'select'      => 'EO_Booking_Form_Element_Select',
		'input'       => 'EO_Booking_Form_Element_Input',
		'textarea'    => 'EO_Booking_Form_Element_Textarea',
		'radio'       => 'EO_Booking_Form_Element_Radio',
		'checkbox'    => 'EO_Booking_Form_Element_Checkbox',
		'multiselect' => 'EO_Booking_Form_Element_Multiselect',
	
		//Advanced
		'number'   => 'EO_Booking_Form_Element_Number',
		'section'  => 'EO_Booking_Form_Element_Section',
		'html'     => 'EO_Booking_Form_Element_Html',
		'fieldset' => 'EO_Booking_Form_Element_Fieldset',
		'address'  => 'EO_Booking_Form_Element_Address',
		'phone'    => 'EO_Booking_Form_Element_Phone',
		'email'    => 'EO_Booking_Form_Element_Email',
		'date'     => 'EO_Booking_Form_Element_Date',
		'url'      => 'EO_Booking_Form_Element_Url',
		'antispam' => 'EO_Booking_Form_Element_Antispam',
		'terms_conditions' => 'EO_Booking_Form_Element_Terms_Conditions',
		'hook'     => 'EO_Booking_Form_Element_Hook',
	
		//Required
		'gateway'      => 'EO_Booking_Form_Element_Gateway',
		'ticketpicker' => 'EO_Booking_Form_Element_Ticketpicker',
		'name'         => 'EO_Booking_Form_Element_Name',
		'button'       => 'EO_Booking_Form_Element_Button',
	
	);
	
	private static $backwards_compat = false;
	
	private static function backwards_combat(){
		
		//Run once
		if( false !== self::$backwards_compat ){
			return self::$backwards_compat;
		}
		
		self::$backwards_compat = array();
		
		$backwards = array(
			'standard'  => array(),
			'advanced'  => array(),
			'_required' => array(),
		);
		
		//Filter these to allow add-ons to add field types & metaboxes
		$backwards = apply_filters( 'eventorganiser_booking_form_element_types', self::$backwards_compat );
		
		foreach( $backwards as $metabox ){
			if( $metabox ){
				foreach( $metabox as $element => $class ){
					self::$backwards_compat[ $element ] = $class;
				}
			}
		}
		
		self::$types = array_merge( self::$types, self::$backwards_compat );

	}
	
	public static function create( $parameters = array() ){
	
		self::backwards_combat();

		if( !isset( $parameters['type'] ) ){
			trigger_error( __( 'Element type not given', 'eventorganiserp' ) );
			return false;
		}elseif( !isset( self::$types[ $parameters['type'] ] ) ){
			trigger_error( sprintf( __( 'Element type "%s" not recognised', 'eventorganiserp' ), $parameters['type'] ) );
			return false;
		}elseif( !isset( $parameters['id'] ) ){
			trigger_error( __( 'No element ID provided', 'eventorganiserp' ) );
			return false;
		}elseif( in_array( $parameters['id'], array( 'id', 'type' ) ) ){
			trigger_error( sprintf( __( 'The element ID "%s" is not allowed', 'eventorganiserp' ), $parameters['id'] ) );
			return false;
		}
		
		$classname = self::$types[ $parameters['type'] ];
		$element   = new $classname( $parameters );
		
		$element->type = $parameters['type'];
	
		return $element;
	}
	
	public static function register( $id, $class ){
		self::$types[ $id ] = $class;
	}
	
}
