<?php
//TODO
//Name?
//Gateway?
//Can't hide: HTML / HOOK 
//Should hide?  TicketPicker
/**
 * Abstract class for booking form element (front-end) view.
 * @author stephen
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class EO_Booking_Form_Element_View implements iEO_Booking_Form_View{

	/**
	 * The EO_Booking_Form_Element instance attached to this model.
	 * @var EO_Booking_Form_Element
	 */
	var $element = false;
	
	/**
	 * Prefix used by the view.
	 * @var string
	 */
	static $prefix = 'booking';
	
	/**
	 * How the field should be wrapped
	 *  %1$s - Classes for the form field (e.g. classes to indicate an error for this field)
	 *  %2$s - The actual content of the element view
	 *  %3$s - The element ID
	 *  %4$s - Element style - internal use only (i.e. conditional logic visibility).
	 * @var string
	 */
	var $wrap = '<div id="%3$s" class="eo-booking-field %1$s" %4$s>%2$s</div>';

	/**
	 * Sets up the view instance and attaches the mode.
	 * @param EO_Booking_Form_Element $element
	 */
	final function __construct( $element ){
		$this->element = $element;
	}

	/**
	 * Returns the mark-up for the element on the front-end
	 * @return string HTML mark-up for this form element
	 */
	//abstract function render(); Issues on php5.2? @see http://stackoverflow.com/questions/17525620/php-fatal-error-cant-inherit-abstract-function
	
	/**
	 * Returns the CSS class of this element
	 *
	 * Gets admin-set classes and adds the appropriate core class.
	 * @see get()
	 * @return string The HTML class attribute for this element
	 */
	function get_class(){
		$class = $this->element->get( 'class' ) ? trim( $this->element->get( 'class' ) ) : '';
		$class .= ' eo-booking-field-' . str_replace( '_', '-', $this->element->type );
		$class = apply_filters( 'eventorganiser_booking_element_classes', $class, $this->element );
		return trim( $class );
	}
	
	/**
	 * Returns the field name for use with the `name=""` attribute
	 * Please note that `$this->element->get_field_name()` is where an element looks 
	 * for its data in `$_POST`. As such, barring some exceptional circumstances,
	 * ({@see EO_Booking_Form_Element_Antispam_View}) this method should not be over-riddden.
	 * 
	 * @final
	 * @param string $component To return a component specific field name. Usual {fieldname}[{component}]
	 * @return string
	 */
	function get_name( $component = false ){
		return $this->element->get_field_name( $component );
	}
	
}

/** @ignore **/
class EO_Booking_Form_Element_Input_View extends EO_Booking_Form_Element_View{

	function render(){
		ob_start();
		include( eo_locate_template( 'eo-booking-form-input.php', false ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	function get_value(){
		return $this->element->get_value();
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Hidden_View extends EO_Booking_Form_Element_View{

	var $wrap = '%2$s';
	
	function render(){
		if( is_array( $this->element->get_value() ) ){
			$html = '';
			$base_id = $this->element->get( 'field_id' );
			
			foreach( $this->element->get_value() as $key => $value ){
				$html .= eventorganiser_text_field(array(
					'type' 	=> 'hidden',
					'data' 	=> $this->element->get( 'data' ),
					'class' => $this->element->get_class(),
					'id' 	=> $base_id ? $base_id . '-'.  $key : false,
					'name' 	=> $this->element->get_field_name( $key ),
					'value' => $value,
					'echo' 	=> 0,
				));
			}
		}else{
			$html = eventorganiser_text_field(array(
				'type' 	=> 'hidden',
				'data' 	=> $this->element->get( 'data' ),
				'class' => $this->element->get_class(),
				'id' 	=> $this->element->get( 'field_id' ),
				'name' 	=> $this->element->get_field_name(),
				'value' => $this->element->get_value(),
				'echo' 	=> 0,
			));
		}
		return $html;
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Select_View extends EO_Booking_Form_Element_View{

	function get_options(){
			
		$options = $this->element->get( 'options' );
		
		if( eventorganiser_is_associative( $options ) ){
			$select_options = $this->element->get( 'options' );
		}else{
			$select_options = array_combine( $this->element->get( 'options' ), $this->element->get( 'options' ) );
		}
		
		return $select_options;
	}
	
	function render(){
		$multiselect = ( $this->element->type == 'multiselect' );
		ob_start();
		if( !$multiselect ){
			include( eo_locate_template( 'eo-booking-form-select.php', false ) );	
		}else{
			include( eo_locate_template( 'eo-booking-form-multiselect.php', false ) );
		}
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	function selected( $option, $echo = true ){
		return selected( $this->element->is_selected( $option ), true , $echo );
	}
	
}

/** @ignore **/
class EO_Booking_Form_Element_Antispam_View extends EO_Booking_Form_Element_View{

	function render(){
		
		$n1 = (int) $this->element->get( 'number_1' );
		$n2 = (int) $this->element->get( 'number_2' );
		
		$this->element->set( 'label', sprintf( __( 'What is %d + %d?', 'eventorganiserp' ), $n1, $n2 ) );
		
		ob_start();
		include( eo_locate_template( 'eo-booking-form-input.php' ) );
		$html = ob_get_contents();
		ob_end_clean();
		
		$html .= eventorganiser_text_field(array(
			'type'  => 'hidden',
			'value' => wp_hash( $n1 + $n2, 'nonce' ),
			'id'    => 'eo-booking-field-'.$this->element->id.'-2',
			'name'  => $this->element->get_field_name( 'h' ),
			'echo'  => 0,
		));

		return $html;
	}
	
	function get_value(){
		return $this->element->get_value( 'i' );
	}

	function get_name( $component = false ){
		return $this->element->get_field_name( 'i' );
	}	
	
}

/** @ignore **/
class EO_Booking_Form_Element_Checkbox_View extends EO_Booking_Form_Element_View{
	
	function get_options(){
			
		$options = $this->element->get( 'options' );
		
		//TODO Remove this and always expect associative array (requires modification of data from form customser) 
		if( eventorganiser_is_associative( $options ) ){
			$select_options = $this->element->get( 'options' );
		}else{
			$select_options = array_combine( $this->element->get( 'options' ), $this->element->get( 'options' ) );
		}
		
		return $select_options;
	}
	
	
	function checked( $option, $echo = true ){
		return checked( $this->element->is_selected( $option ), true , $echo );
	}
	
	function render(){
		ob_start();
		include( eo_locate_template( 'eo-booking-form-checkbox.php' ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

}

/** @ignore **/
class EO_Booking_Form_Element_Radio_View extends EO_Booking_Form_Element_View{

	function get_options(){
			
		$options = $this->element->get( 'options' );
		
		//TODO Remove this and always expect associative array (requires modification of data from form customser)
		if( eventorganiser_is_associative( $options ) ){
			$select_options = $this->element->get( 'options' );
		}else{
			$select_options = array_combine( $this->element->get( 'options' ), $this->element->get( 'options' ) );
		}
		
		return $select_options;
	}
	

	function checked( $option, $echo = true ){
		return checked( $this->element->is_selected( $option ), true , $echo );
	}
	
	function render(){
		ob_start();
		include( eo_locate_template( 'eo-booking-form-radio.php' ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Section_View extends EO_Booking_Form_Element_View{
	function render(){		
		ob_start();
		include( eo_locate_template( 'eo-booking-form-section.php' ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Fieldset_View extends EO_Booking_Form_Element_View{

	var $wrap = '%2$s';

	function render(){
		
		$style = $this->element->is_visible( false ) ? '' : ' style="display:none;"';
		
		$html = sprintf( 
			'<fieldset id="%s" class="eo-booking-field %s" %s>',
			esc_attr( 'eo-booking-form-element-wrap-'.$this->element->id ), 
			esc_attr( $this->element->get_class() ) ,
			$style
		);
		if( $this->element->get( 'label' ) ){
			$html .= '<legend><span>'.esc_html( $this->element->get( 'label' ) ).'</span></legend>';
		}
		
		$elements_view_class = 'EO_Booking_Form_Elements_View';
		$elements_view_class = apply_filters( 'eventorganiser_booking_form_elements_view', $elements_view_class, $this->element->form );
		
		$elements_view = new $elements_view_class( $this->element->_elements );
		$html .= $elements_view->render();
		
		$html .= '</fieldset>';
		
		return $html;
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Name_View extends EO_Booking_Form_Element_View{

	function render(){
		ob_start();
		include( eo_locate_template( 'eo-booking-form-name.php' ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
}

/** @ignore **/
class EO_Booking_Form_Element_Textarea_View extends EO_Booking_Form_Element_View{

	function render(){
		ob_start();
		if( $this->element->get( 'tinymce' ) ){
			include( eo_locate_template( 'eo-booking-form-textarea.php' ) );
		}else{
			include( eo_locate_template( 'eo-booking-form-textarea.php' ) );
		}
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	function get_value(){
		return $this->element->get_value();
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Html_View extends EO_Booking_Form_Element_View{
	function render(){
		return $this->element->get( 'html' );
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Hook_View extends EO_Booking_Form_Element_View{
	
	function render(){
		$html = '';
		if( $this->element->get( 'wp-action' ) ){
			$action = $this->element->get( 'wp-action' );
			ob_start();
			do_action( $action, $this );
			$html = ob_get_contents();
			ob_end_clean();
		}
		return $html;
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Ticketpicker_View extends EO_Booking_Form_Element_View{
	
	function render(){
	
		$event_id     = $this->element->form->get( 'event_id' );
		$booking_form = $this->element->form;

		$occurrence_tickets = eo_get_the_occurrences_tickets( $event_id );
		$tickets = eo_get_event_tickets_on_sale( $event_id );
	
		if( $booking_form && $booking_form->is_simple_booking_mode() //SBM is enabled
			&& 1 == count( $tickets ) //only 1 ticket available
			&& ( eventorganiser_pro_get_option( 'book_series' ) || !eo_reoccurs( $event_id ) ) //No date selection needed
		){
			$ticket = array_pop( $tickets );
			$ticket_id = $ticket['mid'];
			$html = sprintf(
				'<input type="hidden" name="eventorganiser[booking][tickets][%d]" max="1" style="width:auto;" min="1" value="1" />',
				$ticket_id
			);
		}else{
	
			$html = apply_filters( 'eventorganiser_pre_booking_table_form', '', $event_id );
	
			$template = eo_locate_template( 'eo-ticket-picker.php', false );
			ob_start();
			if( $template ){
				include( $template );
			}
				
			$html .= ob_get_contents();
			ob_end_clean();
				
			//The booking table...
			$html .= apply_filters( 'eventorganiser_post_booking_table_form', '', $event_id );
	
		}
	
		return $html;
	}
	
}

/** @ignore **/
class EO_Booking_Form_Element_Button_View extends EO_Booking_Form_Element_View{
	
	function render(){
		ob_start();
		include( eo_locate_template( 'eo-booking-form-button.php' ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
	function waiting_img_src( $echo = true ){
		$src = esc_attr( EVENT_ORGANISER_PRO_URL . 'images/loader.gif' );
		if( $echo ){
			echo $src;
		}else{
			return $src;
		} 
	}
	
	function get_class(){
		$form_button_classes = $this->element->form ? $this->element->form->get( 'button_classes' ) : '';
		$form_button_classes = $this->element->get( 'class' ) ? $this->element->get( 'class' ) : $form_button_classes;
	
		$class = trim( $form_button_classes );
		$class .= ' eo-booking-field-' . str_replace( '_', '-', $this->element->type );
		return apply_filters( 'eventorganiser_booking_button_classes', $class, $this->element );
	}
	
}

/** @ignore **/
class EO_Booking_Form_Element_Gateway_View extends EO_Booking_Form_Element_View{

	function checked( $option, $echo = true ){
		
		$_selected = $this->element->get_value();
		$checked   = checked( $_selected, $option, false );
		
		if( $echo ){
			echo $checked;
		}else{
			return $checked;
		}
		
	}
	
	function render(){
	
		$booking_form = $this->element->form;
	
		//Get tickets on save now
		$tickets = eo_get_event_tickets_on_sale( $booking_form->get( 'event_id' ) );
	
		$enabled_gateways = $this->element->get_enabled_gateways();
		$total_prices     = ( $tickets ? eventorganiser_list_sum( $tickets, 'price' ) : 0 );
	
		if ( count( $enabled_gateways ) > 1 && $total_prices ) {
			ob_start();
			include( eo_locate_template( 'eo-booking-form-gateway.php' ) );
			$html = ob_get_contents();
			ob_end_clean();

		}else{
			reset( $enabled_gateways );
			$gateway = (  $total_prices ? key( $enabled_gateways ) : 'free' );
			$html = sprintf(
				'<input type="hidden" class="%3$s" name="eventorganiser[booking][%2$s]" id="gateway_%1$s" value="%1$s" />',
				esc_attr( $gateway ),
				esc_attr( $this->element->id ),
				$this->element->get_class()
			);
	
			if( 'offline' == $gateway ){
				$html .= '<p>'.eventorganiser_pro_get_option( 'offline_instructions' ).'</p>';
			}
		}
	
		return $html;
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Terms_Conditions_View extends EO_Booking_Form_Element_View{
	function render(){
		ob_start();
		include( eo_locate_template( 'eo-booking-form-terms-conditions.php' ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

/** @ignore **/
class EO_Booking_Form_Element_Address_View extends EO_Booking_Form_Element_View{

	function label( $component, $echo = true, $escape = true ){
		
		$label = $this->element->get_label( $component );
		
		if( $escape ){
			$label = esc_html( $label );
		}
		
		if( $echo ){
			echo $label;
		}
		
		return $label; 
		
	}
	
	function render(){
		ob_start();
		include( eo_locate_template( 'eo-booking-form-address.php', false ) );
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
	
}