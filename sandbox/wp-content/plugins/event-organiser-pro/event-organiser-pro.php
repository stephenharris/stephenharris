<?php
/*
Plugin Name: Event Organiser Pro
Plugin URI: http://www.wp-event-organiser.com
Version: 1.10.4-beta-1
Description: A premium add-on for Event Organiser. Adds booking management and many other features.
Author: Stephen Harris
Author URI: http://www.stephenharris.info
*/
/*  Copyright 2013 Stephen Harris (contact@stephenharris.info)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

define( 'EVENT_ORGANISER_PRO_DIR', plugin_dir_path( __FILE__ ) );
define( 'EVENT_ORGANISER_PRO_VER', '1.10.4-beta-1' );

function _eventorganiser_pro_set_constants() {
	/*
 	* Defines the plug-in directory url
 	* <code>url:http://mysite.com/wp-content/plugins/event-organiser-pro</code>
	*/
	if ( ! defined( 'EVENT_ORGANISER_PRO_URL' ) ) {
		define( 'EVENT_ORGANISER_PRO_URL', plugin_dir_url( __FILE__ ) );
	}
}
add_action( 'after_setup_theme', '_eventorganiser_pro_set_constants' );

//Install
register_activation_hook( __FILE__, 'eventorganiser_pro_install' );
register_deactivation_hook( __FILE__, 'eventorganiser_pro_deactivate' );
register_uninstall_hook( __FILE__,'eventorganiser_pro_uninstall' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/install.php' );

/**
 * Load the translation file for current language. Checks in wp-content/languages first
 * and then the event-organiser-pro/languages.
 *
 * Edits to translation files inside event-organiser/languages will be lost with an update
 * **If you're creating custom translation files, please use the global language folder.**
 *
 * @since 1.0
 * @ignore
 * @uses apply_filters() Calls 'plugin_locale' with the get_locale() value
 * @uses load_textdomain() To load the textdomain from global language folder
 * @uses load_plugin_textdomain() To load the textdomain from plugin folder
 */
function eventorganiser_pro_load_textdomain() {
	$domain = 'eventorganiserp';
	$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

	$mofile = $domain . '-' . $locale . '.mo';

	/* Check the global language folder */
	$files = array( WP_LANG_DIR . '/event-organiser-pro/' . $mofile, WP_LANG_DIR . '/' . $mofile );
	foreach ( $files as $file ) {
		if ( file_exists( $file ) ) {
			return load_textdomain( $domain, $file );
		}
	}

	//If we got this far, fallback to the plug-in language folder.
	//We could use load_textdomain - but this avoids touching any more constants.
	load_plugin_textdomain( 'eventorganiserp', false, basename( dirname( __FILE__ ) ).'/languages' );
}
add_action( 'plugins_loaded', 'eventorganiser_pro_load_textdomain', 5 );


//Add-on handlings
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/addon.php' );

//General functions
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/functions.php' );

//Register post types, scripts & tables
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/register.php' );

//Utility functions
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/utility-functions.php' );

//Booking & ticket functions
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/tickets.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/booking-tickets.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/bookings.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/booking-actions.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/booking-status.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/booking-form-handler.php' );

//Email handling
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/email.php' );

//User booking functions
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/user-bookings.php' );

//Venue functions
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/venue-functions.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/advanced-queries.php' );


//Booking form customiser
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-controller.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-factory.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-element-factory.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/interface-eo-booking-form-element.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/interface-eo-booking-form-view.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-element.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-elements.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-elements-view.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-view.php' );

if ( ! is_admin() ) {
	require_once( EVENT_ORGANISER_PRO_DIR . 'includes/form-customiser/class-eo-booking-form-element-view.php' );
}

require_once( EVENT_ORGANISER_PRO_DIR . 'includes/template-tags.php' );

/*Gateways */
require_once( EVENT_ORGANISER_PRO_DIR . 'includes/gateways.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'gateways/paypal-standard.php' );
require_once( EVENT_ORGANISER_PRO_DIR . 'gateways/class-eo-gateway.php' );

//Add settigns
add_filter( 'eventorganiser_settings_tabs','eventorganiser_pro_add_settings' );

require_once( EVENT_ORGANISER_PRO_DIR . 'includes/admin-actions.php' );

function eventorganiser_pro_load_files() {

	if ( ! defined( 'EVENT_ORGANISER_DIR' ) ) {
		return;
	}

	if ( is_admin() ) {
		/* Admin pages */
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/settings.php' );
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/edit.php' );
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/post.php' );
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/calendar.php' );
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/includes/ajax-actions.php' );
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/bookings.php' );
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/includes/venues.php' );
		require_once( EVENT_ORGANISER_PRO_DIR . 'admin/includes/event_ticket_table.php' );
	};

	//Shortcodes & Widgets
	require_once( EVENT_ORGANISER_PRO_DIR . 'includes/shortcodes.php' );
	require_once( EVENT_ORGANISER_PRO_DIR . 'includes/class-eo-user-attending-widget.php' );

	add_action( 'widgets_init', 'eventorganiser_pro_widgets_init' );

	//Register scripts
	add_action( 'admin_init', 'eventorganiser_pro_admin_register_scripts', 15 );
	add_action( 'admin_enqueue_scripts', 'eventorganiser_pro_admin_enqueue_scripts', 15 );
	add_action( 'init', 'eventorganiser_pro_register_scripts', 15 );

}
add_action( 'plugins_loaded', 'eventorganiser_pro_load_files' );
